################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Chronometer.cpp \
../src/Context.cpp \
../src/Contract.cpp \
../src/CplexSolver.cpp \
../src/LB.cpp \
../src/NodeImplementation.cpp \
../src/Optimizer.cpp \
../src/Problem.cpp \
../src/Split.cpp \
../src/UB.cpp \
../src/Util.cpp \
../src/is_binary.cpp \
../src/main.cpp \
../src/node_container.cpp 

OBJS += \
./src/Chronometer.o \
./src/Context.o \
./src/Contract.o \
./src/CplexSolver.o \
./src/LB.o \
./src/NodeImplementation.o \
./src/Optimizer.o \
./src/Problem.o \
./src/Split.o \
./src/UB.o \
./src/Util.o \
./src/is_binary.o \
./src/main.o \
./src/node_container.o 

CPP_DEPS += \
./src/Chronometer.d \
./src/Context.d \
./src/Contract.d \
./src/CplexSolver.d \
./src/LB.d \
./src/NodeImplementation.d \
./src/Optimizer.d \
./src/Problem.d \
./src/Split.d \
./src/UB.d \
./src/Util.d \
./src/is_binary.d \
./src/main.d \
./src/node_container.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DIL_STD -I/home/gwen/build/cplex/cplex/include -I/home/gwen/build/cplex/concert/include -I/home/gwen/build/compiled_armadillo/include -I../dlib -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


