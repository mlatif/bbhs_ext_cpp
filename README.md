# Branch and Bound for sparse signals

## How to build

This project has three mandatory dependencies:
- [dlib](http://dlib.net/)
- [armadillo](http://arma.sourceforge.net/)
- [qpOASES](https://github.com/coin-or/qpOASES) [1]  for  the Sparse unmixing extension ([User's Manual](https://www.coin-or.org/qpOASES/doc/3.0/manual.pdf))


CPLEX is an optional dependency. Make sure to write down the path of the CPLEX installation
if you didn't install it in a standard location.

This project uses CMake to build:

```
$ mkdir build_dir && cd build_dir
$ cmake -DUSE_CPLEX=<0|1> -DCPLEX_ROOT_DIR=<path> -DDLIB_ROOT_DIR=<path> -DArmadillo_ROOT_DIR=<path> -DQpoases_ROOT_DIR=<path> -DLOG_PER_NODE_STAT=<0|1>  -DCMAKE_INSTALL_PREFIX=<path>  ..
```

Note the last `..` is mandatory.
Meaning of the options:

- `USE_CPLEX`: build with CPLEX support (=1) or not (=0). Building with CPLEX allows you to compare the performance of the whole dedicated Branch-and-Bound algorithm
as well as just the dedicated lower bound method agaisnt the corresponding problem
solved by CPLEX. By default, try to build with CPLEX support, and fall-back to a no-CPLEX
build if no CPLEX intallation is found.
- `CPLEX_ROOT_DIR`: the path to CPLEX installation. If building without CPLEX support,
this is ignored. Standard locations are auto-detected, so you don't need to set this
option for a standard CPLEX installation.
- `DLIB_ROOT_DIR`: the path to Dlib installation. Standard locations are auto-detected,
so you don't need to set this option for a standard Dlib installation.
- `Armadillo_ROOT_DIR`: the path to Armadillo installation. Standard locations are
auto-detected, so you don't need to set this option for a standard Armadillo
installation.
- `Qpoases_ROOT_DIR`: the path to qpOases solver installation. Standard locations are **not**
auto-detected, so you need to set this option
- `LOG_PER_NODE_STAT`: output (=1) or don't output (=0) statistics for each node of the
Branch-and-Bound algorithm. Default to 0
(per node statistics gives a huge log, and hurts performance).
- `CMAKE_INSTALL_PREFIX`: path used for the `make install` call as the installation root.
Default to `/`.


### Making a .deb

To distribute the code, you can make a .deb with ̀`cpack` inside the `build_dir` directory:

```
$ cpack -g DEB
```

You'll end up with a `MimosaUnmix_.._amd64.deb` file.

## How to run

(speak about data somewhere)

If you wish to be able to compare performances fairly, you must use
a single-threaded run. This is done in the code for the CPlex solver,
but for armadillo you must tell your BLAS implementation to do so.

In OpenBLAS, this is done by setting the following environment variables
in the launching shell:

```
$ export OPENBLAS_NUM_THREADS=1
$ export GOTO_NUM_THREADS=1
$ export OMP_NUM_THREADS=1
$ [Debug/bb_cpp, or more generally the result of this repo's compilation] [path_to_Dictionary_matrix_in_dat_format] [path_to_instances_directory] [problem_type] [solver_type] [strategy] [strategy option] [SNR] [K] [instance_number]
```

As you can see there are numerous arguments. For now, each argument is mandatory and must
be given in the correct order specified above.

The `path_to_instances_directory` is the directory where the `SA_SNRXX_KY_instanceZZ` directories are found, each instance directory containing:

- `lambda.dat`
- `alpha_bruit.dat`
- `y.dat`
- (optional) `x0_2p0_sbr.dat`

The problem type can be one of the following:

- `l2l0`
- `l0l2`
- `l2pl0_without_SBR`
- `l2pl0_with_SBR`

This is an exact match which is done under the hoods: `L2L0` won't match an will issue an error.

The solver type can be one of the following:

- `full_cplex` to solve each problem with a Cplex model
- `bb_cplex` to solve each problem with our Branch & Bound, using cplex to compute lower bounds
- `bb_homotopy` to solve each problem with our Branch & Bound, using a homotopy method to compute lower bounds
- `bb_activeset_warm` to solve each problem with our Branch & Bound, using an active-set based method, enabling warm start
- `bb_activeset_warm` to solve each problem with our Branch & Bound, using an active-set based method, disabling warm start

The last two methods are only available in P2+0 problem class, and this is checked at runtime.

The strategy is the container of nodes which should be used. The strategy option is there to be able
to feed the strategy with an argument.

For now, the strategy can be:

- `stack`, this corresponds to a depth-first walk in the search tree (pure intensification). Ignores strategy option.
- `heap_on_lb`, this corresponds to a best-first walk in the search tree (pure diversification). Ignores strategy option.
- `heap_on_ls`, which also uses a heap but using the l1 norm of the convex-relaxed problem solution instead of its cost (this is the same as `heap_on_lb` for `l0l2` problem, but different for the others). Ignores strategy option.
- `stack_then_heap_on_XX_iteration_threshold` where `XX` can be `lb` or `l1`. It begins as a stack for N iterations, and then convert to the corresponding heap strategy. The N must be given as the strategy option.

SNR, K and instance number can be single scalars or ranges in the matlab style:

- 5
- 5:9
- 5:2:9

So for example, if you wish to test all the 50 instance for all K in {5, 7, 9}, with an SNR of 10,
the last three arguments would be `10 5:2:9 1:50` (also `10:10 5:2:9 1:50` and `10:1:10 5:2:9 1:1:50` is possible of course).

### Examples

Running P2/0 problem for one instance (SNR=10, K=5, instance=1) using the cplex solver:
```
$ ./bb_cpp [path_to_dict] [path_to_instances] l2l0 full_cplex stack 0 10 5 1
```

Running P2+0 without SBR, using warm started active set, on every odd instances for SNR=10 and K in {5, 7}:
```
$ ./bb_cpp [path_to_dict] [path_to_instances] l2pl0_without_SBR bb_activeset_warm stack_then_heap_on_lb_iteration_threshold 20 10 5:2:7 1:2:49
```


## Sparse unmixing extension (P2/0+ASC+ANC)
Solve
```math
\min_{ \boldsymbol{x} \in [0,1]^{Q}} \quad \frac{1}{2}\big\|\boldsymbol{y}-\mathbf{H}\boldsymbol{x}\big\|_{2}^{2}\quad \textrm{ st. } \|\boldsymbol{x}\|_{0} \leq K, \quad \boldsymbol{1}_{Q}^{\intercal}\boldsymbol{x} =1
```

As mentioned before, you must respect the order of the arguments to launch the program.
```
$ export OPENBLAS_NUM_THREADS=1; export GOTO_NUM_THREADS=1; export OMP_NUM_THREADS=1
$ export LD_LIBRARY_PATH=/home/user/Bureau/DL_BB/qpOASES/bin
$ <command generating a list of instances> | MimosaUnmix <problem_type> <solver_type> <strategy> <strategy option> <K> <asc_form>
```
In order to launch the program to solve the sparse unmixing problem under abundance non-negativity constraint (ANC) and abundance sum-to-one constraint (ASC), you must respect the following notations :
- **problem_type** : `l2l0_asc_anc`
- **solver type** : 6 options
	 - `full_cplex` : to solve each problem with a Cplex mode
	 - `bb_cplex` :  to solve each problem with our Branch & Bound, using cplex to compute bounds (lower and upper)
	 - `bb_qpoases` : to solve each problem with our Branch & Bound, using qpOases[1] to compute bounds
	 - `bb_fcls` : to solve each problem with our Branch & Bound, using the FCLS[2] algorithm to compute bounds **but** only available for over-constrained problems;
	 - `bb_homotopy_qpoases` : hybrid strategy with lower (resp. upper) bound evaluations using homotopy algorithm (resp. qpoases solver)
	 - `bb_homotopy_fcls` : hybrid strategy with lower (resp. upper) bound evaluations using homotopy [3,4,5,6] algorithm (resp. FCLS algorithm)
- **strategy** : `stack`
- **strategy option** : `0`
- **ASC formulation (optional)** : to consider the formulation of the problem with the unit sum constraint (ASC) as an equality (`eq`) or an inequality (`ineq`). This argument is optional and its default value is `eq`.

The values of *K*, *SNR* and *instance index* are identical to those presented above (scalar or range).
To ensure compatibility with the existing framework, we assume that the stored instances contain the dictionary, so we have : ```path_to_dict``` = ```path_to_instances```.  <br>
The names of the directories in which the instances are stored follow the same syntax as above : ```SA_SNRXX_KY_instanceZZ``` and these directories contain in particular the following *.dat* files :
- ```Hmatrix.dat```
- ```x_truth.dat``` (if known ! else a vector of ```0```)
- ```y.dat```
- ```inst.mat``` all information related to the generation of the instance from the Matlab script.

An instance generator is available [here](https://gitlab.com/mlatif/branch-and-bound-unmixing-competitor/-/tree/main/generators), two datasets are also available [here](https://uncloud.univ-nantes.fr/index.php/s/eyGtmM2wJ9k6Px5).<br>

### Output and results
Finally, the experimental results are stored in the directories of the instances as .txt files (easily usable and parsable with a software as Matlab or Julia <3 ) with the following notation convention :

- `method` _ K `x` _ `y`_ xopt.txt : contains the abundance vector;
- `method` _ K `x` _ `y`_ output.txt :   
with
	- `method` : BBRcplex, BBRfcls, BBRqpoases, BBRhom_qpoases, BBRhom_fcls
	- `x` : the desired sparsity coefficient.
	- `y` : the ASC formulation (eq or in)  

The *_output.txt* file, unique for all methods, is a parsable vector structured as follows:

| **Index** | **Metric** | **Available for methods** |
|:---:|:---:|:---:|
| **0** | The computed upperbound | all |
| **1** | The number of explorated nodes | all |
| **2** | The execution time | all |
| **3** | The modeling time | with cplex, qpoases |
| **4** | The fcls execution time | with fcls |
| **5** | The homotopy execution time | with homotopy |
| **6** | The best node number  | branch-and-bound methods |
| **7** | The Cplex status | MIP method |

The default value for methods not concerned by a metric is `inf`. For variable 6, if we descended to deeply in the tree, the default node number is set to `INT_MAX`

Meanwhile, a file allowing to follow the evolution of the resolutions is automatically created and updated at the root of the UNMIX directory and named `listing_solver.txt`.  A line stands for a resolution and contains :
```
[instance_name] [desired_k] [asc_form] [method] [execution_time] [nb of explorated node(s)] [least square error]
```
If an error is encountered during the resolution (which can occur sometimes with the FCLS algorithm due to the matrix conditioning ...), the fields are filled with the `inf` value.

### Quick setup procedure
Do this without asking too many questions:
```
$ export OPENBLAS_NUM_THREADS=1; export GOTO_NUM_THREADS=1;
$ export OMP_NUM_THREADS=1
$ export LD_LIBRARY_PATH=[...]/qpOASES/bin
$ mkdir build_dir && cd build_dir
```
Adapt the following line according to your configuration (according to previous explanations); for example, in my case it is :
```
$ cmake -DUSE_CPLEX=1 -DQpoases_ROOT_DIR=/home/user/Bureau/DL_BB/qpOASES -DCPLEX_ROOT_DIR=/opt/ibm/ILOG/CPLEX_Studio1210/ ..
```
Then we type :
```
$ cat /proc/cpuinfo // I've 4 cores
$ make -j 4
$ sudo make install
$ ls
CMakeCache.txt  cmake_install.cmake  CPackSourceConfig.cmake  MimosaUnmix
CMakeFiles      CPackConfig.cmake    Makefile
```
We can see `MimosaUnmix` program, it's cool !!  To check that the setup was successful :
```
user@[...]:[...]/UNMIX/build_dir$ ./MimosaUnmix
ERROR: No problem type supplied !
USAGE: ./MimosaUnmix ProblemType SolverType ExplorationStrategyType ExplorationStrategyTypeArgument K AscFormulation
with:
- For ProblemType = l2l0_asc_anc :
	 * SolverType in {full_cplex, bb_cplex, bb_fcls, bb_qpoases, bb_homotopy_qpoases, bb_homotopy_fcls}
	 * ExplorationStrategy : stack
	 * ExplorationStrategyType : 0
	 * K a positive integer
	 * [optionnal] AscFormulation : eq, ineq (default value "eq")
```

### Examples :
We assume that the instances are stored in a directory named ```ASC_ANC_INST/```
Running P2/0+ASC+ANC on the instance number 42 with $K = 5$ and $SNR = 40$ dB  using cplex solver
```
./build/bb_cpp ASC_ANC_INST/ ASC_ANC_INST/ l2l0_asc_anc full_cplex stack 0 40 5 42
```
Running P2/0+ASC+ANC on every odd instances with $K \in \{5,6,7\}$ and $SNR = \{40,45,50,55\}$ dB using qpOases solver :
```
./build/bb_cpp ASC_ANC_INST/ ASC_ANC_INST/ l2l0_asc_anc bb_qpoases stack 0 40:5:55 5:7 1:2:139
```
Gwen has developed several bash scripts for easier use; they are listed in the `UNMIX/scripts` directory. For example, you can do this in order to launch a resolution for an problem with the `bb_homotopy_fcls` with an estimation sparsity coefficient equals to 2 and the ASC as an equality :
```
user@[...]:[...]/UNMIX/INST/SA_SNR40_K4_instance30$ MimosaUnmix_1inst_meth $(pwd) 2 bb_homotopy_fcls eq
L2L0_ASC_ANC_BB_Rhom_fcls
Instance : SA_SNR40_K4_instance30
K = 2	 Q = 50	 N = 224	 ASC form : equality
-----------------------------------------------------------------------
#(it) = 10	 #(node) = 19	 T = 0.083769	 card(L) = 2
xUB* = 1'	 34'	 
UB*  = 0.312598	sum(xUB*) = 1
-----------------------------------------------------------------------
[...]
-----------------------------------------------------------------------
#(it) = 100	 #(node) = 199	 T = 0.319659	 card(L) = 1
xUB* = 24	 39'	 
UB*  = 0.170461	sum(xUB*) = 1
-----------------------------------------------------------------------
SA_SNR40_K4_instance30 - bb_homotopy_fcls
	T = 0.322625 (s)
	#(node) = 211	/ best(node) = 221
	UB*  = 0.170461
	xUB* :
		 x(24) = 	0.234965
		 x(39)' = 	0.765035
	sum(xUB*) = 1
```
The apostrophes in the terminal display appear because in this case we know the vector `x_truth`. This is a dummy example because the instance we are running was generated with 4 non-zero components (`SA_SNR40_K4_instance30`).


### Bibliography
[1] Ferreau, H.J., Kirches, C., Potschka, A. _et al._ qpOASES: a parametric active-set algorithm for quadratic programming. _Math. Prog. Comp._  6, 327–363 (2014).  
[2] : D. C. Heinz and Chein-I-Chang, "Fully constrained least squares linear spectral mixture analysis method for material quantification in hyperspectral imagery," in IEEE Transactions on Geoscience and Remote Sensing, vol. 39, no. 3, pp. 529-545, March 2001.  
[3] D.L. Donoho and Y. Tsaig, Fast solution of l1-norm minimization problems when the solution may be sparse, IEEE Transactions on Information Theory 54 (2008).   
[4] B. Efron, T. Hastie, I. Johnstone, R. Tibshirani, et al., Least angle regression, The Annals of statistics 32 (2004), pp. 407–499.  
[5] M. Osborne, B.P. B, and B.T. BAD, A new approach to variable selection in least squares problems, IMA Journal of Numerical Analysis (2000).  
[6] Ramzi Ben Mhenni, Sébastien Bourguignon, Jordan Ninin. Global Optimization for Sparse Solution of Least Squares Problems. 2019.


## Credits
Ramzi Ben Mhenni, Gwenaël Samain, Sébastien Bourguignon, Mehdi Latif - Equipe SiMS / LS2N
