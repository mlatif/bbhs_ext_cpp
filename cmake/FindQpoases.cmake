# This module finds an installed armadilo package.
#
# It sets the following variables:
#  Qpoases_FOUND              - Set to false, or undefined, if lemon isn't found.
#  Qpoases_INCLUDE_DIR        - Lemon include directory.
#  Qpoases_LIBRARIES          - Lemon library files
set(Qpoases_ROOT_DIR "" CACHE PATH "Qpoases root directory")
FIND_PATH(Qpoases_INCLUDE_DIR qpOASES 
          HINTS ${Qpoases_ROOT_DIR}/include
          PATHS /usr/include /usr/local/include ${CMAKE_INCLUDE_PATH} ${CMAKE_PREFIX_PATH}/include $ENV{Qpoases_ROOT}/include ENV CPLUS_INCLUDE_PATH)
FIND_LIBRARY(Qpoases_LIBRARIES 
  NAMES libqpOASES.a
  HINTS ${Qpoases_ROOT_DIR}/lib
  PATHS $ENV{Qpoases_ROOT}/src/impex $ENV{Qpoases_ROOT}/lib ENV LD_LIBRARY_PATH ENV LIBRARY_PATH
)

GET_FILENAME_COMPONENT(Qpoases_LIBRARY_PATH ${Qpoases_LIBRARIES} PATH)
SET( Qpoases_LIBRARY_DIR ${Qpoases_LIBRARY_PATH} CACHE PATH "Path to lemon library.")

# handle the QUIETLY and REQUIRED arguments and set Qpoases_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Qpoases DEFAULT_MSG Qpoases_LIBRARIES Qpoases_INCLUDE_DIR)

MARK_AS_ADVANCED( Qpoases_INCLUDE_DIR Qpoases_LIBRARIES Qpoases_LIBRARY_DIR )
