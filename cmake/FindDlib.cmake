# This module finds an installed armadilo package.
#
# It sets the following variables:
#  DLIB_FOUND              - Set to false, or undefined, if lemon isn't found.
#  DLIB_INCLUDE_DIR        - Lemon include directory.
#  DLIB_LIBRARIES          - Lemon library files
set(DLIB_ROOT_DIR "" CACHE PATH "DLIB root directory")
FIND_PATH(DLIB_INCLUDE_DIR dlib 
          HINTS ${DLIB_ROOT_DIR}/include
          PATHS /usr/include /usr/local/include ${CMAKE_INCLUDE_PATH} ${CMAKE_PREFIX_PATH}/include $ENV{DLIB_ROOT}/include ENV CPLUS_INCLUDE_PATH)
FIND_LIBRARY(DLIB_LIBRARIES 
  NAMES dlib
  HINTS ${DLIB_ROOT_DIR}/lib
  PATHS $ENV{DLIB_ROOT}/src/impex $ENV{DLIB_ROOT}/lib ENV LD_LIBRARY_PATH ENV LIBRARY_PATH
)

GET_FILENAME_COMPONENT(DLIB_LIBRARY_PATH ${DLIB_LIBRARIES} PATH)
SET( DLIB_LIBRARY_DIR ${DLIB_LIBRARY_PATH} CACHE PATH "Path to lemon library.")

# handle the QUIETLY and REQUIRED arguments and set DLIB_FOUND to TRUE if 
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(DLIB DEFAULT_MSG DLIB_LIBRARIES DLIB_INCLUDE_DIR)

MARK_AS_ADVANCED( DLIB_INCLUDE_DIR DLIB_LIBRARIES DLIB_LIBRARY_DIR )
