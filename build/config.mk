CC := gcc
CXX := g++

#ARMA_LIB_PATH := /home/gwen/build/compiled_armadillo/lib
ARMA_LIB_PATH := /home/user/Bureau/DL_BB/compiled_armadillo/lib
#CPLEX_LIB_PATH := /home/gwen/build/cplex/cplex/lib/x86-64_linux/static_pic
CPLEX_LIB_PATH := /opt/ibm/ILOG/CPLEX_Studio1210/cplex/lib/x86-64_linux/static_pic
#CONCERT_LIB_PATH := /home/gwen/build/cplex/concert/lib/x86-64_linux/static_pic
CONCERT_LIB_PATH := /opt/ibm/ILOG/CPLEX_Studio1210/concert/lib/x86-64_linux/static_pic
######################################################################################
#ARMA_INCLUDE_PATH := /home/gwen/build/compiled_armadillo/include
ARMA_INCLUDE_PATH := /home/user/Bureau/DL_BB/compiled_armadillo/include
#CPLEX_INCLUDE_PATH := /home/gwen/build/cplex/cplex/include
CPLEX_INCLUDE_PATH := /opt/ibm/ILOG/CPLEX_Studio1210/cplex/include
#CONCERT_INCLUDE_PATH := /home/gwen/build/cplex/concert/include
CONCERT_INCLUDE_PATH := /opt/ibm/ILOG/CPLEX_Studio1210/concert/include
######################################################################################
QPOASES_LIB_PATH := /home/user/Bureau/DL_BB/qpOASES/bin
QPOASES_INCLUDE_PATH := /home/user/Bureau/DL_BB/qpOASES/include
######################################################################################
######### -lqpOASES ???
LIBS = -lilocplex -lconcert -lcplex -lm -lpthread -ldl -lopenblas -larmadillo -lqpOASES
#LIBS = -lilocplex -lconcert -lcplex -lm -lpthread -ldl -lmkl_avx2 -larmadillo
