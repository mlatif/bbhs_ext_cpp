

# http://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/

_BB_CPP_OBJS = Chronometer.o \
Context.o \
Contract.o \
CplexSolver.o \
LB.o \
NodeImplementation.o \
Optimizer.o \
Problem.o \
Split.o \
UB.o \
Util.o \
is_binary.o \
main.o \
node_container.o

BUILD_DIR=./objects
SRC_DIR=../src

BB_CPP_OBJS = $(patsubst %,$(BUILD_DIR)/%,$(_BB_CPP_OBJS))

DLIB_OBJS = ../dlib/dlib/all/source.o
