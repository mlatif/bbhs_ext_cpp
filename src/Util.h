#ifndef OCUTIL
#define OCUTIL 1

#include "armadillo"
using namespace arma;

uword arg_min_p(uvec J, vec e, double *val, int *i1, double eps);
uword arg_min_st_p(uvec J, vec e, double *val, int *i1);

uword arg_max_p(uvec J, vec e, double *val, int *i1, double eps);

void pause_cin();
void pause_cin_txt(string);

uword deplacer(uword u1, uvec *v1, uvec *v2);

mat inversion_rec_add_P(mat B, vec HPv, mat vPv);

mat inversion_rec_del_P(mat B, uword pos );

int norm_zero(vec x, double err_x);

// FCLS
uword deplacer(uword u1, uvec *v1, uvec *v2,string txt);
void deplacer(uvec C, uvec *R, uvec *P,string txt);
mat getFCLS_Matrix(uvec R, mat iEtE, vec iEtEOne, vec One);


#endif
