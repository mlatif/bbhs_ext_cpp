#ifndef BBUB
#define BBUB 1

#include "Context.hpp"
#include "interfaces.hpp"

#include "MimosaConfig.hpp"

#include <dlib/optimization.h>


// Upper bounds calculation
// All the following classes are context-aware

/** \file UB.cpp
 * \brief Upper Bound computations
 */

/**
 * Internal tuple datatype used for interaction with ContextAwareUB.commonUB().
 */
typedef std::pair<double, unsigned int> LsK;
/**
 * Internal tuple datatype used for interaction with ContextAwareUB.commonUB().
 */
typedef std::pair<vec, LsK> common_return;


/**
 * When computing the upper bound of a node, we must solve a least-square problem with a bigM constraint on x.
 * We do so by first attempting to solve it in an unconstrained manner through classic (and fast) linear algebra.
 * If the obtained solution violates the bigM constraint, we must take into account the bigM constraint in
 * the problem resolution.
 * This interface is the common denominator of algorithms able to do so.
 */
class QuadraticProgramWithBigMSolverInterface {
public:
	/**
	 * The solver code goes here.
	 *
	 * @param H The dictionary matrix restricted to S1
	 * @param y The data measurements.
	 * @param bigM The BigM constraint.
	 * @returns The antecedent solution of the constrained least-square problem.
	 */
	virtual vec operator()(mat H, vec y, double bigM) = 0;
};

#ifdef USE_CPLEX
/**
 * Behind this class lies a simple Cplex model to solve the bigM-constrained least-square problem.
 */
class CplexQPSolver : public QuadraticProgramWithBigMSolverInterface {
	ContextData* ctx; /**< The optimization context (we update cplex's modeling time here). */
public:
	virtual vec operator()(mat H, vec y, double bigM);
	CplexQPSolver(ContextData* context) : ctx(context) {}
};
#endif // USE_CPLEX

/**
 * This class solves the bigM-constrained least-square using dlib's solve_qp_box_constrained function.
 */
class DlibQPSolver : public QuadraticProgramWithBigMSolverInterface {
protected:
	/**
	 * Conversion from dlib vector to armadillo vector.
	 *
	 * @param dlib_vec The Dlib vector to convert.
	 * @returns The armadillo-converted vector.
	 */

	vec convDlibToArmaVec(dlib::matrix<double, 0, 1>& dlib_vec);
public:
	virtual vec operator()(mat H, vec y, double bigM);
};


/**
 * Generic code to handle context attribute and to compute the common components of the upper bound,
 * which are the least-square error measure and the l0 norm.
 */
class ContextAwareUB : public UBInterface {
protected:
	ContextData* ctx; /**< The context reference to, most notably, update computing times. */
	/**
	 * If the simple least-square inversion leads us to an antecedent violating
	 * the BigM constraint (aka we have at least one i such that |xi| > BigM),
	 * we optimize the least-square error using a solver taking into account the BigM constraint.
	 *
	 * @param H The dictionary matrix reduced to the columns of S1
	 * @param y Data measurements
	 * @param bigM The BigM constraint on every xi
	 * @returns the upper bound antecedent
	 */
	QuadraticProgramWithBigMSolverInterface& optim_QP_bigM;
	/**
	 * This is the common code for every problem type, which does all of the computationally heavy tasks.
	 * It computes the l0 norm and the least square error measure of the upper bound, ensuring the
	 * upper bound antecedent fits in the BigM constraint.
	 */
	virtual common_return commonUB(NodeInterface& node);

public:
	/**
	 * This constructor is preferred to avoid stupid nullptr Context dereference errors.
	 */
	ContextAwareUB(ContextData* context, QuadraticProgramWithBigMSolverInterface& solver) : ctx(context), optim_QP_bigM(solver) {}
	void setContext(ContextData* context) { this->ctx = context; }
	virtual ~ContextAwareUB() {}
};

/**
 * Upper bound for P0/2 problem.
 */
class UBL0L2 : public ContextAwareUB {
public:
	virtual bound_type operator()(NodeInterface& node);
	UBL0L2(ContextData* context, QuadraticProgramWithBigMSolverInterface& solver): ContextAwareUB(context, solver) {}
};

/**
 * Upper bound for P2/0 problem.
 */
class UBL2L0 : public ContextAwareUB {
public:
	virtual bound_type operator()(NodeInterface& node);
	UBL2L0(ContextData* context, QuadraticProgramWithBigMSolverInterface& solver): ContextAwareUB(context, solver) {}
};

/**
 * Upper bound for P2+0 problem.
 */
class UBL2pL0 : public ContextAwareUB {
public:
	virtual bound_type operator()(NodeInterface& node);
	UBL2pL0(ContextData* context, QuadraticProgramWithBigMSolverInterface& solver): ContextAwareUB(context, solver) {}
};

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
class ASC_ANC_SolverInterface {
		public :
			/**
			 * The solver code goes here.
			 *
			 * @param H The dictionary matrix restricted to S1
			 * @param y The data measurements.
			 * @param n The index of the node
			 * @returns The solution vector of the constrained least-square problem.
			 */
				virtual vec operator()(mat H,vec y,uvec Q1,unsigned long long int n) = 0;
};
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
#ifdef USE_CPLEX
class CplexQPSolver_ASC_ANC : public ASC_ANC_SolverInterface {
		ContextData* ctx;
		public :
				/**
						Implemntation of the SolverInterface for Cplex Solver to evaluate upper bounds
				*/
				virtual vec operator()(mat H, vec y, uvec Q1,unsigned long long int n);
				CplexQPSolver_ASC_ANC(ContextData* context) : ctx(context)	{};
};
#endif // USE_CPLEX

class FCLSSolver_ASC_ANC : public ASC_ANC_SolverInterface {
		ContextData* ctx;
		public :
				/**
						Implemntation of the SolverInterface for the FCLS algorithm to evaluate upper bounds
				*/
				virtual vec operator()(mat H, vec y, uvec Q1,unsigned long long int n);
				FCLSSolver_ASC_ANC(ContextData* context) : ctx(context)	{};
};
class OSQPSolver_ASC_ANC : public ASC_ANC_SolverInterface {
		ContextData* ctx;
		public :
				/**
						Implemntation of the SolverInterface for qpOASES Solver to evaluate upper bounds
				*/
				virtual vec operator()(mat H, vec y, uvec Q1,unsigned long long int n);
				OSQPSolver_ASC_ANC(ContextData* context) : ctx(context) {};
};

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
/**
 * Generic code to handle context attribute and to compute the common components of the upper bound,
 * which are the least-square error measure and the l0 norm.
 * Cplex Solver Implementation
 */
#ifdef USE_CPLEX
class ContextAwareUB_ASC_ANC_QPCplex : public UBInterface {
		protected :
				ContextData* ctx;
				ASC_ANC_SolverInterface& optim_QPC_ASC_ANC;
				virtual common_return commonUB_ASC_ANC(NodeInterface& node);
		public :
				ContextAwareUB_ASC_ANC_QPCplex(ContextData* context,ASC_ANC_SolverInterface& solver) : ctx(context),optim_QPC_ASC_ANC(solver) {};
				void setContext(ContextData* context) {this-> ctx = context;};
				virtual ~ContextAwareUB_ASC_ANC_QPCplex() {};
};
#endif // USE_CPLEX
/**
 * Generic code to handle context attribute and to compute the common components of the upper bound,
 * which are the least-square error measure and the l0 norm.
 * FCLS algorithm Implemntation
 */
class ContextAwareUB_ASC_ANC_FCLS : public UBInterface {
		protected :
				ContextData* ctx;
				ASC_ANC_SolverInterface& optim_FCLS_ASC_ANC;
				virtual common_return commonUB_ASC_ANC(NodeInterface& node);
		public :
				ContextAwareUB_ASC_ANC_FCLS(ContextData* context,ASC_ANC_SolverInterface& solver) : ctx(context),optim_FCLS_ASC_ANC(solver) {};
				void setContext(ContextData* context) {this-> ctx = context;};
				virtual ~ContextAwareUB_ASC_ANC_FCLS() {};
};
// OPERATEUR UNIQUE O.pen S.ource Q.uadratic P.rogram
/**
 * Generic code to handle context attribute and to compute the common components of the upper bound,
 * which are the least-square error measure and the l0 norm.
 * qpOASES Solver Implemntation
 */
class ContextAwareUB_ASC_ANC_OSQP : public UBInterface {
		protected :
				ContextData* ctx;
				ASC_ANC_SolverInterface& optim_OSQP_ASC_ANC;
				virtual common_return commonUB_ASC_ANC(NodeInterface& node);
		public :
				ContextAwareUB_ASC_ANC_OSQP(ContextData* context,ASC_ANC_SolverInterface& solver) : ctx(context),optim_OSQP_ASC_ANC(solver) {};
				void setContext(ContextData* context) {this-> ctx = context;};
				virtual ~ContextAwareUB_ASC_ANC_OSQP() {};
};

/*
// OPERATEUR HYBRIDE (Pour comparer les résultats Cplex qpOASES)
class ContextAwareUB_ASC_ANC_OSQP_Cplex : public UBInterface {
		protected :
				ContextData* ctx;
				ASC_ANC_SolverInterface& optim_OSQP_ASC_ANC;
				ASC_ANC_SolverInterface& optim_QPCplex_ASC_ANC;
				virtual common_return commonUB_ASC_ANC(NodeInterface& node);
		public :
				ContextAwareUB_ASC_ANC_OSQP_Cplex(ContextData* context,ASC_ANC_SolverInterface& solverO, ASC_ANC_SolverInterface& solverC) : ctx(context),optim_OSQP_ASC_ANC(solverO),optim_QPCplex_ASC_ANC(solverC) {};
				void setContext(ContextData* context) {this-> ctx = context;};
				virtual ~ContextAwareUB_ASC_ANC_OSQP_Cplex() {};
};
*/

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
/**
 * Upper bound for P2/0+ASC+ANC problem for Cplex solver
 */
#ifdef USE_CPLEX
class UBL2L0_ASC_ANC_QPCplex : public ContextAwareUB_ASC_ANC_QPCplex {
		public :
				virtual bound_type operator()(NodeInterface& node);
				UBL2L0_ASC_ANC_QPCplex(ContextData* context,ASC_ANC_SolverInterface& solver) : ContextAwareUB_ASC_ANC_QPCplex(context,solver) {};
};
#endif // USE_CPLEX
/**
 * Upper bound for P2/0+ASC+ANC problem for FCLS algorithm
 */
class UBL2L0_ASC_ANC_FCLS : public ContextAwareUB_ASC_ANC_FCLS {
		public :
				virtual bound_type operator()(NodeInterface& node);
				UBL2L0_ASC_ANC_FCLS(ContextData* context,ASC_ANC_SolverInterface& solver) : ContextAwareUB_ASC_ANC_FCLS(context,solver) {};
};
// OPERATEUR UNIQUE O.pen S.ource Q.uadratic P.rogramming
/**
 * Upper bound for P2/0+ASC+ANC problem for qpOASES solver
 */
class UBL2L0_ASC_ANC_OSQP : public ContextAwareUB_ASC_ANC_OSQP {
		public :
				virtual bound_type operator()(NodeInterface& node);
				UBL2L0_ASC_ANC_OSQP(ContextData* context,ASC_ANC_SolverInterface& solver) : ContextAwareUB_ASC_ANC_OSQP(context,solver) {};
};
/*
// OPERATEUR HYBRIDE (Pour comparer les résultats Cplex qpOASES)
class UBL2L0_ASC_ANC_OSQP_QPCplex : public ContextAwareUB_ASC_ANC_OSQP_Cplex {
		public :
				virtual bound_type operator()(NodeInterface& node);
				UBL2L0_ASC_ANC_OSQP_QPCplex(ContextData* context,ASC_ANC_SolverInterface& solverO, ASC_ANC_SolverInterface& solverC) : ContextAwareUB_ASC_ANC_OSQP_Cplex(context,solverO,solverC) {};
};
*/

#endif
