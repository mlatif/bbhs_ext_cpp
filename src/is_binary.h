#ifndef RFISBINARY
#define RFISBINARY 1

#include "armadillo"

using namespace arma;

bool is_binary(vec b);

#endif
