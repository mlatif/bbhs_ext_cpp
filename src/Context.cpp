#include <iostream>
#include "Problem.hpp"
#include "Context.hpp"

#include "UB.hpp"
#include "LB.hpp"


ContextData::ContextData(ProblemData* pb)
: it_num(0), UB(exp10(10)), feasible_solution_set(nullptr), problem(pb),
  T_test(0.0), T_model(0.0), T_relaxation(0.0), T_homo(0.0),T_FCLS(0.0), nbr_iter(0), itmax(0), cut(0), warm_restart(true), BBNodeNum(0), BestNodeNum(0), TimeBBMax(1000),
  nbr_update_Bsupp(0), T_best_Bsupp(0.0),
#ifdef USE_CPLEX
  CplexNodeNum(0), CplexStatus(IloAlgorithm::Status::Unknown), cplex_branching_rule(1),
#endif
  gap(exp10(-8)), tolC(exp10(-8)),tolH(exp10(-5)) , tolX(0), tolF(0),
  inst_id(""), set_id(""), force_stop(false),verbose(false)
{}

ContextData::ContextData() : ContextData(nullptr) {}

void ContextData::reset(ProblemData* pb)
{
	it_num = 0;
	UB = exp10(10);
	feasible_solution_set = nullptr;
	problem = pb;

	T_test = 0.0;
	T_model = 0.0;
	T_relaxation = 0.0;
	T_homo = 0.0;
  T_FCLS = 0.,
	nbr_iter = 0;
	itmax = 0;
	//cut = 0;
	//warm_restart = true;
	BBNodeNum = 0;
	BestNodeNum = 0;
	//TimeBBMax = 1000;

	nbr_update_Bsupp = 0;
	T_best_Bsupp = 0.0;
#ifdef USE_CPLEX
	CplexNodeNum = 0;
	CplexStatus = IloAlgorithm::Status::Unknown;
#endif
	//cplex_branching_rule = 1;
	//gap  = exp10(-8);
	//tolX = 0;
	//tolF = 0;
}

// void ContextData::reset(ProblemData* pb, vec x) {
// 	it_num = 0;
// 	UB = exp10(10);
// 	feasible_solution_set = nullptr;
// 	problem = pb;
//   //
//   T_test = 0.0;
// 	T_model = 0.0;
// 	T_relaxation = 0.0;
// 	T_homo = 0.0;
//   T_FCLS = 0.;
// 	nbr_iter = 0;
// 	itmax = 0;
// 	BBNodeNum = 0;
// 	BestNodeNum = 0;
// 	nbr_update_Bsupp = 0;
// 	T_best_Bsupp = 0.0;
// 	CplexNodeNum = 0;
// 	CplexStatus = IloAlgorithm::Status::Unknown;
//   //cut = 0;
//   //warm_restart = true;
//   //cplex_branching_rule = 1;
//   //gap  = exp10(-8);
//   //tolX = 0;
//   //tolF = 0;
//   //TimeBBMax = 1000;
//
//   x_truth =x;
//   force_stop=false;
// }

void ContextData::reset(ProblemData* pb, vec x,std::string i_id,std::string s_id,bool verb) {
	it_num = 0;
	UB = exp10(10);
	feasible_solution_set = nullptr;
	problem = pb;
  //
  T_test = 0.0;
	T_model = 0.0;
	T_relaxation = 0.0;
	T_homo = 0.0;
  T_FCLS = 0.;
	nbr_iter = 0;
	itmax = 0;
	BBNodeNum = 0;
	BestNodeNum = 0;
	nbr_update_Bsupp = 0;
	T_best_Bsupp = 0.0;
#ifdef USE_CPLEX
	CplexNodeNum = 0;
	CplexStatus = IloAlgorithm::Status::Unknown;
#endif
  //cut = 0;
  //warm_restart = true;
  //cplex_branching_rule = 1;
  //gap  = exp10(-8);
  //tolX = 0;
  //tolF = 0;
  //TimeBBMax = 1000;

  x_truth = x;
  inst_id = i_id;
  set_id = s_id;
  force_stop=false;
  verbose = verb;
}
