#ifndef BBOPTIMIZER
#define BBOPTIMIZER 1

#include <exception>
#include "interfaces.hpp"
#include "Problem.hpp"
#include "Context.hpp"

using namespace std;

/** \file Optimizer.hpp
 * \brief Problem solver packed as an object.
 */

/**
 * Custom exception to warn for incomplete optimizer object while calling optimize.
 */
class missing_bb_operators: public exception
{
	/**
	 * Warns for lacking split or contract when calling optimize.
	 */
	virtual const char* what() const throw()
	{
		return "Missing Branch&Bound operator (either split or contract) when calling Optimizer.optimize";
	}
};


/**
 * An Optimizer object carries all the necessary operators to perform the optimization.
 * The Split and Contract operators do not depend on the problem, so they are
 * stored at the optimizer's construction.
 * As the bounds operators depends on the problem, they are created each time
 * we call the optimize() method.
 */
class Optimizer {

public:
	ContextData* context; /**< Context that will be filled during optimization. */
	UBInterface* computeUB; /**< Upper bounding operator, created for each optimize() call. */
	LBInterface* computeLB; /**< Lower bounding operator, created for each optimize() call. */
	SplitInterface* split; /**< Node split operator, filled once during optimizer construction. */
	ContractInterface* contract; /**< Node contraction operator, filled once during optimizer construction. */

	virtual ~Optimizer();
	Optimizer(ContextData* ctx = nullptr, SplitInterface* branch = nullptr, ContractInterface* ctrt = nullptr);
	/**
	 * Start the optimization process for a given problem.
	 *
	 * @param problem The problem instance to solve.
	 * @param rt The relaxation to use.
	 * @returns the solution antecedent.
	 */
	vec optimize(ProblemData& problem, RelaxationType rt, NodeContainerInterface& node_set);
	/**
	 * Start the optimization process for a given l2l0 problem under ASC and ANC constraints
	 *
	 * @param problem The problem instance to solve.
	 * @param rt The relaxation to use.
	 * @returns the abundance vector.
	 */
	vec optimize_ASC_ANC(ProblemData& problem, RelaxationType rt, NodeContainerInterface& node_set);


};

#endif
