#ifndef BBSPLIT
#define BBSPLIT 1

#include "Context.hpp"
#include "interfaces.hpp"

/** \file Split.hpp
	\brief Branching operators

	They are object functions sub-classing SplitInterface.
	They are doing, in a combined way, the choice of the
	branching variable(s), and the given split on that variable(s).
	The process is done on one node at a time, and it must
	return a correct node_list which will be iterated over.
	Usually, ones branch on a single variable, spltting the domain
	in two halves. But as long as the returned node_list is valid,
	you can use every branching and splitting strategy.
*/


/**
 * This class is a shorthand for the common use case of branching
 * on a single variable and splitting in two halves.
 * The only thing that needs to be defined in its sub-classes
 * is how to select a variable to branch on.
 */
class AbstractSplitInTwo : public SplitInterface {
protected:
	ContextData& ctx; /**< Context reference, not used for now. */
public:
	/**
	 * \brief The function operator defining the common branching choice.
	 *
	 * @param node the node to be splitted.
	 * @returns Two children, separated by
	 * branching on the variable indicated by getBranchingIndex..
	 */
	virtual node_list operator()(NodeInterface& node);
	/**
	 * \brief The variable to branch on.
	 *
	 * @param node the node to be splitted.
	 * @returns the index of the splitting variable.
	 */
	virtual int getBranchingIndex(NodeInterface& node) = 0;
	AbstractSplitInTwo(ContextData& context) : ctx(context) {};


};


/**
 * \brief Brannching strategy: max(abs(xi))
 */
class MaxXiBranchingRule : public AbstractSplitInTwo {
public:
	virtual int getBranchingIndex(NodeInterface& node);
	MaxXiBranchingRule(ContextData& context) : AbstractSplitInTwo(context) {}
};

/**
 * \brief Brannching strategy: max(abs(xi)) and xi is non-zero
 */
class MaxNZXiBranchingRule : public AbstractSplitInTwo {
public:
		virtual int getBranchingIndex(NodeInterface& node);
		MaxNZXiBranchingRule(ContextData& context) : AbstractSplitInTwo(context) {}
};

/**
 * \brief Branching strategy: min(abs(0.5 - bi))
 */
class MostUndeterminedXiBranchingRule : public AbstractSplitInTwo {
public:
	virtual int getBranchingIndex(NodeInterface& node);
	MostUndeterminedXiBranchingRule(ContextData& context) : AbstractSplitInTwo(context) {}
};

/**
 * \brief Branching strategy: max(lps_score)
 */
class MaxLPSBranchingRule : public AbstractSplitInTwo {
public:
	virtual int getBranchingIndex(NodeInterface& node);
	MaxLPSBranchingRule(ContextData& context) : AbstractSplitInTwo(context) {}
};

#endif
