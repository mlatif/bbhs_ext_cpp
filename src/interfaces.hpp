#ifndef BBINTERFACES
#define BBINTERFACES 1

#include <vector>
#include <armadillo>

using namespace arma;

/**
 * Just a list of indexes, corresponding to the variables
 * choosen to be 0, choosen to be not 0, and not choosen yet.
 */
typedef std::vector<int> support;

/**
 * A pair (x, val) for lower bound and upper bound, such that
 * val  = J(x), J being the cost function of the problem.
 */
typedef std::pair<vec, double> bound_type;

/**
 * The type of the problem to be solved.
 */
enum ProblemType {L2L0 = 1, L0L2 = 2, L2pL0 = 3, L2L0_ASC_ANC = 4};

/**
 * The relaxation to be used
 */
enum RelaxationType {Cplex = 1, Homotopy = 2, ActiveSet = 3, FCLS = 4, OSQP = 5, HYB_HOM_OSQP = 6, HYB_HOM_FCLS = 7};

/**
 * The problem parameter type, which is int for L2L0, and double for L2+L0 and L0L2.
 */
union ParamType
{
	unsigned int ui; /**< Integer type for the sparsity constraint (P2/0 problem). */
	double db; /**< Floating-point type for epsilon and lambda (P0/2 and P2+0 problem, respectively). */
};

/**
 * Formulation of the ASC constraint (only available for the L2L0_ASC_ANC)
 * Default value : Eq
 */
enum ASCFormulation {EQ = true, INEQ = false};
/**
	*
 */
enum HomotopyMovement {Init=0,ItO=1,OtI=2};

class NodeInterface;

/**
 * A straightforward node list to be iterated over after a node split.
 */
typedef std::vector<NodeInterface*> node_list;

/**
 * This is the interface of a node in the Branch&Bound tree,
 * which is basically a sub-domain of the original search space.
 * We are constrained by memory here. Consequently, some of the getters
 * will be transparently computed on-the-fly
 */
class NodeInterface {
public:
	// These methods should never call external node operations such as UB, LB, Split, ...
	virtual ~NodeInterface(){}
	virtual int getDepth() = 0; /**< Get the depth in the search tree. */
	virtual support getS0() = 0; /**< Get the 0-valued support. */
	virtual support getS1() = 0; /**< Get the not-null-valued support. */
	virtual support getSBar() = 0; /**< Get the to-be-determined support. */
	virtual int getK() = 0; /**< Get the sparsity level (size of S1). */
	virtual double getUBLS() = 0; /**< Get the least-square component of the upper bound. */
	virtual double getLBLS() = 0; /**< Get the least-square component of the lower bound. */
	virtual double getLBL1() = 0; /**< Get the l1-norm component of the lower bound. */
	virtual vec getLPS() = 0; /**< Get the l1 path selection score. */
	virtual mat getHtHinv() = 0; /**< get the inverse of H_{S_1}^T*H_{S_1}.*/
	virtual void setUB(double ub) {} /**< Set the upper bound value. */
	virtual void setLB(double lb) {} /**< Set the lower bound value. */
	virtual void setxUB(vec xub) {} /**< set the upper bound antecedent. */
	virtual void setxLB(vec xlb) {} /**< set the lower bound antecedent. */
	virtual void setUBLS(double ls) {} /**< Set the upper bound least-square component. */
	virtual void setLBLS(double ls) {} /**< Set the lower bound least-square component. */
	virtual void setLBL1(double l1) {} /**< Set the lower bound l1-norm component. */
	virtual void setS0(support s0) {} /**< Set the 0-valued support. */
	virtual void setS1(support s1) {} /**< Set the not-null-valued support. */
	virtual void setSBar(support sbar) {} /**< Set the to-be-determined support. */
	virtual void setLPS(vec lps) {} /**< Set the l1 path selection score. */
	virtual NodeInterface* duplicate(bool preComputedLB=false,bool preComputedUB=false) = 0; /**< Duplicate the given node, copying what's relevant (used to split nodes). For <b>l2l0_asc_anc</b>, the two boolean variables are used to disable the evaluation of bounds. */
	virtual void setFeasible(bool feasible) = 0; /**< Set the feasible flag. */
	virtual bool isFeasible() = 0; /**< Get the feasible flag. */
	virtual void setSolution(bool solution) = 0; /**< Set the solution flag. */
	virtual bool isSolution() = 0; /**< Get the solution flag. */
	// These methods are calling the correct object function
	virtual bound_type computeUB() = 0; /**< Direct call to the upper bound object function. */
	virtual double getUB() = 0; /**< Get the upper bound value. */
	virtual vec getxUB() = 0; /**< Get the upper bound antecedent. */
	virtual bound_type computeLB(vec xub) = 0; /**< Direct call to the lower bound object function. */
	virtual double getLB(bool old_is_good = false) = 0; /**< Get the lower bound value. */
	virtual vec getxLB(bool old_is_good = false) = 0; /**< Get the lower bpound antecedent. */
	virtual node_list split() = 0; /**< Direct call to the split object function. */
	virtual NodeInterface& contract() = 0; /**< Direct call to the contract object function */
	// Special functions for l2l0_asc_anc problem
	virtual unsigned long long int getIdx() = 0;	 /**< For <b>l2l0_asc_anc</b> problem, get the index of the current node. */
	virtual void setIdx(unsigned long long int) = 0; /**< For <b>l2l0_asc_anc</b> problem, set the index of the current node. */
	virtual support getSv()	= 0;
	virtual void setSv(support) = 0;
	virtual void updateSvNz(vec) = 0;
	virtual void resetSv() = 0;
	//
	virtual uvec getNZ() = 0;
	virtual void setNZ(uvec) = 0;
	//virtual void updateNZ(vec) = 0;
	virtual void resetNZ() = 0;
};

/**
 *  A node container (a given concrete subclass of this one) implements
 * a given strategy for the node exploration. For example, it could be a heap
 * sorting the nodes on their lower bound, which would implement the "best-first"
 * exploration strategy.
 */
class NodeContainerInterface {
public:
	virtual ~NodeContainerInterface(){}
	virtual NodeInterface* top() = 0; /**< Get the top-priority node. */
	virtual NodeInterface* pop() = 0; /**< Get and remove from container the top-priority node. */
	virtual bool push(NodeInterface* node) = 0; /**< Push a node to the container. */
	virtual double getLowestLB() = 0; /**< Get the lowest lower bound of all nodes in the container. */
	virtual void prune(double ub) = 0; /**< Remove all node whose lower bound is greater than the given threshold. */
	virtual bool empty() = 0; /**< Is the container empty ?*/
	virtual void reset() = 0; /**< Reset the container back to its construction state. */
	virtual void remove(NodeInterface* node) = 0; /**< Remove a node from the container. */
	/////////////////////////////////////////////////////////////////////////////
	virtual unsigned long long int size() = 0;
};

/**
 *  Beware, this is an interface and a function object. Several heuristics may be tested and used.
 * Upper Bound computation.
 *
 * @param node The node needing an upper bound computation.
 * @returns a pair (xUB, UB) containing the upper bound value (UB) and antecedent (xUB).
 */
class UBInterface {
public:
	virtual ~UBInterface(){}
	virtual bound_type operator()(NodeInterface& node) = 0;
};

/**
 * Beware, this is an interface and a function object. Several heuristics may be tested and used.
 * Lower Bound computation.
 *
 * @param node The node needing a lower bound computation.
 * @param xub The uppper bound antecedent of the node.
 * @returns a pair (xLB, LB) containing the upper bound value (LB) and antecedent (xLB).

 */
class LBInterface {
public:
	virtual ~LBInterface(){}
	virtual bound_type operator()(NodeInterface& node, vec xub) = 0;
};

/**
 * Beware, this is an interface and a function object. Several heuristics may be tested and used.
 * Node splitting into children.
 *
 * @param node The node needing a split.
 * @returns a list of children, result of the split.
 */

class SplitInterface {
public:
	virtual ~SplitInterface(){}
	virtual node_list operator()(NodeInterface& node) = 0;
};

/**
 * Beware, this is an interface and a function object. Several heuristics may be tested and used.
 * Heuristic to reduce the domain of a given node (like constraints propagation or screening).
 *
 * @param node The node to contract.
 * @returns the contracted node.
 */
class ContractInterface {
public:
	virtual ~ContractInterface(){}
	virtual NodeInterface& operator()(NodeInterface& node) = 0;
};

/**
 * This is an abstraction to measure time, to ease cross-platform programming.
 */
class ChronometerInterface {
public:
	virtual ~ChronometerInterface() {}
	virtual void start() = 0; /**< Starts the inner timer. */
	virtual double check() = 0; /**< Check the time elapsed since start() invocation. */
};


#endif
