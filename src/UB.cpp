#include "MimosaConfig.hpp"

#ifdef USE_CPLEX
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN
#else
using namespace std;
#endif

#include <utility>

#include "UB.hpp"
#include "Chronometer.hpp"
#include <dlib/optimization.h>
#include "Util.h"
#include <iostream>
#include <iomanip>      // std::setprecision
//
#include <qpOASES.hpp>
USING_NAMESPACE_QPOASES
//


/** \file UB.cpp - Upper Bound computations
 * Calculating the upper bound is quite simple for our problem:
 * we restrict the search space to the support S1 for the active atoms,
 * every other atom is considered inactive.
 * So, computing the upper bound is just a least-square inversion.
 */

common_return ContextAwareUB::commonUB(NodeInterface& node)
{
	vec w, xub;
	uvec Q1= conv_to <uvec>::from(node.getS1()),
	     Q0= conv_to <uvec>::from(node.getS0()),
	     Qv= conv_to <uvec>::from(node.getSBar());

	const ProblemData& pb = *(this->ctx->problem);
	mat A_Q1 = pb.H.cols(Q1);
	xub = zeros(pb.H.n_cols);

	// TODO: double-check the optimization done for l2l0 problem
	w = node.getHtHinv() * (A_Q1.t() * pb.y);

	if(!w.is_empty()) {
		if( pb.bigM < max(abs(w)) ){
			w = this->optim_QP_bigM(A_Q1, pb.y, pb.bigM);
		}
	}

	xub(Q1) = w;

	double least_square_term = pow(norm( (pb.y) - A_Q1 * w, 2), 2);
	node.setUBLS(least_square_term);
	unsigned int K = Q1.n_elem;
	common_return ret = std::make_pair(xub,std::make_pair(least_square_term, K));
	return ret;
}

vec DlibQPSolver::convDlibToArmaVec(dlib::matrix<double, 0, 1>& dlib_vec)
{
	const long dlib_nrows = dlib_vec.nr();
	vec arma_vec = vec(dlib_nrows);
	for (long r = 0; r < dlib_nrows; ++r)
		arma_vec[r] = dlib_vec(r);
	return arma_vec;
}

vec DlibQPSolver::operator()(mat H, vec y, double bigM)
{
	double eps = 1e-10;
	unsigned long maxiter = 30000;
	mat HtH = H.t()*H;
	mat b_arma = -H.t()*y;
	vec lower_arma = -arma::ones(H.n_cols, 1)*bigM;
	vec upper_arma = arma::ones(H.n_cols, 1)*bigM;
	dlib::matrix<double, 0, 0> Q = dlib::mat(HtH);
	dlib::matrix<double, 0, 1> b = dlib::mat(b_arma);
	dlib::matrix<double, 0, 1> alpha;
	alpha.set_size(HtH.n_cols);
	dlib::matrix<double, 0, 1> lower = dlib::mat(lower_arma);
	dlib::matrix<double, 0, 1> upper = dlib::mat(upper_arma);
	const unsigned long status = dlib::solve_qp_box_constrained(Q, b, alpha, lower, upper, eps, maxiter);
	if (status > maxiter)
		std::cout << "dlib solver: couldn't converge !" << std::endl;
	vec w = convDlibToArmaVec(alpha);
	return w;
}

#ifdef USE_CPLEX
vec CplexQPSolver::operator()(mat H, vec y, double bigM)
{
	//clock_t t0, t1;
	//t0=clock();
	ChronometerInterface* chrono = alloc_chrono();
	chrono->start();

	mat  HH;
	vec  Hy;

	HH = H.t() * H;
	Hy = H.t() * y;

	int Q = H.n_cols;
	//cout << "Q= "<<Q<<endl;

	vec w = zeros(Q);
	IloEnv env;

	try {
		IloModel model(env);
		IloCplex cplex(model);

		//-----------variables------------
		IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM

		model.add(x);
		//-----------Objective------------

		IloExpr objExpr(env);

		// -2 Y' * H * X
		for (int i = 0; i < Q; i++)
			objExpr += -2 * Hy(i) * x[i];

		// X'*HH*X
		for (int i = 0; i < Q; i++)
			for (int j = 0; j < Q; j++)
				objExpr += x[i] * HH(i, j) * x[j];

		IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
		model.add(obj);

		cplex.setParam(IloCplex::Threads, 1); // nombre de threads
		cplex.setOut(env.getNullStream());


		//t1=clock();
		//this->ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
		this->ctx->T_model += chrono->check();

		cplex.solve();

		IloNumArray vals(env);
		cplex.getValues(vals,x);

		for (int i = 0; i < Q; i++)
			w[i]=vals[i];

		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}
	delete chrono;
	return w;

}
#endif

bound_type UBL0L2::operator()(NodeInterface& node)
{
	common_return cr = this->commonUB(node);
	vec xub = cr.first;
	LsK ls_k = cr.second;
	double ls = ls_k.first;
	unsigned int k = ls_k.second;
	const ProblemData& pb = *(ctx->problem);
	double eps = pb.param.db;
	double ub = ls < eps ? (double) k : exp10(10);

	node.setxUB(xub);
	node.setUB(ub);
	return make_pair(xub, ub);
}

bound_type UBL2L0::operator()(NodeInterface& node)
{
	common_return cr = this->commonUB(node);
	vec xub = cr.first;
	LsK ls_k = cr.second;
	double ls = ls_k.first;
	unsigned int k = ls_k.second;
	const ProblemData& pb = *(ctx->problem);
	unsigned int targetK = pb.param.ui;
	double ub = k <= targetK ? ls : exp10(10);

	node.setxUB(xub);
	node.setUB(ub);
	return make_pair(xub, ub);
}

bound_type UBL2pL0::operator()(NodeInterface& node)
{
	common_return cr = this->commonUB(node);
	vec xub = cr.first;
	LsK ls_k = cr.second;
	double ls = ls_k.first;
	unsigned int k = ls_k.second;
	const ProblemData& pb = *(ctx->problem);
	double lambda = pb.param.db;
	double ub = ls + lambda * (double)k;

	node.setxUB(xub);
	node.setUB(ub);
	return make_pair(xub, ub);
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

// **************** P_2/0 + ASC + ANC problem ****************
#ifdef USE_CPLEX
common_return ContextAwareUB_ASC_ANC_QPCplex::commonUB_ASC_ANC(NodeInterface& node) {
		const ProblemData& pb = *(this->ctx->problem);
		vec xub;
		double least_square_term(0);
		uvec Q1= conv_to <uvec>::from(node.getS1());
		if (node.getIdx() == 0 || Q1.n_elem == 0) {
				//cout << "-----> node->getIdx() == 0 || Q1.n_elem == 0" << endl;
				xub = zeros(pb.H.n_cols);
				least_square_term = pow(norm(pb.y,2),2);				// cout<< "||y||_2^2 = " << pow(norm(pb.y,2),2) << endl;
		} else {
				xub = this->optim_QPC_ASC_ANC(pb.H,pb.y,sort(Q1),node.getIdx());
				least_square_term = pow(norm( (pb.y) - pb.H * xub, 2), 2);
		}
		//
		node.setUBLS(least_square_term);
		unsigned int K = pb.param.ui;
		common_return ret = std::make_pair(xub,std::make_pair(least_square_term, K));
		return ret;
}

bound_type UBL2L0_ASC_ANC_QPCplex::operator()(NodeInterface& node) {
		//cout << ">> UBL2L0_ASC_ANC::operator() - node "<< node.getIdx() << endl;
		const ProblemData& pb = *(ctx->problem);
		unsigned int targetK = pb.param.ui;
		common_return cr = this->commonUB_ASC_ANC(node);
		vec xub = cr.first;
		LsK ls_k = cr.second;
		double ls = ls_k.first;
		unsigned int k = ls_k.second;
		double ub = k <= targetK ? ls : exp10(10);
		node.setxUB(xub);
		node.setUB(ub);
		return make_pair(xub, ub);
}

vec CplexQPSolver_ASC_ANC::operator()(mat H, vec y, uvec Q1,unsigned long long int n){
		//cout << ">>>> Cplex_UB_operator()" << endl;
		bool ASC_asEq(ctx->problem->get_ASC_formulation());
		ChronometerInterface* chrono = alloc_chrono();
		chrono->start();

		mat HtH(this->ctx->problem->HtH), sub_HtH(HtH(Q1,Q1));
		vec Hty(H.t() * y);
		vec x_relache = zeros(H.n_cols);

		//cout << "ASC_asEq = " << ASC_asEq << endl;
		IloNum lb_ASC(1.);
		if (!ASC_asEq){ lb_ASC = 0.;}
		//cout << "lb_ASC = " << lb_ASC << endl;
		//pause_cin_txt("UB = CplexQPSolver_ASC_ANC");

		//
		IloEnv env;
		//
		IloModel model(env);
		IloCplex cplex(model);
		//
		IloNumVarArray x(env,Q1.n_elem,0,1);
		for (int i; i <Q1.n_elem; ++i ) {	std::stringstream name;	name << "x" << Q1(i);	x[i] = IloNumVar(env, name.str().c_str());	}
		model.add(x);
		//
		IloExpr objExpr(env);
		//
		try {
				for (int i(0); i<Q1.n_elem;++i) {
						objExpr+= -2*Hty(Q1(i))*x[i];
						for (int j(0); j<Q1.n_elem; ++j) {
								objExpr+= x[i]*sub_HtH(i,j)*x[j];
						}
				}
				IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
				model.add(obj);
				//
				//IloRange ctr_sum(env, 0, IloSum(x), 1, "ctr_sum");
				IloRange ctr_sum(env, lb_ASC, IloSum(x), 1, "ctr_sum");

				model.add(ctr_sum);
				//
				for (int i(0); i < Q1.n_elem; ++i){
						std::stringstream name_bnd;
						name_bnd << "ctr_BND_" << Q1(i);
						model.add(IloRange(env, 0,x[i],1,name_bnd.str().c_str()));
				}
				//
				cplex.setOut(env.getNullStream());
				cplex.setParam(IloCplex::Threads, 1); // nombre de threads
				//
				this->ctx->T_model += chrono->check();
				//
				cplex.solve();
				//
				IloNumArray vals(env);
				cplex.getValues(vals,x);
				for (int i = 0; i < Q1.n_elem; ++i){
						x_relache[Q1(i)]=vals[i];
						//cout << "Q1 = " << Q1(i) << " => " << vals[i] << endl;
				}
				//
				// std::stringstream file_name;
				// file_name << "l2l0_ASC_ANC_ub_node_" << n <<".lp";
				// cplex.exportModel(file_name.str().c_str());
				env.end();
		} catch (IloException & e) {
			cerr << " ERREUR : exception = " << e << endl;
		}
		delete chrono;
		return x_relache;
}
#endif // USE_CPLEX

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
common_return ContextAwareUB_ASC_ANC_FCLS::commonUB_ASC_ANC(NodeInterface& node){
		const ProblemData& pb = *(this->ctx->problem);
		double least_square_term(0);
		vec xub;
		uvec Q1= conv_to <uvec>::from(node.getS1());
		//cout << node.getIdx() << " UB / " << Q1.n_elem << endl;



				// if (node.getIdx()==4607182418732908541) {
				// 	string tok("UB");
				// 	vec m_y = conv_to<vec>::from(pb.y);
				// 	mat m_H = pb.H;
				// 	mat m_Q = conv_to<mat>::from(Q1);
				// 	std::stringstream file_y;	file_y << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_y.txt";
				// 	std::stringstream file_H;	file_H << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_H.txt";
				// 	std::stringstream file_Q;	file_Q << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_Q.txt";
				// 	m_y.save(file_y.str(),raw_ascii);
				// 	m_Q.save(file_Q.str(),raw_ascii);
				// 	m_H.save(file_H.str(),raw_ascii);
				//
				// 	this->ctx->force_stop = true;
				// 	//pause_cin_txt("Stop");
				//
				// }









		//
		if (node.getIdx() == 0 || Q1.n_elem == 0) {
				xub = zeros(pb.H.n_cols);
				least_square_term = pow(norm(pb.y,2),2);				// cout<< "||y||_2^2 = " << pow(norm(pb.y,2),2) << endl;
		} else {
				ChronometerInterface * chrono = alloc_chrono();
				chrono->start();
				xub = this->optim_FCLS_ASC_ANC(pb.H,pb.y,sort(Q1),node.getIdx());
				this->ctx->T_FCLS += chrono->check();
				delete chrono;
				least_square_term = pow(norm( (pb.y) - pb.H * xub, 2), 2);
		}
		//
		node.setUBLS(least_square_term);
		//
		unsigned int K = pb.param.ui;
		common_return ret = std::make_pair(xub,std::make_pair(least_square_term, K));
		// -----------------------ABSURDITE-----------------------------------------
		bool absurde(false);
		bool negX(false);





		// cout << "SELECTION_FCLS (UB)\tSUM = " << sum(xub) << endl;
		for (int i(0); i < xub.size(); ++i) {
				//if (xub[i] != 0) {	string tok = xub[i] < 0 ? "-":"+";	cout << "\t"<<tok<<"\tx["<<i<<"] = " << xub[i] << endl;	}
				if (xub[i] < 0 ) {negX = true;}
		}	//cout << endl;
		//TODO : Créer le répertoire NEG_INST_FCLS
		if(negX) {
			string tok("UB");
			vec m_y = conv_to<vec>::from(pb.y);
			mat m_H = pb.H;
			mat m_x = conv_to<mat>::from(xub);
			mat m_Q = conv_to<mat>::from(Q1);

			std::stringstream file_y;	file_y << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_y.txt";
			std::stringstream file_H;	file_H << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_H.txt";
			std::stringstream file_x;	file_x << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_x.txt";
			std::stringstream file_Q;	file_Q << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_Q.txt";
			m_y.save(file_y.str(),raw_ascii);
			m_Q.save(file_Q.str(),raw_ascii);
			m_H.save(file_H.str(),raw_ascii);
			m_x.save(file_x.str(),raw_ascii);

			this->ctx->force_stop = true;
			//abort();
		}







		return ret;
}

bound_type UBL2L0_ASC_ANC_FCLS::operator()(NodeInterface& node) {
		const ProblemData& pb = *(ctx->problem);
		unsigned int targetK = pb.param.ui;
		common_return cr = this->commonUB_ASC_ANC(node);
		vec xub = cr.first;
		LsK ls_k = cr.second;
		double ls = ls_k.first;
		unsigned int k = ls_k.second;
		double ub = k <= targetK ? ls : exp10(10);
		node.setxUB(xub);
		node.setUB(ub);
		return make_pair(xub, ub);
}

vec FCLSSolver_ASC_ANC::operator()(mat H, vec y, uvec Q1,unsigned long long int n) {
		//bool verbose(n==4607182418732908541);
		bool verbose(false);
		if(verbose){cout << ">>>> FCLS_ASC_ANC::UB_operator()" << endl;}
		// ChronometerInterface* chrono = alloc_chrono();
		// chrono->start();
		//
		mat H_Qv = H.cols(Q1);
		//
		vec x_relache = zeros(H.n_cols);
		//
		double tol(exp10(-9)),delta(1./10./max(y)),sumIEtEOne,lambdiv;
		int N(H_Qv.n_rows),Q(H_Qv.n_cols),iter(0),iter_ow(0);
		mat dH,E,EtE,iEtE;
		vec One(ones(Q,1)), iEtEOne(zeros(Q,1)),W(zeros(Q,1)),dy(zeros(N,1)),f(zeros(N+1,1)),Etf(zeros(Q,1)),ls(zeros(Q,1)),x(zeros(Q,1)),x_Old(zeros(Q,1));
		//
		dH = delta*H_Qv;
		E = join_cols(One.t(),dH);
		EtE = E.t()*E;
		iEtE = inv(EtE);
		iEtEOne = iEtE*One;
		sumIEtEOne = sum(iEtEOne);
		W = iEtE.diag();
		dy = delta*y;
		f = join_cols(ones(1),dy);
		Etf = E.t()*f;
		//
		ls = iEtE*Etf;
		lambdiv = (-(1.-(ls.t()*One)) /sumIEtEOne).eval()(0,0);
		x = ls - lambdiv*iEtEOne;
		x_Old = x;
		//
		mat L;																		// Création de la matrice L pour le calcul des lagrangiens
		mat sL1,sL2,cL;															// Sauvegarde de la matrice L hors (sL1) et dans (sL2) la boucle while d'exclusion des max_neg; cL la matrice sur laquelle on effectue le test d'arrêt
		vec x_e,lag,max_neg;
		uword idx_max_neg;												// index du "most positive" lambda (while 2)
		uvec R, P(regspace<uvec>(0,Q-1));
		//
		if(verbose){cout << "R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
		if(verbose){cout << "P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
		if(verbose){cout <<"------" << endl;}
		// --------------------- //
		if (!find(x < -tol).is_empty()) {
			iter = 0;
			R.clear();
			// --------------------- //
			// Ici la boucle d'élimination
			//while (!find(x < -tol  && abs(x)>1e-5 ).is_empty()) {
			while (!find(x < -tol).is_empty()) {
					//cout << "node = " << n << endl;
					// On ne peut pas déplacer de P dans R un indice qui n'est pas dans P ...
					// => Provient de merde numériques à 10e-9 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					//deplacer(find(x < -tol && abs(x)>1e-5), &P, &R,"P->R");
					deplacer(find(x < -tol), &P, &R,"P->R");
					R = sort(R);
					if(verbose){cout << ">R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
					if(verbose){cout << ">P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
					// Reset x
					x = x_Old;
					L = getFCLS_Matrix(R,iEtE,iEtEOne,One);
					sL1.clear();	sL1 = L;
					x_e = x(R); x_e.insert_rows(x_e.n_rows,1);
					lag = inv(L)*x_e;
					if (verbose) {lag.t().print("lag");}
					if (verbose) {pause_cin_txt("W1c- Next step ? ");}
					//while (!find(lag.rows(0,R.size()-1)>0).is_empty()) {
					while (!find(lag.rows(0,R.size()-1)>exp10(-6)).is_empty()) {		// PATCH SEB
							// cout << ">>|R| = " << R.size() << endl << ">>|P| = " << P.size() << endl;
							//max_neg = W(R)%lag.rows(0,R.size()-1);		// %	: element-wise multiplication of two objects
							max_neg = lag.rows(0,R.size()-1);		// %	: element-wise multiplication of two objects
							idx_max_neg = max_neg.index_max();
							if(verbose){cout << "idx_max_neg => " << idx_max_neg << "\t R(idx_max_neg) = " << R(idx_max_neg) << "\t max_neg = "<< max_neg(idx_max_neg) <<endl;}
							deplacer(R(idx_max_neg),&R,&P,"R->P");
							//cout << "\n" << endl;
							if(verbose){cout << ">>R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
							if(verbose){cout << ">>P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
							//
							L = getFCLS_Matrix(R,iEtE,iEtEOne,One);
							sL2.clear();	sL2 = L;
							//
							x_e = x(R); x_e.insert_rows(x_e.n_rows,1);
							lag = inv(L)*x_e;
							if (verbose) {lag.t().print("lag_W2");}
							if(verbose){pause_cin_txt("While_2 - NEXT ! ");}
					}
					if (!R.is_empty()) {
							x = x - iEtE.cols(R)*lag.rows(0,R.size()-1) - lag(R.size())*iEtEOne;
							if (verbose) {x.t().print("x_W1");}
					}
					iter++;

					if ( verbose && iter % 100000 == 0) {
							cout << "UB \t ITER = " << iter <<"\t node "<< n <<endl;
					}

					// Le cas de "base" est L = sL2;
					cL = sL2;
					if (L.n_rows == sL1.n_rows) {	cL = sL1;}
					//

					// CETTE CONDITION D'ARRET FAIT APPARAITRE DES VALEURS NEGATIVES ....
					if (all(all(iter > cL))) {
							//pause_cin_txt("all(all(iter > cL)) - next ? ");
							//break;
					}

					//if(verbose){pause_cin_txt("While_1 - NEXT ! ");}
			}
			// --------------------- //
			iter_ow++;
			//cout << "ITER_OW = " << iter_ow << endl;
			if (all(all(iter_ow > 10*cL))) {
					//pause_cin_txt("all(all(iter_ow > 10*cL)) - next ? ");
					//break;		// Pas dans une boucle ... quel intérêt ??
			}

		}	 // 	if (!find(x < -tol).is_empty()) {

		uvec neglig = find(abs(x)<1e-7);
		x.elem(neglig) = zeros(neglig.size());

		for (int i = 0; i < Q1.n_elem; ++i){
				x_relache[Q1(i)]=x[i];
		}


		if(verbose){pause_cin_txt("FIN FCLS(UB)");}
		//delete chrono;

		//return x_relache;
		return x_relache;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
// OPERATEUR UNIQUE O.pen S.ource Q.uadratic P.rogramming
common_return ContextAwareUB_ASC_ANC_OSQP::commonUB_ASC_ANC(NodeInterface& node) {
		//cout << "ContextAwareUB_ASC_ANC_OSQP::commonUB_ASC_ANC" << endl;
		const ProblemData& pb = *(this->ctx->problem);
		unsigned int K = pb.param.ui;
		vec xub;
		double least_square_term(0);
		uvec Q1= conv_to <uvec>::from(node.getS1());
		if (node.getIdx() == 0 || Q1.n_elem == 0) {
				//cout << "-----> node->getIdx() == 0 || Q1.n_elem == 0" << endl;
				xub = zeros(pb.H.n_cols);
				least_square_term = pow(norm(pb.y,2),2);				// cout<< "||y||_2^2 = " << pow(norm(pb.y,2),2) << endl;
		} else {
				xub = this->optim_OSQP_ASC_ANC(pb.H,pb.y,sort(Q1),node.getIdx());
				least_square_term = pow(norm( (pb.y) - pb.H * xub, 2), 2);
		}
		node.setUBLS(least_square_term);
		common_return ret = std::make_pair(xub,std::make_pair(least_square_term, K));
		return ret;
}
//
bound_type UBL2L0_ASC_ANC_OSQP::operator()(NodeInterface& node) {
		//cout << "UBL2L0_ASC_ANC_OS::operator()" << endl;
		const ProblemData& pb = *(ctx->problem);
		unsigned int targetK = pb.param.ui;
		common_return cr = this->commonUB_ASC_ANC(node);
		vec xub = cr.first;
		LsK ls_k = cr.second;
		double ls = ls_k.first;
		unsigned int k = ls_k.second;
		double ub = k <= targetK ? ls : exp10(10);
		node.setxUB(xub);
		node.setUB(ub);
		return make_pair(xub, ub);
}

vec OSQPSolver_ASC_ANC::operator()(mat H, vec y, uvec Q1,unsigned long long int n) {
		//cout << ">>>> OS_UB_operator()" << endl;
		bool ASC_asEq(ctx->problem->get_ASC_formulation());
		//cout << "ASC_asEq = " << ASC_asEq << endl;

		ChronometerInterface* chrono = alloc_chrono();
		chrono->start();
		//
		double eps = 1e-10;
		unsigned long maxiter = 30000;
		//
		mat HtH(this->ctx->problem->HtH), sub_HtH(HtH(Q1,Q1));
		int nV(sub_HtH.n_cols),nC(1);
		vec Hty(-1*H.cols(Q1).t() * y),rows_sub_HTH(reshape(sub_HtH,nV*nV,1));
		vec lower_arma(zeros(sub_HtH.n_cols, 1)),upper_arma(ones(sub_HtH.n_cols, 1));
		vec A_arma = arma::ones(sub_HtH.n_rows,1);				// La matrice des contraintes
		vec x_relache = zeros(H.n_cols);
		// Initialisation qpOASES
		real_t Q[nV*nV];								// qpOASES::real_t Q[nV*nV];
		real_t A[nV];										// qpOASES::real_t A[nV];
		real_t g[nV];										// qpOASES::real_t g[nV];
		real_t lb[nV];								 	// qpOASES::real_t lb[nV];
		real_t ub[nV];									// qpOASES::real_t ub[nV];
		real_t ubA[1] = { 1. };					// qpOASES::real_t ubA[1] = { 1. };

		real_t lbA[1] = { 1. };					// qpOASES::real_t lbA[1] = { 0. };
		// ATTENTION => lbA[1] définit le vecteur de taille 1
		// Pour y acceder, c'est avec par indicage au départ de 0.
		if (!ASC_asEq){ lbA[0] = 0.;}
		//cout << "lbA = " << lbA[0] << endl;
		//pause_cin_txt("UB = OSQPSolver_ASC_ANC::operator");


		int_t nWSR = 10;								// qpOASES::int_t nWSR = 10;
		nWSR = 5 *(nV + nC);						// Manal - default value;
		// Conversion
		for (int k(0); k<rows_sub_HTH.n_rows; ++k) {
				Q[k] = rows_sub_HTH[k];
		}
		for (int k(0); k<Hty.n_rows; ++k) {
				g[k] = Hty[k];
		}
		for (int k(0);k<lower_arma.n_rows;++k) {
				lb[k] = lower_arma[k]; 	ub[k] = upper_arma[k];
		}
		//A_arma.print("A_arma");
		for (int k(0); k<A_arma.n_rows; ++k) {
				A[k] = A_arma[k];
		}
		// qpOASES::QProblem P(nV,nC,qpOASES::HST_UNKNOWN,qpOASES::BT_TRUE);
		QProblem P(nV,nC,HST_UNKNOWN,BT_TRUE);
		Options myOptions;
		myOptions.printLevel = PL_LOW;
		P.setOptions(myOptions);
		//

		// Temporaire   SUPPRIMER
		//double save_chrono(chrono->check());
		//vec tempo_T(1); tempo_T.at(0) = save_chrono;
		//this->ctx->T_model += save_chrono;
		// Original
		this->ctx->T_model += chrono->check();


		//
		real_t xOpt[nV];							//qpOASES::real_t xOpt[nV];
		//
		P.init( Q,g,A,lb,ub,lbA,ubA, nWSR );
		P.getPrimalSolution( xOpt );
		//
		for (int i = 0; i < Q1.n_elem; ++i){
				x_relache[Q1(i)]=xOpt[i];
		}
		//x_relache.print("x_R");

		// Demande de Said et seb pour l'execution
		// if (false) {
		// 		string tok("UB");
		// 		stringstream file; file <<"COLLECT_USGS_NODES/" <<this->ctx->inst_id <<"_"<<tok<<"_"<<n<<"_x.txt";
		// 		stringstream file_H; file_H <<"COLLECT_USGS_NODES/" <<this->ctx->inst_id <<"_"<<tok<<"_"<<n<< "_H.txt";
		// 		stringstream file_y; file_y <<"COLLECT_USGS_NODES/" <<this->ctx->inst_id <<"_"<<tok<<"_"<<n<< "_y.txt";
		// 		stringstream file_Q; file_Q <<"COLLECT_USGS_NODES/" <<this->ctx->inst_id <<"_"<<tok<<"_"<<n<< "_Q.txt";
		// 		stringstream file_T; file_T <<"COLLECT_USGS_NODES/" <<this->ctx->inst_id <<"_"<<tok<<"_"<<n<< "_T.txt";
		// 		x_relache.save(file.str(),raw_ascii);
		// 		H.save(file_H.str(),raw_ascii);
		// 		y.save(file_y.str(),raw_ascii);
		// 		Q1.save(file_Q.str(),raw_ascii);
		// 		tempo_T.save(file_T.str(),raw_ascii);		// REMETTRE LE VRAI CHRONO
		// }



		// if (abs(sum(x_relache)-1) > 1e-10) {
		// 		cout << "/\\ UB \t" << this->ctx->inst_id << " \t "<< n <<" \t " << sum(x_relache) << endl;
		// 		//pause_cin_txt("UB NOT 1");
		// }



		delete chrono;
		return x_relache;
}


/*
// OPERATEUR HYBRIDE (Pour comparer les résultats Cplex qpOASES)
common_return ContextAwareUB_ASC_ANC_OSQP_Cplex::commonUB_ASC_ANC(NodeInterface& node) {
		cout << "ContextAwareUB_ASC_ANC_OSQP_CPLEX::commonUB_ASC_ANC" << endl;
		const ProblemData& pb = *(this->ctx->problem);
		unsigned int K = pb.param.ui;
		vec xub,xub_C,xub_D;
		double least_square_term(0);
		uvec Q1= conv_to <uvec>::from(node.getS1());

		if (node.getIdx() == 0 || Q1.n_elem == 0) {
				//cout << "-----> node->getIdx() == 0 || Q1.n_elem == 0" << endl;
				xub = zeros(pb.H.n_cols);
				least_square_term = pow(norm(pb.y,2),2);				// cout<< "||y||_2^2 = " << pow(norm(pb.y,2),2) << endl;
		} else {
				xub_D = this->optim_OSQP_ASC_ANC(pb.H,pb.y,sort(Q1),node.getIdx());
				//xub_C = this->optim_QPCplex_ASC_ANC(pb.H,pb.y,sort(Q1),node.getIdx());
				//this->ctx->main_solver.push_back(pow(norm( (pb.y) - pb.H * xub_D, 2), 2));
				//this->ctx->other_solver.push_back(pow(norm( (pb.y) - pb.H * xub_C, 2), 2));
				// cout<<"UB_C = " << pow(norm( (pb.y) - pb.H * xub_C, 2), 2) << endl;
				// cout<<"UB_D = " << pow(norm( (pb.y) - pb.H * xub_D, 2), 2) << endl;
				// for (int k(0); k<Q1.n_elem; ++k) {
				// 		cout << Q1(k) << "\t => C " << xub_C(Q1(k)) << "\t D " << xub_D(Q1(k)) << endl;
				// }
				// cout << "sum(C) = " << sum(xub_C) << "\t sum(D) = " << sum(xub_D) << endl;
				//pause_cin_txt("COMPARAISON RESULTATS ");
				xub = xub_D;
				least_square_term = pow(norm( (pb.y) - pb.H * xub, 2), 2);
		}
		node.setUBLS(least_square_term);
		common_return ret = std::make_pair(xub,std::make_pair(least_square_term, K));
		return ret;
}
// OPERATEUR HYBRIDE (Pour comparer les résultats Cplex qpOASES)
bound_type UBL2L0_ASC_ANC_OSQP_QPCplex::operator()(NodeInterface& node) {
		cout << "UBL2L0_ASC_ANC_OS_QPC::operator()" << endl;
		const ProblemData& pb = *(ctx->problem);
		unsigned int targetK = pb.param.ui;
		common_return cr = this->commonUB_ASC_ANC(node);
		vec xub = cr.first;
		LsK ls_k = cr.second;
		double ls = ls_k.first;
		unsigned int k = ls_k.second;
		double ub = k <= targetK ? ls : exp10(10);
		node.setxUB(xub);
		node.setUB(ub);
		return make_pair(xub, ub);
}
*/
