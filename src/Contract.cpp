#include "Contract.hpp"

/**
 * This is implementing lasso safe screening as defined by
 * Laurent El Ghaoui, Vivian Viallon, Tarek Rabbani in :
 * Safe Feature Elimination for the LASSO and Sparse Supervised Learning Problems
 * (https://arxiv.org/pdf/1009.4219.pdf)
 *
 * Tailored to our problem (that is, we don't try to zero components which are part of S1).
 */
NodeInterface& L2pL0L1Screening::operator()(NodeInterface& node)
{
        ProblemData& pb = *(this->ctx.problem);
        vec xub = node.getxUB();

        support sbar = node.getSBar();
        support s1 = node.getS1();
        support s0 = node.getS0();

        uvec Q1 = conv_to<uvec>::from(s1);
        uvec Qv = conv_to<uvec>::from(sbar);
        mat B = pb.H.cols(Q1);

        vec Hy = abs(pb.H.t() * (pb.y - (B * xub(Q1))));
        uword I0 = Hy.index_max();
        double lambda_max = Hy.at(I0);

	colvec H_c_n_as_col = pb.H_columnwise_norm_as_column;

	colvec y_norm = sqrt(pb.sqrt_yty);

        colvec y_hk = y_norm(0) + H_c_n_as_col(Qv);

        colvec rho_vec = (y_hk + Hy(Qv)) / (y_hk + lambda_max);

        colvec lambda_safe = rho_vec * lambda_max;

        double lambda = pb.param.db;

        for (unsigned int i = 0; i < lambda_safe.n_rows; ++i) {
                if (lambda > lambda_safe(i)) {
                        unsigned int atom_index = Qv(i);
                        // This is inefficient: the efficient way is to erase only after the loop,
                        // having kept track of the past-the-end iterators returned by remove.
                        sbar.erase(remove(sbar.begin(), sbar.end(), atom_index), sbar.end());
                        s0.push_back(atom_index);
                }
        }

        node.setSBar(sbar);
        node.setS0(s0);

        return node;
}
