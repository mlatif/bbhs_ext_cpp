//============================================================================
// Name        : BranchandBound_l2l0M.cpp
// Author      : ramzi
// Version     :
// Copyright   : Your copyright notice
// Description : BranchandBound algorithm in C++, Ansi-style
//============================================================================
#include <iostream>
#include <vector>
#include <math.h>
#include "armadillo"
#include <time.h>
#include <sstream>
#include <string>

#include "MimosaConfig.hpp"

#ifdef USE_CPLEX
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN
#else
using namespace std;
#endif

//#include "model.h"
#include <fstream> // Stream class to both read and write from/to files.

using namespace arma;
#include "Util.h"
#include "Problem.hpp"
#include "Context.hpp"
#include "Optimizer.hpp"
#include "Split.hpp"
#include "Contract.hpp"
#include "CplexSolver.hpp"
#include "Chronometer.hpp"
#include "node_container.hpp"


/************************    Principal Program    ****************************************/
struct parsed_range {
	unsigned int start, end, step;
};

/**
 * Show program usage, with the tons of arguments needed in the correct order.
 */
void show_usage(const string&);

/**
 * Parse a format which can be a single number, or matlab style start:end and start:step:end number ranges.
 * @param user_range The string supplied in the command line arguments.
 * @returns a range structure with start, stop and step correctly filled.
 */
parsed_range parse_range(const string&);
/**
	* Implemntation of user friendly interface to run the program with user's input
*/
void user_interface();


/**
 * Get an instance name from a path. This is basically basename, but instead
 * of a file we retrieve the last directory of the path.
 */
string get_instance_name(const string& path);
/************************    Principal Program    ****************************************/
int main(int argc, char **argv) {

	bool opt_relaxation_with_cplex(0),opt_relaxation_with_actset(0),opt_relaxation_with_hom(0),opt_with_cplex(0);
	bool warm_restart(1);
	// **********************************************************************************************
	// ASC ANC Case
	bool opt_relaxation_with_qpoases(false), opt_relaxation_with_fcls(false);
	bool opt_relaxation_with_hom_qpoases(false), opt_relaxation_with_hom_fcls(false);
	// **********************************************************************************************

	//
	mat A;
	vec y;
	vec x_opt;
	double norm2carre_y = 0;
//	sp_mat eyeN;
	//mat AA;
	double BigM=0;
	double born_sup= exp10(10);

	vec x_truth;

	//clock_t t_, t2_;
	double execution_time = 0;
	ChronometerInterface* chrono;

	// Let's do something very dirty for arguments:
	// [cmd] ProblemType SolverType ExplorationStrategy ExplorationStrategyArgument K
	// with everything positional and:
	// - ProblemType in {l2l0, l2pl0_with_SBR, l2pl0_without_SBR, l0l2}
	// - SolverType in {full_cplex, bb_cplex, bb_homotopy, bb_activeset_warm, bb_activeset_cold}
	// - ExplorationStrategy in {stack, heap_on_lb, heap_on_l1, stack_then_heap_on_lb_iteration_threshold, stack_then_heap_on_l1_iteration_threshold}
	// - ExplorationStrategyArgument being unused for stack and heap (lb and l1), being the iteration triggering the strategy change for stack_then_heap_on_*_iteration_threshold
	string Hmatrix_file_path;
	string instances_path;
	string problem_type;
	string solver_type;
	string strategy_type;
	char* strategy_argument;
	//parsed_range snr_range, k_range, instance_range;
	//////////////////////////////////////////////////////////////////////////////
	if (argc < 6) {
		if (argc < 5) {
			if (argc < 4) {
				if (argc < 3) {
					if (argc < 2) {
						cerr << "ERROR: No problem type supplied !" << endl;
					} else {
						cerr << "ERROR: No solver choice provided !" << endl;
					}
				} else {
					cerr << "ERROR: No exploration strategy selected !" << endl;
				}
			} else {
				cerr << "ERROR: no argument for the exploration strategy !" << endl;
			}
		} else {
			cerr << "ERROR: No K provided !" << endl;
		}
		show_usage(string(argv[0]));
		exit(1);
	}

	//Hmatrix_file_path = argv[1];
	//instances_path = argv[2];
	//if (instances_path[instances_path.size() -1] != '/') {
	//	instances_path.push_back('/');
	//}
	stringstream pathdata, pathdata_y, pathdata_eps, pathdata_lambda, pathdata_x0_sbr_2p0, pathdata_out;
	string	version ="";

	bool L2L0(0), L2pL0(0), L0L2(0);
	bool init_SBR=0;
	// **********************************************************************************************
	// ASC ANC Case
	string results_path("ASC_ANC_RES/");	//
	bool L2L0_ASC_ANC(false);
	stringstream pathdata_res,pathdata_asc_Hmatrix,pathdata_xtruth;
	stringstream inst_asnc_name; 	// cas particlulier du 0-branching
	// Just for ASC ANC - by default, we use an equality form for the ASC constraint (optionnal param)
	bool ASC_eq(true), verbose(false);


	// **********************************************************************************************

	// - ProblemType in {l2l0, l2pl0_with_SBR, l2pl0_without_SBR, l0l2}
	problem_type = argv[1];
	if (problem_type == "l0l2")
		L0L2 = 1;
	else if (problem_type == "l2l0")
		L2L0 = 1;
	else if (problem_type == "l2pl0_with_SBR") {
		L2pL0 = 1;
		init_SBR = 1;
	} else if (problem_type == "l2pl0_without_SBR") {
		L2pL0 = 1;
		init_SBR = 0;
	} else if (problem_type == "l2l0_asc_anc") {
		L2L0_ASC_ANC = 1;
	} else {
		cerr << "ERROR: invalid value for problem type." << endl;
		show_usage(string(argv[0]));
		exit(2);
	}

	// - SolverType in {full_cplex, bb_cplex, bb_homotopy, bb_activeset_warm, bb_activeset_cold}
	solver_type = argv[2];
#ifdef USE_CPLEX
	if (solver_type == "full_cplex") {
		opt_with_cplex = 1;
	} else if (solver_type == "bb_cplex") {
		opt_relaxation_with_cplex = 1;
	} else
#endif
	if (solver_type == "bb_homotopy") {
		opt_relaxation_with_hom = 1;
	} else if (solver_type == "bb_activeset_warm") {
		opt_relaxation_with_actset = 1;
		warm_restart = 1;
	} else if (solver_type == "bb_activeset_warm") {
		opt_relaxation_with_actset = 1;
		warm_restart = 0;
	} else if (solver_type == "bb_fcls") {
		opt_relaxation_with_fcls = 1;
	} else if (solver_type == "bb_qpoases") {
		opt_relaxation_with_qpoases = 1;
	} else if (solver_type == "bb_homotopy_qpoases") {
		opt_relaxation_with_hom_qpoases = 1;
	} else if (solver_type == "bb_homotopy_fcls") {
		opt_relaxation_with_hom_fcls = 1;
	} else {
		cerr << "ERROR: invalid solver type provided !" << endl;
		show_usage(string(argv[0]));
		exit(2);
	}

	// Simple check to avoid crazy things afterwards
	if (opt_relaxation_with_actset && !L2pL0) {
		cerr << "ERROR: active set (both warm and cold started) is only available for l2pl0 problem." << endl;
		exit(3);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	ContextData ctx;

	// - ExplorationStrategy in {stack, heap_on_lb, heap_on_l1, stack_then_heap_on_lb_iteration_threshold, stack_then_heap_on_l1_iteration_threshold}
	NodeContainerInterface* node_set = nullptr;
	strategy_type = argv[3];
	strategy_argument = argv[4];
	if (strategy_type == "stack") {
		node_set = new LIFONodeContainer(ctx);
	} else if (strategy_type == "heap_on_lb") {
		node_set = new MinimierNodeContainer<MinimierLBCompare>(ctx);
	} else if (strategy_type == "heap_on_l1") {
		node_set = new MinimierNodeContainer<MinimierL1Compare>(ctx);
	} else if (strategy_type == "heap_on_ls") {
		node_set = new MinimierNodeContainer<MinimierLSCompare>(ctx);
	} else if (strategy_type == "stack_then_heap_on_lb_iteration_threshold") {
		unsigned int thre = atoi(strategy_argument);
		// Some safety wouldn't be unnecessary here
		node_set = new LIFOThenMinimierThresholdNodeContainer<MinimierLBCompare>(ctx, thre);
	} else if (strategy_type == "stack_then_heap_on_l1_iteration_threshold") {
		unsigned int thre = atoi(strategy_argument);
		// Some safety wouldn't be unnecessary here
		node_set = new LIFOThenMinimierThresholdNodeContainer<MinimierL1Compare>(ctx, thre);
	} else if (strategy_type == "stack_then_heap_on_ls_iteration_threshold") {
		unsigned int thre = atoi(strategy_argument);
		// Some safety wouldn't be unnecessary here
		node_set = new LIFOThenMinimierThresholdNodeContainer<MinimierLSCompare>(ctx, thre);
	} else {
		cerr << "ERROR: invalid strategy choice !" << endl;
		show_usage(string(argv[0]));
		exit(2);
	}


	if (L2L0) {
		if (opt_with_cplex)
			cout<< "L2L0_MIP_Cplex"<<version;
		else if(opt_relaxation_with_cplex)
			cout<< "L2L0_BB_Rcplex";
		else
			cout<< "L2L0_BB_Rhom";

	}
	if (L2pL0) {
		if (opt_with_cplex)
			cout<< "L2pL0_MIP_Cplex"<<version;
		else if(opt_relaxation_with_cplex)
			cout<< "L2pL0_BB_Rcplex";
		else if(opt_relaxation_with_hom)
			cout<< "L2pL0_BB_Rhom";
		else if(opt_relaxation_with_actset)
			cout<< "L2pL0_BB_actset";

	}

	if (L0L2) {
		if (opt_with_cplex)
			cout<< "L0L2_MIP_Cplex"<<version;
		else if(opt_relaxation_with_cplex)
			cout<< "L0L2_BB_Rcplex";
		else
			cout<< "L0L2_BB_Rhom";
	}

	if (L2L0_ASC_ANC) {
			// By default, the ASC formulation is written as an inequality.
			// Force the inequality formulation using the "ineq" value
			if (argc > 6) {
					string ASC_form(argv[argc-1]);
					if (ASC_form == "ineq") {
							ASC_eq = false;
					}
			}
			//////////////////////////////////////////////////////////////////////////
			if (opt_with_cplex) {
					cout<< "L2L0_ASC_ANC_MIP_Cplex"<<version;
			} else if(opt_relaxation_with_cplex) {
					cout<< "L2L0_ASC_ANC_BB_Rcplex";
			} else if(opt_relaxation_with_hom) {
					cout<< "L2L0_ASC_ANC_BB_Rhom";
			} else if (opt_relaxation_with_fcls) {
					cout<< "L2L0_ASC_ANC_BB_Rfcls";
			} else if (opt_relaxation_with_qpoases) {
					cout<< "L2L0_ASC_ANC_BB_Rqpoases";
			} else if (opt_relaxation_with_hom_qpoases) {
					cout <<"L2L0_ASC_ANC_BB_Rhom_qpoases";
			} else if (opt_relaxation_with_hom_fcls) {
					cout <<"L2L0_ASC_ANC_BB_Rhom_fcls";
			}

	}
	cout << std::endl;

	// Juste une petite fantaisie dans le cas de multiple instances
	int cptResetScreen(0);

	vec epsilon;
	vec lambda;

	//snr_range = parse_range(string(argv[7]));
	//k_range = parse_range(string(argv[8]));
	//instance_range = parse_range(string(argv[9]));
  int k = atoi(argv[5]);


	chrono = alloc_chrono();

	string stdin_line;

	//for (unsigned int SNR = snr_range.start; SNR <= snr_range.end; SNR += snr_range.step) {
	//	for (unsigned int k=k_range.start; k <= k_range.end; k+=k_range.step) {
	//		for (unsigned int instance = instance_range.start; instance <= instance_range.end; instance+=instance_range.step) {
	while (getline(cin, stdin_line)) {
				node_set->reset();
				// Reset all file paths
				pathdata_y.str("");
				pathdata_eps.str("");
				pathdata_lambda.str("");
				pathdata_x0_sbr_2p0.str("");
				pathdata_asc_Hmatrix.str("");
				inst_asnc_name.str("");
				// Set the root path
				if (stdin_line[stdin_line.size()-1] != '/') {
					stdin_line.append("/");
				}
				pathdata.str(stdin_line);
				// Technically the line below is wrong,
				// but for now we don't care: this is only used as a model name for Cplex.
				instances_path = stdin_line;
				//pathdata << instances_path << "SA_SNR"<< SNR << "_K" << k << "_instance" << instance << "/";

				// Set the individual file paths
				pathdata_y << pathdata.str() << "y.dat";
				pathdata_eps << pathdata.str() << "alpha_bruit.dat";
				pathdata_lambda << pathdata.str() << "lambda.dat";
				pathdata_x0_sbr_2p0 << pathdata.str() << "x0_2p0_sbr.dat";

				// Defined in global_var.h, A is the dictionary matrix
				Hmatrix_file_path = pathdata.str() + "Hmatrix.dat";
				A.load(Hmatrix_file_path,raw_ascii);
				// Defined in global_var.h, eyeN is a sparse identity matrix
				//eyeN.eye(A.n_rows, A.n_rows); // @suppress("Field cannot be resolved")
				//
				//AA = A.t() * A;
				// *********************************************************************
				// (ASC_ANC) Added extra paths in the initial code to ensure compatibility with the general framework
				pathdata_res.str("");
				pathdata_res << pathdata.str();
				//inst_asnc_name <<  "SA_SNR"<< SNR << "_K" << k << "_instance" << instance;
				inst_asnc_name.str(get_instance_name(pathdata.str()));
				if (L2L0_ASC_ANC) {
						pathdata_xtruth.str("");
						pathdata_xtruth << pathdata.str() << "x_truth.dat";
						ifstream exist_xtruth(pathdata_xtruth.str());
						// It's a simulation => we know the real value of x_truth
						if(exist_xtruth) {	x_truth.load(pathdata_xtruth.str(),raw_ascii);
						} else { x_truth = zeros(A.n_cols, 1);}
				}
				// *********************************************************************
				if (!L2L0_ASC_ANC) {
						BigM = 1.1 * max(abs(A.t() * y)) / pow(norm(A.col(1)), 2);
				}

				y.load(pathdata_y.str(), raw_ascii);
				//cout << "norm(A.col(1))" << pow(norm(A.col(1)), 2)<<endl;
				vec x_sol, x0;
				x0 =zeros<vec> (A.n_cols); // @suppress("Field cannot be resolved")
				pathdata_out.str("");
				//
				////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////
				try{
					if (L2L0) {
						//t_ = clock();
						chrono->start();
						if (opt_with_cplex){
#ifdef USE_CPLEX
							ParamType pt;
							pt.ui = k;
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L2L0);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							CplexSolver opt(&ctx);
							x_sol = opt.optimize(prob_dat);
							pathdata_out << pathdata.str() << "L2L0_MIP_Cplex" << version << "_res.txt";
#else
							cerr << "Using full_cplex on P2/0 problem, but there is no Cplex support in this binary !" << endl;
#endif

						}else{
							ParamType pt;
							pt.ui = k;
							RelaxationType rt;
							if (opt_relaxation_with_cplex)
								rt = RelaxationType::Cplex;
							else
								rt = RelaxationType::Homotopy;
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L2L0);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							MaxXiBranchingRule branch(ctx);
							OptimizerDummyContract dummy;
							Optimizer opt(&ctx, &branch, &dummy);
							x_sol = opt.optimize(prob_dat, rt, *node_set);

#ifdef USE_CPLEX
							if(opt_relaxation_with_cplex)
								pathdata_out << pathdata.str() << "L2L0_BB_Rcplex" << version << "_res.txt";
							else
#endif
								pathdata_out << pathdata.str() << "L2L0_BB_Rhom" << version << "_res.txt";

						}
						//t2_ = clock();
						execution_time = chrono->check();

					} else if (L0L2) {
						epsilon.load(pathdata_eps.str(), raw_ascii);
						if (isnan(epsilon(0))){
							continue;
						}
						//t_ = clock();
						chrono->start();
						if (opt_with_cplex){
#ifdef USE_CPLEX
							ParamType pt;
							pt.db = epsilon(0);
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L0L2);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							CplexSolver opt(&ctx);
							x_sol = opt.optimize(prob_dat);
							pathdata_out << pathdata.str() << "L0L2_MIP_Cplex"<<version<<"_res.txt";
#else
							cerr << "Using full_cplex on P0/2 problem, but there is no Cplex support in this binary !" << endl;
#endif

						}else{
							ParamType pt;
							pt.db = epsilon(0);
							RelaxationType rt;
							if (opt_relaxation_with_cplex)
								rt = RelaxationType::Cplex;
							else
								rt = RelaxationType::Homotopy;
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L0L2);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							MaxXiBranchingRule branch(ctx);
							OptimizerDummyContract dummy;
							Optimizer opt(&ctx, &branch, &dummy);
							x_sol = opt.optimize(prob_dat, rt, *node_set);

#ifdef USE_CPLEX
							if(opt_relaxation_with_cplex)
								pathdata_out << pathdata.str() << "L0L2_BB_Rcplex"<<version<<"_res.txt";
							else
#endif
								pathdata_out << pathdata.str() << "L0L2_BB_Rhom"<<version<<"_res.txt";
						}
						//t2_ = clock();
						execution_time = chrono->check();
					} else if (L2pL0) {
						lambda.load(pathdata_lambda.str(), raw_ascii);
						cout << "lambda = " << lambda(0) << endl;
						//t_ = clock();
						chrono->start();
						if (opt_with_cplex){
#ifdef USE_CPLEX
							ParamType pt;
							pt.db = lambda(0);
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L2pL0);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							CplexSolver opt(&ctx);
							x_sol = opt.optimize(prob_dat);
							pathdata_out << pathdata.str() << "L2pL0_MIP_Cplex"<<version<<"_res.txt";
#else
							cerr << "Using full_cplex on P2+0 problem, but there is no Cplex support in this binary !" << endl;
#endif
						}else{
							if(init_SBR)
								x0.load(pathdata_x0_sbr_2p0.str(), raw_ascii);
							born_sup = pow(norm(y- A*x0, 2), 2) + lambda(0) * norm_zero(x0, exp10(-4)) - norm2carre_y;
							cout << "born_sup : "<< born_sup << endl;
							ParamType pt;
							pt.db = lambda(0);
							RelaxationType rt;
#ifdef USE_CPLEX
							if (opt_relaxation_with_cplex)
								rt = RelaxationType::Cplex;
							else
#endif
							if (opt_relaxation_with_hom)
								rt = RelaxationType::Homotopy;
							else
								rt = RelaxationType::ActiveSet;
							ProblemData prob_dat(A, y, BigM, pt, ProblemType::L2pL0);
							//ctx = new ContextData(&prob_dat);
							ctx.reset(&prob_dat);
							ctx.warm_restart = warm_restart;
							MaxXiBranchingRule branch(ctx);
							//OptimizerDummyContract dummy;
							L2pL0L1Screening contract(ctx);
							Optimizer opt(&ctx, &branch, &contract);
							x_sol = opt.optimize(prob_dat, rt, *node_set);

#ifdef USE_CPLEX
							if(opt_relaxation_with_cplex)
								pathdata_out << pathdata.str() << "L2pL0_BB_Rcplex_res.txt";
							else
#endif
							if(opt_relaxation_with_hom)
								pathdata_out << pathdata.str() << "L2pL0_BB_Rhom_res.txt";
							else if(opt_relaxation_with_actset)
								pathdata_out << pathdata.str() << "L2pL0_BB_Ractset_res.txt";

						}
						//t2_ = clock();
						execution_time = chrono->check();
						//////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////////////////////////////////////

					} else if (L2L0_ASC_ANC) {
							ParamType pt;		// Voir interface.hpp
							pt.ui = k;			// Définition du coefficient de parcimonie
							// By default => eq formulation of the asc;
							string tok_asc_form(ASC_eq?"eq_":"in_");
							cout << "Instance : " << inst_asnc_name.str() << endl << "K = " << k << "\t Q = " << A.n_cols << "\t N = " << A.n_rows <<"\t ASC form : " <<(ASC_eq?"equality":"inequality")<<  endl;
							// Multi resolution : definition of an indicator attribute
							// By convention, the instances_path is finished by '/'; we can remove this character in order to define a set indicator attribute
							string set_id(instances_path);	set_id.pop_back();
							ProblemData prob_dat(A, y, pt, ProblemType::L2L0_ASC_ANC);
							switch(ASC_eq){
									case true : prob_dat.set_ASC_formulation(ASCFormulation::EQ); break;
									case false : prob_dat.set_ASC_formulation(ASCFormulation::INEQ); break;
							}
							//
							chrono->start();
							if (opt_with_cplex){
#ifdef USE_CPLEX
									//ctx.reset(&prob_dat);
									ctx.reset(&prob_dat,x_truth,inst_asnc_name.str(),set_id,verbose);
									CplexSolver opt(&ctx);
									x_sol = opt.optimize(prob_dat);
									pathdata_out << pathdata.str() << "L2L0_ASC_ANC_MIP_Cplex" << version << "_res.txt";
									pathdata_res << "MIPcplex_" << tok_asc_form;
#else
									cerr << "Using full_cplex on P2/0 ASC ANC problem, but there is no Cplex support in this binary !" << endl;
#endif
							} else {
									RelaxationType rt;
#ifdef USE_CPLEX
									if (opt_relaxation_with_cplex) {
											rt = RelaxationType::Cplex;
									} else
#endif
									if (opt_relaxation_with_hom) {
											rt = RelaxationType::Homotopy;
									}	else if (opt_relaxation_with_fcls) {
											rt = RelaxationType::FCLS;
									} else if (opt_relaxation_with_qpoases) {
											rt = RelaxationType::OSQP;
									} else if (opt_relaxation_with_hom_qpoases) {
											rt = RelaxationType::HYB_HOM_OSQP;
									} else if (opt_relaxation_with_hom_fcls) {
											rt = RelaxationType::HYB_HOM_FCLS;
									}

									//ProblemData prob_dat(A, y, pt, ProblemType::L2L0_ASC_ANC);
									// -----------------------------------------------------------
									//ctx.reset(&prob_dat);
									ctx.reset(&prob_dat,x_truth,inst_asnc_name.str(),set_id,verbose);
									// -----------------------------------------------------------
									//MaxXiBranchingRule branch(ctx);
									MaxNZXiBranchingRule branch(ctx);
									// -----------------------------------------------------------
									OptimizerDummyContract dummy;
									Optimizer opt(&ctx, &branch, &dummy);
									x_sol = opt.optimize_ASC_ANC(prob_dat, rt, *node_set);

									// ***********************************************************************//
									//pathdata_res << pathdata_out.str() << pathdata.str();
									// ***********************************************************************//

#ifdef USE_CPLEX
									if(opt_relaxation_with_cplex) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rcplex_res.txt";
											pathdata_res << "BBRcplex_K" << k << "_" << tok_asc_form;
									} else
#endif
									if(opt_relaxation_with_hom) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rhom_res.txt";
											pathdata_res << "BBRhom_K" << k << "_" << tok_asc_form;
									} else if(opt_relaxation_with_fcls) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rfcls_res.txt";
											pathdata_res << "BBRfcls_K" << k << "_" << tok_asc_form;
									} else if (opt_relaxation_with_qpoases) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rosqp_res.txt";
											pathdata_res << "BBRqpoases_K" << k << "_" << tok_asc_form;
									} else if (opt_relaxation_with_hom_fcls) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rhom_fcls_res.txt";
											pathdata_res << "BBRhom_fcls_K" << k << "_" << tok_asc_form;
									} else if (opt_relaxation_with_hom_qpoases) {
											pathdata_out << pathdata.str() << "L2L0_ASC_ANC_BB_Rhom_qpoases_res.txt";
											pathdata_res << "BBRhom_qpoases_K" << k << "_" << tok_asc_form;
									}
							}
							execution_time = chrono->check();
					}
				}catch(exception& e ){
					std::cerr << e.what() << endl;
					continue;
				}

				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				if (L2L0_ASC_ANC) {
						cout << "-----------------------------------------------------------------------"<< endl;
						// Terminal display
						cout << ctx.inst_id << " - " << solver_type << endl;
						cout << "\tT = " << execution_time << " (s)" <<endl;
						if (!opt_with_cplex) { cout << "\t#(node) = " << ctx.BBNodeNum; if (ctx.BestNodeNum >= 0) {cout << "\t/ best(node) = " <<  ctx.BestNodeNum  << endl;} else {cout << endl;}}
						cout << "\tUB*  = " << pow(norm(y-A*x_sol,2),2) << endl;
						cout << "\txUB* : " << endl;	for (size_t i(0); i < x_sol.n_rows; ++i){ if (x_sol[i]>0) {char tok = x_truth[i]>0 ?'\'':'\0'; cout << "\t\t x("<<i<<")"<< tok <<" = \t" << x_sol[i]<<"\n";} }  //cout << endl;
						cout << "\tsum(xUB*) = " << sum(x_sol) << endl;
						////////////////////////////////////////////////////////////////////
						//cout << pathdata.str() << endl;
						//cout << "SUM(x) = " << sum(x_sol) << endl;
						// if (abs(sum(x_sol) - 1)>1e-6 && ASC_eq) {
						// 		cout << "Probleme avec méthode " << pathdata_res.str() << endl;
						// 		//pause_cin_txt("test_sum(x)");
						// }
						////////////////////////////////////////////////////////////////////
						// General listing
						stringstream listing_solver; listing_solver << instances_path << "../../listing_solver.txt";
						ofstream fout;
				    ifstream fin;
				    fin.open(listing_solver.str());
				    fout.open(listing_solver.str(),ios::app);
						if (fin.is_open()) {
								fout << inst_asnc_name.str() << "\t" << k << "\t" << (ASC_eq?"EQ":"IN") << "\t" << solver_type;
								if (!ctx.force_stop) {
										fout << "\t" << execution_time << "\t"
#ifdef USE_CPLEX
										     << (opt_with_cplex?ctx.CplexNodeNum:ctx.BBNodeNum) << "\t"
#endif
										     << pow(norm(y-A*x_sol,2),2) << endl;
								} else {
										fout << "\t" << datum::inf << "\t" << datum::inf << "\t" << datum::inf << endl;
								}
								fin.close();
								fout.close();
						}
						////////////////////////////////////////////////////////////////////
						// In folder results file
						if (!ctx.force_stop){
								// 	File 1 ~ abundances vector
								stringstream infolderres_xopt; infolderres_xopt << pathdata_res.str() << "xopt.txt";
								//cout << infolderres_xopt.str() << endl;
								x_sol.save(infolderres_xopt.str(),raw_ascii);
								// File 2 ~ other informations (for an automatic parser)
								stringstream infolderres_output; infolderres_output << pathdata_res.str() << "output.txt";
								//cout << infolderres_output.str() << endl;
								vec res_output(8);
								res_output.at(0) = pow(norm(y-A*x_sol,2),2);
#ifdef USE_CPLEX
								res_output.at(1) = opt_with_cplex?ctx.CplexNodeNum:ctx.BBNodeNum;
#endif
								res_output.at(2) = execution_time;
								res_output.at(3) = (opt_with_cplex || opt_relaxation_with_cplex || opt_relaxation_with_qpoases || opt_relaxation_with_hom_qpoases)?ctx.T_model:datum::inf;
								res_output.at(4) = (opt_relaxation_with_fcls || opt_relaxation_with_hom_fcls)?ctx.T_FCLS:datum::inf;
								res_output.at(5) = (opt_relaxation_with_hom_qpoases || opt_relaxation_with_hom_fcls)?ctx.T_homo:datum::inf;
								res_output.at(6) = opt_with_cplex?datum::inf:(ctx.BestNodeNum<0?INT_MAX:ctx.BestNodeNum);
#ifdef USE_CPLEX
								res_output.at(7) = opt_with_cplex?ctx.CplexStatus:datum::inf;
#endif
								//res_output.t().print();
								res_output.save(infolderres_output.str(),raw_ascii);
						}
						////////////////////////////////////////////////////////////////////
				} // if (L2L0_ASC_ANC) {
				if (ctx.force_stop) {
						cout << ctx.inst_id << endl;
						//pause_cin_txt("FORCE STOP => Next ?");
				}
				// Juste une petite fantaisie dans le cas de multiple instances
				//if (cptResetScreen > 0) {
				//		system("clear");
				//}
				cptResetScreen++;
				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////
				//         RESULT
				if (!L2L0_ASC_ANC) {
					cout << endl << "BigM:" << BigM << endl;
#ifdef USE_CPLEX
					if (opt_with_cplex) {
						cout << "Node Number Cplex: " << ctx.CplexNodeNum << endl;
						cout << "val opt: " << pow(norm(y-A*x_sol,2),2) << endl;
						cout << "temps d'execution: " << execution_time << endl;
					} else {
#endif
						//cerr << SNR << ", ";
						cerr << k << ", ";
						// cerr << instance << ", ";
						cerr << ctx.BBNodeNum << ", ";
						cerr << ctx.UB << ", ";
						cerr << ctx.nbr_iter / ctx.BBNodeNum <<", ";
						cerr << execution_time - ctx.T_model << ", ";
						cerr << ctx.T_model << ", ";

						cerr << ctx.T_homo << ", ";
						cerr << ctx.T_FCLS << ", ";
						cerr << ctx.T_test << ", " ;
						cerr << ctx.BestNodeNum << endl;

						cout << "Node_Number_BB: " << ctx.BBNodeNum << endl;
						cout << "val_opt: " << ctx.UB << endl;

						cout << "nbr_iter mot :" << ctx.nbr_iter / ctx.BBNodeNum <<endl;
						cout << "temps_d'execution: " << execution_time - ctx.T_model << endl;
						cout << "temps de modelisation : " << ctx.T_model << endl;

						cout << "temps relaxation: " << ctx.T_homo << endl;
						cout << "temps test: " << ctx.T_test << endl ;
						cout << "Best_Node_num: "<< ctx.BestNodeNum << endl;


						// if (ctx.BestNodeNum % 2 == 0 && ctx.BestNodeNum != 0) {
						// 		//pause_cin_txt("ARRET BEST NODE NUM PAIR");
						// 		sleep(1);
						// }

#ifdef USE_CPLEX
					}
#endif



				}




	} // while (getline(cin, stdin_line))
			// }	// for (unsigned int instance = instance_range.start; instance <= instance_range.end; instance+=instance_range.step)
		// }	// for (unsigned int k=k_range.start; k <= k_range.end; k+=k_range.step)
	// }	// for (unsigned int SNR = snr_range.start; SNR <= snr_range.end; SNR += snr_range.step)
	delete chrono;
	if (node_set != nullptr)
		delete node_set;
}


void show_usage(const string& program_name)
{
	cerr << "USAGE: " << program_name << " ProblemType SolverType ExplorationStrategyType ExplorationStrategyTypeArgument K AscFormulation" << endl;
	cerr << "with:" << endl;
	// cerr << "- For ProblemType in {l2l0, l2pl0_with_SBR, l2pl0_without_SBR, l0l2}" << endl
	//      << "\t * SolverType in {full_cplex, bb_cplex, bb_homotopy, bb_activeset_warm, bb_activeset_cold}" << endl
	//      << "\t * ExplorationStrategy in {stack, heap_on_lb, heap_on_l1, stack_then_heap_on_lb_iteration_threshold, stack_then_heap_on_l1_iteration_threshold}" << endl
	//      << "\t * ExplorationStrategyType being unused for stack and heap (lb, l1 and ls), being the iteration triggering the strategy change for stack_then_heap_on_*_iteration_threshold" << endl
	//      << "\t * K a positive integer" << endl
	cerr << "- For ProblemType = l2l0_asc_anc : " << endl
			 << "\t * SolverType in {"
#ifdef USE_CPLEX
			 << "full_cplex, bb_cplex, "
#endif
			 << "bb_fcls, bb_qpoases, bb_homotopy_qpoases, bb_homotopy_fcls}" << endl
			 << "\t * ExplorationStrategy : stack" << endl
			 << "\t * ExplorationStrategyType : 0" << endl
			 << "\t * K a positive integer" << endl
			 << "\t * [optionnal] AscFormulation : eq, ineq (default value \"eq\")" << endl;
}

parsed_range parse_range(const string& user_range)
{
	parsed_range ret;
	size_t first_separator, second_separator;
	const string separator_glyph = ":";
	first_separator = user_range.find(separator_glyph, 0);
	if (first_separator == string::npos) {
		// the whole string should be a number
		ret.start = ret.end = atoi(user_range.c_str());
		ret.step = 1;
	} else {
		second_separator = user_range.find(separator_glyph, first_separator+1);
		if (second_separator == string::npos) {
			// a:b format
			string a_string = user_range.substr(0, first_separator);
			string b_string = user_range.substr(first_separator+1, string::npos);
			ret.start = atoi(a_string.c_str());
			ret.end = atoi(b_string.c_str());
			ret.step = 1;
		} else if (string::npos == user_range.find(separator_glyph, second_separator+1)) {
			// a:step:b
			string a_string = user_range.substr(0, first_separator);
			string step_string = user_range.substr(first_separator+1, second_separator-first_separator-1);
			string b_string = user_range.substr(second_separator+1, string::npos);
			ret.start = atoi(a_string.c_str());
			ret.step = atoi(step_string.c_str());
			ret.end = atoi(b_string.c_str());
		} else {
			cerr << "ERROR: there's too many separators for given range " << user_range << " !" << endl;
			exit(2);
		}
	}
	return ret;
}

string get_instance_name(const string& path)
{
	auto trailing_slash = path.rfind("/", path.size()-1);
	auto just_before_instance_rep_slash = path.rfind("/", trailing_slash-1);

	string inst_rep_name = path.substr(just_before_instance_rep_slash+1, trailing_slash - just_before_instance_rep_slash - 1);
	return inst_rep_name;
}
