#ifndef BBCONTRACT
#define BBCONTRACT 1

#include "interfaces.hpp"
#include "Context.hpp"

// For now we are not using any contractor, so this is a place-holder to
// validate the software architecture used.

/** \file Contract.hpp
 * \brief Contract heuristics.
 *
 * These operators takes a given node, applies some heuristics to it
 * (screening methods for example) to reduce the search space, and returns
 * the contracted node.
 *
 */

/**
 * This is a no-op contractor for the code using Optimizer.
 */
class OptimizerDummyContract : public ContractInterface {
public:
	virtual NodeInterface& operator()(NodeInterface& node) { return node; }
	OptimizerDummyContract() {}
};

class L2pL0L1Screening : public ContractInterface {
	ContextData& ctx;
public:
	virtual NodeInterface& operator()(NodeInterface& node);
	L2pL0L1Screening(ContextData& context): ctx(context) {}
};

#endif
