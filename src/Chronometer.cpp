#include "Chronometer.hpp"

void LinuxChronometer::start()
{
	this->start_base = clock();
}

double LinuxChronometer::check()
{
	return ((double)(clock() - this->start_base)) / CLOCKS_PER_SEC;;
}
