#include <ilcplex/ilocplex.h>

ILOSTLBEGIN

#include "CplexSolver.hpp"
#include "Util.h"

void CplexSolver::generate_model_l0l2_with_bigM(mat H, vec y, double epsilon, double bigM){
	mat  HH;
	vec  Hy;
	vec yy;

	HH = H.t() * H;
		Hy = H.t() * y;

	yy= y.t()*y;

	//	bigM = 1*max(abs(Hy)) / pow(norm(H.col(1)), 2);
    int Q = H.n_cols;
		//-----------Affichage------------
		cout << "bigM = " << bigM << endl;
		cout << "epsilon = " << epsilon << endl;

		IloEnv env;
			try {

					IloModel model(env);
					IloCplex cplex(model);


		//-----------variables------------

					IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
					IloBoolVarArray b(env, Q); //** declaration de Q variables Booléennes

					//** Facultatif:: nommer les variables **//
					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "b" << i + 1;
						b[i] = IloBoolVar(env, name.str().c_str());
					}

					model.add(x);
					model.add(b);

					//-----------Objective------------

					IloObjective obj(env, IloSum(b) , IloObjective::Minimize, "OBJ");
					model.add(obj);

					std::cout<<"obj ajouté"<<endl;



					//-----------Contraintes------------

					IloExpr cnstExpr(env);

					// X'*AA*X
					for (int i = 0; i < Q; i++)
						for (int j = 0; j < Q; j++)
							cnstExpr += HH(i, j) * x[i] * x[j];

				//	cnstExpr += HH(0, 0) * x[0] * x[0];

					// -2 Y' * A * X
					for (int i = 0; i < Q; i++)
						cnstExpr += -2 * Hy(i) * x[i];


				//	IloRange ctr_Qp(env, 0, cnstExpr, epsilon , "ctr_Qp");


					IloRangeArray ctr(env);
					ctr.add(cnstExpr +yy(0) <= pow(epsilon,2) );
					//ctr.add(HH(0, 0) * x[0] * x[0] <= 0);



					std::cout<<"Contraintes créés"<<endl;
					model.add(ctr);

					std::cout<<"Contraintes ajoutés"<<endl;



					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "ctr_bigM_" << i + 1 << "_1";
						model.add(
								IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
										name.str().c_str()));

						name << "ctr_bigM_" << i + 1 << "_2";
						model.add(
								IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
										name.str().c_str()));
						// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
					}



				//	cplex.setOut(env.getNullStream());

					//------------ save Model------------
					cplex.exportModel("model_L0L2_BigM.lp");
					std::cout<<"modèle avec BigM créé"<<endl;

					cnstExpr.end();
					env.end();

				} catch (IloException & e) {
					cerr << " ERREUR : exception = " << e << endl;
					exit(0);
				}

}



vec CplexSolver::Cplexsolver_l0l2(mat *A,vec *y,double epsilon ,double BigM){

    int Q = A->n_cols;
    vec x = zeros(Q);
    ContextData *ctx = this->context;


IloEnv env;
	try {
			IloModel model(env);
			IloCplex cplex(model);


			clock_t t0, t1;
				t0=clock();

		generate_model_l0l2_with_bigM(*A, *y, epsilon, BigM);

		t1=clock();
				ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;

		IloObjective obj;
		IloNumVarArray var(env);
		IloRangeArray rng(env);
		cplex.importModel(model, "model_L0L2_BigM.lp", obj, var, rng);
		//------------Resolution------------



		cplex.setParam(IloCplex::Param::TimeLimit,ctx->TimeBBMax);
		cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap,  ctx->gap);
		if(ctx->cplex_branching_rule==4){
		cplex.setParam(IloCplex::VarSel, CPX_VARSEL_STRONG);
		cplex.setParam(IloCplex::BrDir, CPX_BRDIR_UP);
		}
		cplex.solve();




		std::cout << std::endl << "------------Resultat--------------"<< std::endl;
		ctx->CplexNodeNum =cplex.getNnodes() ;
		ctx->CplexStatus =cplex.getStatus() ;



		ctx->UB = cplex.getObjValue();
		//std::cout << "Time = " << cplex.getTime() - start << " sec"<< std::endl;

		IloNumArray vals(env);
		cplex.getValues(vals, var);


		for (int i = 0; i < Q; i++){
				x(i)=vals[i+Q];

		}
		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	return x;
}

void CplexSolver::generate_model_with_bigM_l2l0(mat H, vec y, int k, double bigM){
	mat  HH;
	vec  Hy;

	HH = H.t() * H;
		Hy = H.t() * y;
	//	bigM = 1*max(abs(Hy)) / pow(norm(H.col(1)), 2);
    int Q = H.n_cols;
		//-----------Affichage------------
		cout << "bigM = " << bigM << endl;
		cout << "K = " << k << endl;

		IloEnv env;
			try {

					IloModel model(env);
					IloCplex cplex(model);


		//-----------variables------------

					IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
					IloBoolVarArray b(env, Q); //** declaration de Q variables Booléennes

					//** Facultatif:: nommer les variables **//
					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "b" << i + 1;
						b[i] = IloBoolVar(env, name.str().c_str());
					}

					model.add(x);
					model.add(b);

					//-----------Objective------------

					IloExpr objExpr(env);

					// -2 Y' * H * X
					for (int i = 0; i < Q; i++)
						objExpr += -2 * Hy(i) * x[i];

					// X'*HH*X
					for (int i = 0; i < Q; i++)
						for (int j = 0; j < Q; j++)
							objExpr += x[i] * HH(i, j) * x[j];

					IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
					model.add(obj);

					//-----------Contraintes------------

					IloRange ctr_sum(env, 0, IloSum(b), k, "ctr_sum");
					model.add(ctr_sum);

					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "ctr_bigM_" << i + 1 << "_1";
						model.add(
								IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
										name.str().c_str()));

						name << "ctr_bigM_" << i + 1 << "_2";
						model.add(
								IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
										name.str().c_str()));
						// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
					}

					//------------ save Model------------
					cplex.exportModel("model_L2L0_BigM.lp");
					std::cout<<"modèle avec BigM créé"<<endl;

					env.end();

				} catch (IloException & e) {
					cerr << " ERREUR : exception = " << e << endl;
				}

}

void CplexSolver::generate_model_with_bigM_l2l0_NC(mat H, vec y, int k, double bigM){
	mat  HH;
	vec  Hy;

	HH = H.t() * H;
		Hy = H.t() * y;
	//	bigM = 1*max(abs(Hy)) / pow(norm(H.col(1)), 2);
    int Q = H.n_cols;
    int N = H.n_rows;
		//-----------Affichage------------
		cout << "bigM = " << bigM << endl;
		cout << "K = " << k << endl;

		IloEnv env;
			try {

					IloModel model(env);
					IloCplex cplex(model);


		//-----------variables------------

					IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
					IloBoolVarArray b(env, Q); //** declaration de Q variables Booléennes
					IloNumVarArray z(env, N, -IloInfinity, IloInfinity );

					cout << "N"<< N <<endl;

					model.add(x);
					model.add(b);
					model.add(z);


					cout << "2"<<endl;
					//-----------Objective------------

					IloExpr objExpr(env);

					// -2 Y' * Z
					for (int i = 0; i < N; i++)
						objExpr += -2  * z[i] * y(i);

					cout << "3"<<endl;

					// Z'*Z
					for (int i = 0; i < N; i++)
							objExpr += z[i] * z[i];

					cout << "4"<<endl;

					IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
					model.add(obj);

					cout << "5"<<endl;

					//-----------Contraintes------------

					for (int i = 0; i < N; i++){
						IloExpr exp_Hx_i(env);
						for (int j = 0; j < Q; j++) {
							exp_Hx_i += H(i,j) * x[j];
						//cout << i << " " << j;
									}
						//cout <<endl;
					model.add(IloRange(env, 0, exp_Hx_i - z[i], 0 ));
					}


					IloRange ctr_sum(env, 0, IloSum(b), k, "ctr_sum");
					model.add(ctr_sum);

					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "ctr_bigM_" << i + 1 << "_1";
						model.add(
								IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
										name.str().c_str()));

						name << "ctr_bigM_" << i + 1 << "_2";
						model.add(
								IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
										name.str().c_str()));
						// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
					}

					//------------ save Model------------
					cplex.exportModel("model_L2L0_BigM.lp");
					std::cout<<"modèle avec BigM créé"<<endl;

					env.end();

				} catch (IloException & e) {
					cerr << " ERREUR : exception = " << e << endl;
				}

}




vec CplexSolver::Cplexsolver_l2l0(mat *A,vec *y,int k ,double BigM){

    int Q = A->n_cols;
    int N = A->n_rows;
    vec x_sol = zeros(Q);
    ContextData *ctx = this->context;

IloEnv env;
	try {
			IloModel model(env);
			IloCplex cplex(model);

			clock_t t0, t1;
				t0=clock();

		//if ( 0) {
		//	generate_modell2plusl0with_bigM(H, y, lambda, bigM);
		//	generate_modell2plusl0with_bigM_bcarre(H, y, lambda, bigM);
			if(Q<N){
				cout << "Q<N" << endl;
				generate_model_with_bigM_l2l0(*A, *y, k, BigM);
			}else{
				cout << "Q>=N" << endl;
				generate_model_with_bigM_l2l0_NC(*A, *y, k, BigM);
			}


		t1=clock();
			ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
		//}else{

		IloObjective obj;
		IloNumVarArray var(env);
		IloRangeArray rng(env);
		cplex.importModel(model, "model_L2L0_BigM.lp", obj, var, rng);
		//------------Resolution------------

		cplex.setParam(IloCplex::Param::TimeLimit,ctx->TimeBBMax);
		//cplex.setParam(IloCplex::Param::Threads,1);
		cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap,  ctx->gap);
		if(ctx->cplex_branching_rule==4){
		cplex.setParam(IloCplex::VarSel, CPX_VARSEL_STRONG);
		cplex.setParam(IloCplex::BrDir, CPX_BRDIR_UP);
		}

		cplex.solve();

		//------------Resultat--------------
		std::cout << std::endl << "------------Resultat--------------"
				<< std::endl;
		ctx->CplexNodeNum =cplex.getNnodes() ;
		ctx->CplexStatus =cplex.getStatus();
		ctx->UB = cplex.getObjValue();

		IloNumArray vals(env);
		cplex.getValues(vals, var);

	//	cout << "size(var) " << var.getSize() << endl;


	if(Q<N){

		for (int i = 0; i < Q; i++)
				x_sol[i]=vals[i];
	}else{
		for (int i = 0; i < Q; i++)
				x_sol[i]=vals[i+N];
	}



	//	}
		env.end();

		} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	return x_sol;
}

void CplexSolver::generate_model_with_bigM_l2pl0(mat H, vec y,  double lambda, double bigM){
	mat  HH;
	vec  Hy;

	HH = H.t() * H;
		Hy = H.t() * y;
	//	bigM = 1*max(abs(Hy)) / pow(norm(H.col(1)), 2);
    int Q = H.n_cols;
		//-----------Affichage------------
		cout << "bigM = " << bigM << endl;
		cout << "lambda = " << lambda << endl;

		IloEnv env;
			try {

					IloModel model(env);
					IloCplex cplex(model);


		//-----------variables------------

					IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
					IloBoolVarArray b(env, Q); //** declaration de Q variables Booléennes

					//** Facultatif:: nommer les variables **//
					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "b" << i + 1;
						b[i] = IloBoolVar(env, name.str().c_str());
					}

					model.add(x);
					model.add(b);

					//-----------Objective------------

					IloExpr objExpr(env);

					// -2 Y' * H * X
					for (int i = 0; i < Q; i++)
						objExpr += -2 * Hy(i) * x[i];

					// X'*HH*X
					for (int i = 0; i < Q; i++)
						for (int j = 0; j < Q; j++)
							objExpr += x[i] * HH(i, j) * x[j];

					// lambda sum b
					for (int i = 0; i < Q; i++)
					objExpr += lambda * b[i];

					IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
					model.add(obj);

					//-----------Contraintes------------

					for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "ctr_bigM_" << i + 1 << "_1";
						model.add(
								IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
										name.str().c_str()));

						name << "ctr_bigM_" << i + 1 << "_2";
						model.add(
								IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
										name.str().c_str()));
						// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
					}


					IloNumArray startx(env);
					for (int i = 0; i < Q; i++) {
							startx.add(0);
					}

					cplex.addMIPStart(x, startx,IloCplex::MIPStartAuto,"secondMIPStart");


					//------------ save Model------------
					cplex.exportModel("model_L2pL0_BigM.lp");
					std::cout<<"modèle avec BigM créé"<<endl;

					env.end();

				} catch (IloException & e) {
					cerr << " ERREUR : exception = " << e << endl;
				}

}

vec CplexSolver::Cplexsolver_l2pl0(mat *A,vec *y, double lambda,double BigM){

    int Q = A->n_cols;
    vec x = zeros(Q);
    ContextData *ctx = this->context;


IloEnv env;
	try {
			IloModel model(env);
			IloCplex cplex(model);


		clock_t t0, t1;
		t0=clock();


			generate_model_with_bigM_l2pl0(*A, *y, lambda, BigM);

		t1=clock();
		ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;


		IloObjective obj;
		IloNumVarArray var(env);
		IloRangeArray rng(env);
		cplex.importModel(model, "model_L2pL0_BigM.lp", obj, var, rng);
		//------------Resolution------------


		//cplex.setParam(IloCplex::Threads, 4); // nombre de threads
		cplex.setParam(IloCplex::Param::TimeLimit,ctx->TimeBBMax);
		cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap,  ctx->gap);
		if(ctx->cplex_branching_rule==4){
		cplex.setParam(IloCplex::VarSel, CPX_VARSEL_STRONG);
		cplex.setParam(IloCplex::BrDir, CPX_BRDIR_UP);
		}

		cplex.solve();




		std::cout << std::endl << "------------Resultat--------------"<< std::endl;
		ctx->CplexNodeNum =cplex.getNnodes() ;
		ctx->CplexStatus =cplex.getStatus();
		ctx->UB = cplex.getObjValue();
		//std::cout << "Time = " << cplex.getTime() - start << " sec"<< std::endl;

		IloNumArray vals(env);
		cplex.getValues(vals, var);


		for (int i = 0; i < Q; i++){
				x(i)=vals[i];

		}
		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	return x;
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
vec CplexSolver::Cplexsolver_l2l0_ASC_ANC(mat *H,vec *y,int k){
		int Q(H->n_cols);
		int N(H->n_rows);
		vec x_sol(zeros(Q));

		//
		ContextData *ctx = this->context;
		bool ASC_asEq(ctx->problem->get_ASC_formulation());
		std::string set_id(ctx->set_id);
		std::stringstream mdl_name("");	mdl_name<<set_id<<"_mdl.lp";
		//cout << "ASC_asEq = " << ASC_asEq << endl;
		//
		IloEnv env;
		/* --- */
		try {
			IloModel model(env);
			IloCplex cplex(model);
			clock_t t0,t1;


			//pause_cin_txt("cplex_solver pause");
			/* --- */
			t0 = clock();
			if (Q < N) {
					cout << "MIP CPLEX : Cas N > Q - OC" << endl;
					generate_model_l2l0_ASC_ANC_OC(*H,*y,k,ASC_asEq,set_id);
			} else {
					cout << "MIP CPLEX : Cas N <= Q - UC" << endl;
					generate_model_l2l0_ASC_ANC_UC(*H,*y,k,ASC_asEq,set_id);
			}
			t1 = clock();
			ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
			/* --- */
			IloObjective obj;
			IloNumVarArray var(env);
			IloRangeArray rng(env);

			//cplex.importModel(model, "model_l2l0_ASC_ANC.lp", obj, var, rng);
			cplex.importModel(model, mdl_name.str().c_str(), obj, var, rng);
			cplex.setParam(IloCplex::Param::TimeLimit,ctx->TimeBBMax);
			cplex.setParam(IloCplex::Param::MIP::Tolerances::MIPGap,  ctx->gap);
			cplex.setParam(IloCplex::Param::MIP::Interval,1);			// La totale
			cplex.setParam(IloCplex::Param::MIP::Display,3);			// No display (3 if we want to follow the resolution)
			cplex.setParam(IloCplex::Param::Threads,1);
			//
			cplex.setOut(env.getNullStream());									// If you want mute all cplex's info (force all)
			/* --- */
			// ??
			if(ctx->cplex_branching_rule==4){
					cplex.setParam(IloCplex::VarSel, CPX_VARSEL_STRONG);
					cplex.setParam(IloCplex::BrDir, CPX_BRDIR_UP);
			}
			/* --- */
			cplex.solve();
			/* --- */
			ctx->CplexNodeNum =cplex.getNnodes() ;
			ctx->CplexNodeNum =cplex.getNnodes() ;
			ctx->CplexStatus =cplex.getStatus();
			ctx->UB = cplex.getObjValue();
			/* --- */
			IloNumArray vals(env);
			cplex.getValues(vals, var);
			//cout << "size(var) " << vals.getSize() << endl;
			string asc_suff(ASC_asEq?"eq":"in");
			//cout << "N= "<<N<<"\tQ= "<<Q<<endl;
			//cout << "size(x_cplex|"<<asc_suff<<") = " << vals.getSize() << endl;
			/* --- */
			if (Q<N) {
					for (int i(0); i<Q; ++i){x_sol[i]=vals[i];}
			} else {
					// Cplex output vector is composed as follow (in case of reformulation with N equality constraint)
					// 		z = Hx  [0,N-1];
					// 		b 			[(N+1)-1,(N+Q)-1];
					//		k 			(N+Q+1)-1;
					//		x 			[(N+Q+2)-1,(2Q+N+1)-1]
	 				//		ASC rhs term : (N+2Q+2)-1 	(only for the inequality formulation of the ASC )
					// Théorème du grillage et maudite indexation à partir de 0 ...
					vec x_all(zeros(vals.getSize()));
					for (int i(0); i<vals.getSize(); ++i) {	x_all[i] = vals[i];	}
					//
					//stringstream x_file; x_file << "x_all_" << asc_suff << ".txt";
					//x_all.save(x_file.str(),raw_ascii);			// to test the result with check_solver.m
					//cout << "range(X) = ["<<(N+Q+2)-1<<","<<(2*Q+N+1)-1<<"]"<< endl;
					x_sol = x_all.rows((N+Q+2)-1,(2*Q+N+1)-1);
					//pause_cin_txt("ICI");
			}
			env.end();
			/* --- */
		}	catch (IloException & e) {
			cerr << " ERREUR : exception = " << e << endl;
		}
		/* --- */
		return x_sol;
}
//////////////////////////////////////////////////////////////////////////////////////////
void CplexSolver::generate_model_l2l0_ASC_ANC_OC(mat H, vec y, int k,bool ASC_asEq,std::string set_id){
		//cout << "Générateur modèle OC " << endl;
		mat HtH(H.t()*H);	//x
		vec Hty(H.t()*y);	//x
		int Q(H.n_cols);	//x
		/* --- */
		IloEnv env;
		try {
				IloModel model(env);
				IloCplex cplex(model);
				/* --- */
				IloNumVarArray x(env,Q,0,1);	//x
				IloBoolVarArray b(env,Q);			//x
				for (int i; i <Q; ++i ) {	std::stringstream name;	name << "b" << i + 1;	b[i] = IloBoolVar(env, name.str().c_str());	}
				for (int i; i <Q; ++i ) {	std::stringstream name;	name << "x" << i + 1;	x[i] = IloNumVar(env, name.str().c_str());	}
				model.add(x);
				model.add(b);
				/* --- */
				IloExpr objExpr(env);
				for (int i(0);i<Q; ++i) {
						objExpr += -2 * Hty(i) * x[i];
						for (int j(0);j<Q;++j){
								objExpr+= x[i]*HtH(i,j)*x[j];
						}
				}
				IloObjective obj(env, objExpr, IloObjective::Minimize,"OBJ");
				model.add(obj);
				/* --- */
				IloRange ctr_sparse(env,0,IloSum(b),k,"ctr_sparse");
				model.add(ctr_sparse);
				/* --- */
				IloNum lb_ASC(1.);
				if (!ASC_asEq){ lb_ASC = 0.;}
				//cout << "lb_ASC = " << lb_ASC << endl;
				IloRange ctr_ASC(env,lb_ASC,IloSum(x),1,"ctr_ASC");
				//pause_cin_txt("Générateur OC");

				model.add(ctr_ASC);
				/* --- */
				for (int i(0); i < Q; ++i){
						std::stringstream name_anc;
						std::stringstream name_link;
						name_anc << "ctr_ANC_" << i+1;
						model.add(IloRange(env, -IloInfinity,-x[i],0,name_anc.str().c_str()));
						name_link << "ctr_link_" << i+1;
						model.add(IloRange(env, -IloInfinity,x[i]-b[i],0,name_link.str().c_str()));
				}
				/* --- */
				std::stringstream mdl_name("");	mdl_name<<set_id<<"_mdl.lp";
				//cplex.exportModel("model_l2l0_ASC_ANC.lp");
				cplex.exportModel(mdl_name.str().c_str() );
				env.end();
		} catch (IloException & e) {
				cerr << " ERREUR : exception = " << e << endl;
		}
		/* --- */
}
//////////////////////////////////////////////////////////////////////////////////////////
void CplexSolver::generate_model_l2l0_ASC_ANC_UC(mat H, vec y, int k,bool ASC_asEq,std::string set_id){
		//cout << "Générateur modèle UC " << endl;
		int Q(H.n_cols),N(H.n_rows);
		/* --- */
		IloEnv env;
		try {
				IloModel model(env);
				IloCplex cplex(model);
				/* --- */
				IloNumVarArray x(env,Q,0,1);
				IloBoolVarArray b(env,Q);
				IloNumVarArray z(env,N,-IloInfinity,+IloInfinity);
				for (int i; i <Q; ++i ) {	std::stringstream name;	name << "x" << i + 1;	x[i] = IloNumVar(env, name.str().c_str());	}
				for (int i; i <Q; ++i ) {	std::stringstream name;	name << "b" << i + 1;	b[i] = IloBoolVar(env, name.str().c_str());	}
				for (int i; i <N; ++i ) {	std::stringstream name;	name << "z" << i + 1;	z[i] = IloNumVar(env, name.str().c_str());	}
				model.add(x);
				model.add(b);
				model.add(z);
				/* --- */
				IloExpr objExpr(env);
				for (int i(0);i<N;++i){
						objExpr+= z[i]*z[i];
						objExpr+= -2*z[i]*y(i);		// Note : z variable, y valeur (vecteur)
				}
				IloObjective obj(env, objExpr, IloObjective::Minimize,"OBJ");		//x
				model.add(obj);
				/* --- */
				IloRange ctr_sparse(env,0,IloSum(b),k,"ctr_sparse");
				model.add(ctr_sparse);		//x
				/* --- */
				IloNum lb_ASC(1.);
				if (!ASC_asEq) {	lb_ASC = 0.;}
				IloRange ctr_ASC(env,lb_ASC,IloSum(x),1.,"ctr_ASC");
				model.add(ctr_ASC);
				//cout << "lb_ASC = " << lb_ASC << endl;
				/* --- */
				for (int i(0); i < Q; ++i){
						std::stringstream name_anc;
						std::stringstream name_link;
						name_anc << "ctr_ANC_" << i+1;
						model.add(IloRange(env, -IloInfinity,-x[i],0,name_anc.str().c_str()));
						name_link << "ctr_link_" << i+1;
						model.add(IloRange(env, -IloInfinity,x[i]-b[i],0,name_link.str().c_str()));
				}
				/* --- */
				for(int i(0);i<N;++i) {
						std::stringstream name;
						name << "ctr_eq_lin_" << i+1;
						IloExpr Hx_i(env);
						for (int j(0); j<Q; ++j){
								Hx_i+= H(i,j)*x[j];
						}
						model.add(IloRange(env,0,Hx_i-z[i],0,name.str().c_str()));		//x
				}
				/* --- */
				std::stringstream mdl_name("");	mdl_name<<set_id<<"_mdl.lp";
				//cplex.exportModel("model_l2l0_ASC_ANC.lp");
				cplex.exportModel(mdl_name.str().c_str() );

				env.end();
		} catch (IloException & e) {
				cerr << " ERREUR : exception = " << e << endl;
		}
		/* --- */
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
vec CplexSolver::optimize(ProblemData& problem)
{
	vec x_sol;
	switch (problem.type) {
	case ProblemType::L0L2:
		x_sol = this->Cplexsolver_l0l2(&problem.H, &problem.y, problem.param.db, problem.bigM);
		break;
	case ProblemType::L2L0:
		x_sol = this->Cplexsolver_l2l0(&problem.H, &problem.y, problem.param.ui, problem.bigM);
		break;
	case ProblemType::L2pL0:
		x_sol = this->Cplexsolver_l2pl0(&problem.H, &problem.y, problem.param.db, problem.bigM);
		break;
	case ProblemType::L2L0_ASC_ANC :
		x_sol = this->Cplexsolver_l2l0_ASC_ANC(&problem.H, &problem.y, problem.param.ui);
		break;
	}
	return x_sol;
}
