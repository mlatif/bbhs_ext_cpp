#include "node_container.hpp"
// Stack-based Container

LIFONodeContainer::LIFONodeContainer(ContextData& context) : ctx(context)
{
}

NodeInterface* LIFONodeContainer::top()
{
	return this->storage.back();
}

NodeInterface* LIFONodeContainer::pop()
{
	NodeInterface* node = this->storage.back();
	this->storage.pop_back();
	return node;
}

bool LIFONodeContainer::push(NodeInterface* node)
{
	//cout << "Pushing "<< node->getIdx() << "\t |Sv| = "<< node->getSv().size() << "\t";
	//if (node->getLB() < this->ctx.UB) {
	// Une sécurité au cas où ...
	if (node->getLB() < this->ctx.UB && !node->getSv().empty()) {
		this->storage.push_back(node);
		//cout << "Y" << endl;
		return true;
	}
	//cout << "N" << endl;
	return false;
}

double LIFONodeContainer::getLowestLB()
{
	// Naive implementation for now
	double min_lb = this->storage.front()->getLB();
	for (std::vector<NodeInterface*>::iterator it = this->storage.begin(); it != this->storage.end(); it++) {
		double cur_lb = (*it)->getLB();
		if (cur_lb < min_lb)
			min_lb = cur_lb;
	}
	return min_lb;
}

void LIFONodeContainer::prune(double ub)
{
	//cout << "X PRUNE : \t ";
	std::vector<NodeInterface*>::iterator it = this->storage.begin();
	while(it != this->storage.end()) {
		if ((*it)->getLB() > ub) {
			//cout << (*it)->getIdx() << " \t";
			delete *it;
			it = this->storage.erase(it);
		} else {
			//cout << "\tNO : node " << (*it)->getIdx() << "\t lb = " << (*it)->getLB() << "\t ctx.ub = " << ub << endl;
			it++;
		}
	}
	//cout << endl;
}

void LIFONodeContainer::reset()
{
	if (!this->storage.empty()) {
		for (std::vector<NodeInterface*>::iterator it = this->storage.begin(); it != this->storage.end(); it++) {
			delete *it;
		}
	}
	this->storage.clear();
}

void LIFONodeContainer::remove(NodeInterface* node)
{
	this->storage.erase(std::remove(this->storage.begin(), this->storage.end(), node), this->storage.end());
}

LIFONodeContainer::~LIFONodeContainer()
{
	this->reset();
}
