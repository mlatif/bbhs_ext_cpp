#include "LB.hpp"

#include "MimosaConfig.hpp"

#ifdef USE_CPLEX
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN
#else
using namespace std;
#endif

#include "is_binary.h"
#include "Util.h"
#include "Chronometer.hpp"

#include <qpOASES.hpp>
USING_NAMESPACE_QPOASES

/** \file LB.cpp - Compute Lower Bounds
 * This is the place for the various lower bounds using convex relaxations:
 * - Cplex based
 * - Homotopy based
 * - Active set based
 * The focus is on efficiency here: we have a lot of lower bounds computations
 * happening, se we must have something fast.
 *
 * Some remarks about the notations:
 * - xub stands for the x leading to the upper bound of the analyzed node,
 *   that is the least-square solution of the problem restricted to the support S1.
 * - w stands for the x upper bound components, so w is xub restricted to S1.
 */


// **************** Generic homotopy *************

vec GenericHomotopy::mixed_BigM_homotopytemps(mat B, vec *w, mat *H, uvec uQ1, uvec uQv, vec y,
		double M, double pb_param,vec *score) {
	//clock_t t0, t1;

	double eps = exp10(-7);
	double lambda, gamma;//, nx;
	double gamma_xint_plus_P, // in SBar, 0 -> positive
	       gamma_xint_plus_N, // in SBar, 0 -> negative
	       gamma_x0_plus,     // in SBar, non-saturated -> 0
	       gamma_xM_plus,     // in SBar, non-saturated -> saturated
	       gamma_xM_moin_N,   // in SBar, saturated -> non-saturated (negative case)
	       gamma_xM_moin_P,   // in SBar, saturated -> non-saturated (positive case)
	       gamma_wM_plus,     // in S1, non-saturated -> saturated
	       gamma_wM_moin;     // in S1, saturated -> non-saturated

	uword i1, i2, i3, i4, i5, i6, i7, i8;
	int i1_, i2_, i3_, i4_, i5_, i6_, i7_, i8_;

	vec Hy = abs(H->t() * (y - (B * *w)));
	uword I0 = Hy.index_max();
	lambda = Hy.at(I0);

	int N = H->n_rows;
	int Q1 = H->n_cols;
	int Q2 = B.n_cols;

	vec x = zeros(Q1);
	*score =x;

	//this->trick();
	vec r, a1, a2, d1, d2, v1, v2, vv1, vv2, e, c, cc;
	uvec IM1, IM2, Iint2;
	uvec Iint1p, Iint1 = regspace<uvec> (0, 0);

	Iint1(0) = I0;

	d1 = zeros(Q1);
	d2 = zeros(Q2);

	vec res = this->ctx->problem->y - B * *w;

	HomotopyStoppingCriteriaReturn early_state = this->early_stopping(this->ctx, x, *w, d1, d2, pb_param, Iint1, gamma, lambda, *H, B, res, Q2);

	if (early_state.stop) {
		*w = early_state.x_Q1;
		x = early_state.x_Qv;
		return x;
	}

	mat AA = this->ctx->problem->HtH;

	mat BB = AA.submat(uQ1, uQ1);
	mat HH = AA.submat(uQv, uQv);
	mat HB = AA.submat(uQv, uQ1);
	mat BH = AA.submat(uQ1, uQv);



	sp_mat eyeN1;
	eyeN1 = this->ctx->problem->eyeN(span(0, N - 1), span(0, N - 1));
	vec Hr, Br, HAd, a, b;
	mat inv_BB,R,F,P, S , inv_S;

	int niter = 1000;

	vec Etat_var_x = zeros(Q1); //1 -> Xint ; 0-> (x == 0) ; 2 -> X_M
	vec Etat_var_w = ones(Q2); //1 -> Wint ; 2 -> w_M

	IM2 = find(abs(abs(*w) - M) <= exp10(-7));
	w->rows(IM2) = (M * sign(w->rows(IM2)));

	Etat_var_w.elem(IM2) = 2 * ones(IM2.n_rows);
	Iint2 = find(Etat_var_w == 1);

	a1 = zeros(N);


	if (!IM2.is_empty()) {
		a2 = B.cols(IM2) * (w->rows(IM2));
		r = y - a2;

	} else {
		a2 = zeros(N);
		r = y;
	}

	Hr = H->t() * r;
	Br = B.t() * r;

	inv_BB = inv(BB.submat(Iint2, Iint2));
	R = B.cols(Iint2) * inv_BB;
	P = R * -B.cols(Iint2).t();

	F = eyeN1 + P;

	if(!Iint2.is_empty())
		S = H->cols(Iint1).t() * F * H->cols(Iint1);
	else
		S = HH.submat(Iint1,Iint1);

	inv_S = inv(S);


	c = Hr - HB.cols(Iint2) * w->rows(Iint2);// - HH.cols(Iint1) * x.rows(Iint1);
	d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));

	uvec J, es; // arg(x == 0)
	Etat_var_x.at(I0) = 1;

	Iint1 = find(Etat_var_x == 1);
	IM1 = find(Etat_var_x == 2);
	IM2 = find(Etat_var_w == 2);
	Iint2 = find(Etat_var_w == 1);
	J = find(Etat_var_x == 0); // à optimiser


	ChronometerInterface* chrono = alloc_chrono();
	for (int it = 0; it < niter; it++) {

		if (it > this->ctx->itmax)
			this->ctx->itmax = it;
		//	cout << " it:" << it;
		//J = find(Etat_var_x == 0); // à optimiser

		v1 = HH.submat(J, Iint1) * d1.rows(Iint1);

		if(!Iint2.is_empty())
			v2 = HB.submat(J,Iint2) * inv_BB * (BH.submat(Iint2, Iint1) * d1.rows(Iint1));
		//v2 = H->cols(J).t() * R * (BH.submat(Iint2, Iint1) * d1.rows(Iint1));
		else
			v2 = zeros(J.n_elem);


		e = (lambda - c.rows(J)) / (1 + v2 - v1);
		i1 = arg_min_p(J, e, &gamma_xint_plus_P, &i1_, eps);

		e = (lambda + c.rows(J)) / (1 - v2 + v1);
		i2 = arg_min_p(J, e, &gamma_xint_plus_N, &i2_, eps);

		e = -x.rows(Iint1) / d1.rows(Iint1);
		i3 = arg_min_p(Iint1, e, &gamma_x0_plus, &i3_, eps);

		e = (M * sign(x.rows(Iint1)) - x.rows(Iint1)) / d1.rows(Iint1);
		i4 = arg_min_p(Iint1, e, &gamma_xM_plus, &i4_, eps);


		gamma = min(gamma_xint_plus_P,min(gamma_xint_plus_N, min(gamma_x0_plus, gamma_xM_plus)));

		if (!IM1.is_empty()) {

			vv1 = HH.submat(IM1, Iint1) * d1.rows(Iint1);
			vv2 = HB.submat(IM1,Iint2) * inv_BB * (BH.submat(Iint2, Iint1) * d1.rows(Iint1));

			e = (lambda - c.rows(IM1)) / (1 - vv2 + vv1);
			i5 = arg_min_p(IM1, e, &gamma_xM_moin_N, &i5_, eps);

			e = (lambda + c.rows(IM1)) / (1 + vv2 - vv1);
			i6 = arg_min_p(IM1, e, &gamma_xM_moin_P, &i6_, eps);

		}

		if (!Iint2.is_empty()) {
			d2(Iint2) = - inv_BB.t() * (BH.submat(Iint2,Iint1) * d1.rows(Iint1));
			//	d2(Iint2) = -( R.t() * (H->cols(Iint1) * d1.rows(Iint1)));

			e = (M * sign(w->rows(Iint2)) - w->rows(Iint2)) / d2.rows(Iint2);
			i7 = arg_min_p(Iint2, e, &gamma_wM_plus, &i7_, eps);
			gamma = min(gamma, gamma_wM_plus);
		}

		if (!IM2.is_empty()) {

			if (!Iint2.is_empty()) {
				cc = Br.rows(IM2) - BB.submat(IM2, Iint2) * w->rows(Iint2) - BH.submat(IM2, Iint1) * x.rows(Iint1);
			} else {
				cc = Br.rows(IM2) - BH.submat(IM2, Iint1) * x.rows(Iint1);
			}

			vv1 = BH.submat(IM2, Iint1) * d1.rows(Iint1);
			vv2 = B.cols(IM2).t() * R * (BH.submat(Iint2, Iint1) * d1.rows(Iint1));

			e = (cc) / (-vv2 + vv1);
			i8 = arg_min_p(IM2, e, &gamma_wM_moin, &i8_, eps);

			gamma = min(gamma, gamma_wM_moin);

		}

		//  any condition is in force
		if (gamma == exp10(10)) {
			//	cout << "aucune condition it:"<<it<<endl;;
			delete chrono;
			return x;
		}

		//  new solution
		x.elem(Iint1) = x.rows(Iint1) + gamma * d1.rows(Iint1);

		*w += gamma * d2;

		score->elem(Iint1) += abs(x.elem(Iint1));

		lambda = lambda - gamma;

		res = y- *H * x- B * *w;

		HomotopyStoppingCriteriaReturn it_state = this->stopping_criteria(this->ctx, x, *w, d1, d2, pb_param, Iint1, gamma, lambda, *H, B, res, Q2);

		if (it_state.stop) {
			*w = it_state.x_Q1;
			x = it_state.x_Qv;
			delete chrono;
			return x;
		}
		chrono->start();

		//  mettre à jour selon le cas (gamma)
		if (gamma == gamma_x0_plus) {

			Etat_var_x.at(i3) = 0;
			deplacer(i3, &Iint1, &J);
			x.at(i3) = 0;

			if (!Iint2.is_empty()) {
				c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
						* x.rows(Iint1));
			} else {
				c = Hr - HH.cols(Iint1) * x.rows(Iint1);
			}

			//  % update direction Iint
			d1 = zeros(Q1);
			d2 = zeros(Q2);



			inv_S= inversion_rec_del_P(inv_S, i3_);

			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));


		} else if (gamma == gamma_xint_plus_P || gamma == gamma_xint_plus_N) {

			if (gamma == gamma_xint_plus_N)
				i1 = i2;

			Etat_var_x.at(i1) = 1;
			Iint1p = Iint1;


			deplacer(i1, &J, &Iint1);

			if (!Iint2.is_empty()) {
				c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
						* x.rows(Iint1));
			} else {
				c = Hr - HH.cols(Iint1) * x.rows(Iint1);
			}

			//  % update direction Iint
			d1 = zeros(Q1);
			d2 = zeros(Q2);

			inv_S = inversion_rec_add_P(inv_S,	H->cols(Iint1p).t() * F * H->col(i1),	H->col(i1).t() * F * H->col(i1));

			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));

		} else if (gamma == gamma_xM_plus) {
			//cout << "gamma_xM_plus"<<endl;

			Etat_var_x.at(i4) = 2;
			deplacer(i4, &Iint1, &IM1);

			x.row(i4) = (M * sign(x.row(i4)));
			a1 += H->col(i4) * x.row(i4);
			r = y - a1 - a2;
			Hr = H->t() * r;

			if (!Iint2.is_empty()) {
				c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
						* x.rows(Iint1));
			} else {
				c = Hr - HH.cols(Iint1) * x.rows(Iint1);
			}

			//  % update direction Iint
			d1 = zeros(Q1);
			d2 = zeros(Q2);


			inv_S= inversion_rec_del_P(inv_S, i4_ );

			//S = H->cols(Iint1).t() * F * H->cols(Iint1);
			//inv_S = inv(S);

			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));

		} else if (gamma == gamma_xM_moin_N || gamma == gamma_xM_moin_P) {

			if (gamma == gamma_xM_moin_P)
				i5 = i6;
			//cout << "gamma_xM_moin_N"<<endl;

			Etat_var_x.at(i5) = 1;
			deplacer(i5, &IM1, &Iint1);

			x.row(i5) = (M * sign(x.row(i5)));
			a1 -= H->col(i5) * x.row(i5);
			r = y - a1 - a2;
			Hr = H->t() * r;

			c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
					* x.rows(Iint1));

			d1 = zeros(Q1);
			d2 = zeros(Q2);

			S = H->cols(Iint1).t() * F * H->cols(Iint1);
			inv_S = inv(S);
			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));

		} else if (gamma == gamma_wM_plus) {
			//cout << "gamma_wM_plus it: "<<it<<endl;

			Etat_var_w.at(i7) = 2;
			deplacer(i7, &Iint2, &IM2);

			w->row(i7) = (M * sign(w->row(i7)));
			a2 += B.col(i7) * w->row(i7);
			r = y - a1 - a2;
			Hr = H->t() * r;
			Br = B.t() * r;

			inv_BB = inv(BB.submat(Iint2, Iint2));

			R =  B.cols(Iint2) * inv_BB;
			F = this->ctx->problem->eyeN + R * -B.cols(Iint2).t() ;


			S = H->cols(Iint1).t() * F * H->cols(Iint1);


			if (!Iint2.is_empty()) {
				c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
						* x.rows(Iint1));
			} else {
				c = Hr - HH.cols(Iint1) * x.rows(Iint1);
			}

			// % update direction Iint
			d1 = zeros(Q1);
			d2 = zeros(Q2);

			inv_S = inv(S);
			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));



		} else if (gamma == gamma_wM_moin) {
			//	cout << "gamma_wM_moin"<<endl;

			Etat_var_w.at(i8) = 1;
			deplacer(i8, &IM2, &Iint2);

			w->row(i8) = (M * sign(w->row(i8)));
			a2 -= B.col(i8) * w->row(i8);
			r = y - a1 - a2;
			Hr = H->t() * r;
			Br = B.t() * r;

			inv_BB = inv(BB.submat(Iint2, Iint2));
			R = B.cols(Iint2) * inv_BB;
			F = R * -B.cols(Iint2).t() + this->ctx->problem->eyeN;

			if (!Iint2.is_empty()) {
				c = Hr - (HB.cols(Iint2) * w->rows(Iint2)) - (HH.cols(Iint1)
						* x.rows(Iint1));
			} else {
				c = Hr - HH.cols(Iint1) * x.rows(Iint1);
			}

			// % update direction Iint
			d1 = zeros(Q1);
			d2 = zeros(Q2);

			S = H->cols(Iint1).t() * F * H->cols(Iint1);
			inv_S = inv(S);
			d1.elem(Iint1) = inv_S * sign(c.rows(Iint1));

		}

		//t1=clock();
		//this->ctx->T_test +=(float)(t1-t0)/CLOCKS_PER_SEC;
		this->ctx->T_test += chrono->check();

	}//for

	delete chrono;
	return x;

}

HomotopyStoppingCriteriaReturn HomotopyDummyEarlyStopping::operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2)
{
	HomotopyStoppingCriteriaReturn ret;
	ret.stop = false;
	return ret;
}


// **************** P_0/2 problem ****************

// For this problem, we should return as soon as we are satisfying
// the constraint, because we can only increase the size of the support.
#ifdef USE_CPLEX
bound_type CplexRelaxL0L2::operator()(NodeInterface& node, vec xub)
{
	uvec Q1= conv_to <uvec>::from(node.getS1()),
	     Q0= conv_to <uvec>::from(node.getS0()),
	     Qv= conv_to <uvec>::from(node.getSBar());

	const ProblemData& pb = *(this->ctx->problem);
	mat A_Q1 = pb.H.cols(Q1);
	double lb;
	double eps = pb.param.db;

	vec x = this->optim_withCplex_model_bigM_l0l2(pb.H, pb.y,  eps, pb.bigM, Q1, Q0);

	double ls_s1 = norm(pb.y- A_Q1* x(Q1) ,2);
	if(ls_s1<=eps){
		// The s1 support already satisfies the data-fitting requirement
		// => we do not need the convex relaxation, as we already have a solution
		// TODO: should be optimized by avoiding the previous lower bound computations.
		lb = Q1.size();
		node.setLBLS(ls_s1);
		x(Qv).zeros();
		node.setSolution(true);
	} else if (!x.is_empty()) {
		vec b = zeros(pb.H.n_cols);
		b(Q1) = ones(Q1.n_elem);
		node.setFeasible(true);
		if (Qv.n_elem)
			b(Qv) = abs(x(Qv)) / pb.bigM;
		lb = sum(b);
		//if (is_binary(b)) {
		//	node.setSolution(true);
		//}
		double ls_s1_sbar = norm(pb.y- pb.H * x,2);
		if (ls_s1_sbar <= eps && is_binary(b))
			node.setSolution(true);
		node.setLBLS(ls_s1_sbar);
	} else {
		// An empty return from optim_withCplex means we were not able
		// to cope with the constraints, so it's not feasible.
		lb = exp10(10);
		node.setFeasible(false);
	}
	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(lb);

	return make_pair(x, lb);
}

vec CplexRelaxL0L2::optim_withCplex_model_bigM_l0l2(mat A, vec y, double epsilon, double bigM, uvec Q1, uvec Q0) {
	mat AA = this->ctx->problem->HtH;
	vec  Ay;
	Ay = A.t() * y;
	vec yy= y.t()*y;

	//	bigM = 1*max(abs(Ay)) / pow(norm(A.col(1)), 2);
	int Q = A.n_cols;
	vec x_relache = zeros(Q);
	//clock_t t0, t1;
	ChronometerInterface* chrono = alloc_chrono();


	IloEnv env;
	try {

		//t0=clock();
		chrono->start();

		IloModel model(env);
		IloCplex cplex(model);

		//-----------variables------------

		IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
		IloNumVarArray b(env, Q,0,1); //** declaration de Q variables Booléennes

		//** Facultatif:: nommer les variables **//
		/*for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "b" << i + 1;
						b[i] = IloNumVar(env,0,1, name.str().c_str());
					}
		 */
		model.add(x);
		model.add(b);

		//-----------Objective------------

		IloExpr cnstExpr(env);

		// -2 Y' * A * X
		for (int i = 0; i < Q; i++)
			cnstExpr += -2 * Ay(i) * x[i];

		// X'*AA*X
		for (int i = 0; i < Q; i++)
			for (int j = 0; j < Q; j++)
				cnstExpr += x[i] * AA(i, j) * x[j];


		//-----------Contraintes------------

		//	IloRange ctr_sum(env, 0,cnstExpr, epsilon  , "ctr_Qp");
		IloRangeArray ctr(env);

		ctr.add(cnstExpr +yy(0) <= pow(epsilon,2) );
		model.add(ctr);


		for (int i = 0; i < Q; i++) {
			std::stringstream name;
			name << "ctr_bigM_" << i + 1 << "_1";
			model.add(
					IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
							name.str().c_str()));

			name << "ctr_bigM_" << i + 1 << "_2";
			model.add(
					IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
							name.str().c_str()));
			// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
		}

		for (unsigned i = 0; i < Q0.n_rows; i++) {
			model.add(IloRange(env, 0, b[Q0(i)], 0));
		}

		for (unsigned i = 0; i < Q1.n_rows; i++) {
			model.add(IloRange(env, 1, b[Q1(i)], 1));
		}



		IloObjective obj(env, IloSum(b) , IloObjective::Minimize, "OBJ");
		model.add(obj);


		cplex.setOut(env.getNullStream());
		cplex.setParam(IloCplex::Threads, 1); // nombre de threads

		//t1=clock();
		//this->ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
		this->ctx->T_model += chrono->check();

		cplex.solve();

		IloNumArray vals(env);
		cplex.getValues(vals,x);


		for (int i = 0; i < Q; i++)
			x_relache[i]=vals[i];

		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	delete chrono;
	return x_relache;
}
#endif

HomotopyStoppingCriteriaReturn HomotopyL0L2StoppingCriteria::operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2)
{
	double Tau = pb_param;
	vec a, b, c;
	mat HAd;
	HomotopyStoppingCriteriaReturn ret;
	ret.stop = false;

	if (norm(res,2) <= Tau ){
		ret.x_Q1 = w;
		ret.x_Qv = x;
		if (norm(res,2) < Tau ){

			HAd= H *d1 + B *d2;
			c=res.t()*res - pow(Tau,2);
			b= 2 *res.t()*HAd;
			a= HAd.t()*HAd ;

			double  gm= (-b(0) + sqrt(pow(b(0),2) - 4*a(0)*c(0) ))/ (2*a(0));

			ret.x_Qv(Iint1) = x(Iint1) - gm*d1(Iint1);
			if (! w.is_empty())
				ret.x_Q1 = w - gm* d2;

			lambda = lambda + gm;

		}

		//delete chrono;
		//return x;
		ret.stop = true;
	} else if(norm(x,1)/ctx->problem->bigM + Q2 > ctx->UB - ctx->cut) {
		ret.x_Q1 = w;
		ret.x_Qv = x;
		ret.stop = true;
		//delete chrono;
		//return x;
	}
	return ret;
}


HomotopyStoppingCriteriaReturn HomotopyL0L2EarlyStopping::operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2)
{
	HomotopyStoppingCriteriaReturn ret;
	ret.stop = false;
	// If Tau is such that x = zeros() is allowed, do it
	if(pb_param >= pow(norm(res,2),2) ){
		ret.stop = true;
		ret.x_Q1 = w;
		// Should be full of zeros.
		ret.x_Qv = x;
	}
	return ret;
}

bound_type GenericHomotopyL0L2::operator()(NodeInterface& node, vec xub)
{
	//clock_t th,thf;
	uvec Q1= conv_to <uvec>::from(node.getS1()),
	     Q0= conv_to <uvec>::from(node.getS0()),
	     Qv= conv_to <uvec>::from(node.getSBar());
	const ProblemData& pb = *(this->ctx->problem);
	mat A_Q1 = pb.H.cols(Q1);
	mat A_Qv = pb.H.cols(Qv);
	vec xlb;
	xlb = zeros(pb.H.n_cols);
	vec score = xlb;
	double eps = pb.param.db;

	double lsUB = norm(pb.y- A_Q1* xub(Q1) ,2);

	if(lsUB<=eps){
		// Same as before: if xUpperBound satisfies the least-square
		// constraint, we have a solution and should return it.

		node.setLB((double) Q1.size());
		node.setxLB(xub);
		node.setLBLS(lsUB);
		node.setLBL1(norm(xub(Q1), 1));
		node.setSolution(true);
		node.setFeasible(true);
		return make_pair(xub, Q1.size());
	} else if (Qv.is_empty()) {
		// Nothing to be relaxed & does not respect the constraint -> drop
		node.setFeasible(false);
		// Shouldn't these setters be skipped since we will drop the node anyway ?
		node.setLB((double) Q1.size());
		node.setxLB(xub);
		return make_pair(xub, Q1.size());
	}
	ChronometerInterface* chrono = alloc_chrono();
	//th=clock();
	chrono->start();
	vec score_tmp;
	vec w = xub(Q1);
	vec x_Qv = this->mixed_BigM_homotopytemps(A_Q1, &w, &A_Qv,Q1, Qv, pb.y, pb.bigM, eps, &score_tmp);

	score(Qv) = score_tmp;
	node.setLPS(score);

	//thf=clock();
	//this->ctx->T_homo +=(float)(thf-th)/CLOCKS_PER_SEC;
	this->ctx->T_homo += chrono->check();
	delete chrono;

	xlb(Qv)=x_Qv;
	xlb(Q1)=w;

	// TODO: this can be wrong !!
	node.setFeasible(true);

	node.setxLB(xlb);
	double lb = norm(x_Qv,1)/pb.bigM + Q1.size();
	node.setLB(lb);
	node.setLBL1(lb);
	node.setLBLS(norm(pb.y - pb.H*xlb, 2));
	return make_pair(xlb, lb);
}


// **************** P_2/0 problem ****************

#ifdef USE_CPLEX
bound_type CplexRelaxL2L0::operator()(NodeInterface& node, vec xub)
{
	uvec Q1= conv_to <uvec>::from(node.getS1()),
	     Q0= conv_to <uvec>::from(node.getS0()),
	     Qv= conv_to <uvec>::from(node.getSBar());

	const ProblemData& pb = *(this->ctx->problem);
	double lb;
	unsigned int K = pb.param.ui;
	vec x = this->optim_withCplex_model_bigM_l2l0(pb.H, pb.y, K, pb.bigM, Q1, Q0);
	vec b = zeros(pb.H.n_cols);
	b(Q1) = ones(Q1.n_elem);

	if (!x.empty()) {
		lb = pow(norm((pb.y) - (pb.H) * x, 2), 2);
		if (Qv.n_elem)
			b(Qv) = abs(x(Qv)) / pb.bigM;
		if (sum(b) <= K + this->ctx->tolC) {
			node.setFeasible(true);
			if (is_binary(b)) {
				node.setSolution(true);
			}
		}
	} else {
		lb = exp10(10);
		node.setFeasible(false);
	}
	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(sum(b));
	node.setLBLS(lb);

	return make_pair(x, lb);
}

vec CplexRelaxL2L0::optim_withCplex_model_bigM_l2l0(mat A ,vec y, int k, double bigM, uvec Q1, uvec Q0) {
	mat AA = this->ctx->problem->HtH;
	vec  Ay;
	Ay = A.t() * y;
	//	bigM = 1*max(abs(Ay)) / pow(norm(A.col(1)), 2);
	int Q = A.n_cols;
	vec x_relache = zeros(Q);
	//clock_t t0, t1;
	ChronometerInterface * chrono = alloc_chrono();

	IloEnv env;
	try {

		//t0=clock();
		chrono->start();

		IloModel model(env);
		IloCplex cplex(model);

		//-----------variables------------

		IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
		IloNumVarArray b(env, Q,0,1); //** declaration de Q variables Booléennes

		//** Facultatif:: nommer les variables **//
		/*for (int i = 0; i < Q; i++) {
						std::stringstream name;
						name << "b" << i + 1;
						b[i] = IloNumVar(env,0,1, name.str().c_str());
					}
		 */
		model.add(x);
		model.add(b);

		//-----------Objective------------

		IloExpr objExpr(env);

		// -2 Y' * A * X
		for (int i = 0; i < Q; i++)
			objExpr += -2 * Ay(i) * x[i];

		// X'*AA*X
		for (int i = 0; i < Q; i++)
			for (int j = 0; j < Q; j++)
				objExpr += x[i] * AA(i, j) * x[j];

		IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
		model.add(obj);

		//-----------Contraintes------------

		IloRange ctr_sum(env, 0, IloSum(b), k, "ctr_sum");
		model.add(ctr_sum);

		for (int i = 0; i < Q; i++) {
			std::stringstream name;
			name << "ctr_bigM_" << i + 1 << "_1";
			model.add(
					IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
							name.str().c_str()));

			name << "ctr_bigM_" << i + 1 << "_2";
			model.add(
					IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
							name.str().c_str()));
			// or	model.add(IloRange  (env, 0, bigM * b[i]- IloAbs(x[i]), IloInfinity,"ctr_bigM"));
		}

		for (unsigned int i = 0; i < Q0.n_rows; i++) {
			model.add(IloRange(env, 0, b[Q0(i)], 0));
		}

		for (unsigned int i = 0; i < Q1.n_rows; i++) {
			model.add(IloRange(env, 1, b[Q1(i)], 1));
		}

		cplex.setOut(env.getNullStream());
		cplex.setParam(IloCplex::Threads, 1); // nombre de threads



		//t1=clock();
		//this->ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
		this->ctx->T_model += chrono->check();

		cplex.solve();

		IloNumArray vals(env);
		cplex.getValues(vals,x);


		for (int i = 0; i < Q; i++)
			x_relache[i]=vals[i];

		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	delete chrono;
	return x_relache;
}
#endif // USE_CPLEX

HomotopyStoppingCriteriaReturn HomotopyL2L0StoppingCriteria:: operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2)
{
	double Tau = pb_param;
	double nx = norm(x.rows(Iint1), 1);
	HomotopyStoppingCriteriaReturn ret;
	ret.stop = false;
	if (nx >= Tau) {
		ret.stop = true;
		ret.x_Q1 = w;
		ret.x_Qv = x;
		if (nx > Tau) {
			vec sn = sign(x.rows(Iint1) - gamma * d1.rows(Iint1) / 2);
			vec gm = (nx - Tau) / (d1.rows(Iint1).t() * sn);
			ret.x_Qv.rows(Iint1) -= d1.rows(Iint1) * gm;
			ret.x_Q1 -= d2 * gm;
		}
	}
	return ret;
}

bound_type GenericHomotopyL2L0::operator()(NodeInterface& node, vec xub)
{
	//clock_t th, thf;
	uvec Q1, Q0, Qv;
	vec x_Qv, x, b, Bz, w;
	mat P, BB, s;
	mat A_Q1, A_Qv;


	Q1 = conv_to<uvec>::from(node.getS1());
	Q0 = conv_to<uvec>::from(node.getS0());
	Qv = conv_to<uvec>::from(node.getSBar());
	const ProblemData& pb = *(this->ctx->problem);
	double lb;
	unsigned int K = pb.param.ui;

	x = zeros(pb.H.n_cols);
	vec score = x;
	b = zeros(pb.H.n_cols);
	b(Q1) = ones(Q1.n_elem);

	double Tau = (K - Q1.size()) * pb.bigM;

	A_Q1 = pb.H.cols(Q1);
	A_Qv = pb.H.cols(Qv);

	w = xub(Q1);
	if (Q1.size() < K) {
			vec score_tmp;
			//th = clock();
			ChronometerInterface * chrono = alloc_chrono();
			chrono->start();
			x_Qv = this->mixed_BigM_homotopytemps(A_Q1, &w, &A_Qv, Q1, Qv, pb.y, pb.bigM,
					Tau,&score_tmp);

			//thf = clock();
			//this->ctx->T_homo += (float) (thf - th) / CLOCKS_PER_SEC;
			this->ctx->T_homo += chrono->check();
			delete chrono;
			score(Qv) = score_tmp;
			node.setLPS(score);
	}
	x(Q1) = w;

	// We need a tolerance for the constraint here because there is
	// numerical noise coming in.

	if (!x_Qv.empty()) {
		x(Qv) = x_Qv;
		b(Qv) = abs(x(Qv))/pb.bigM;
	}
	lb = pow(norm(pb.y- pb.H* x ,2), 2);

	if (sum(b) <= K + this->ctx->tolC) {
		node.setFeasible(true);
		if (is_binary(b))
			node.setSolution(true);
	}

	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(sum(b));
	node.setLBLS(lb);

	return make_pair(x, lb);
}

// **************** P_2+0 problem ****************

#ifdef USE_CPLEX
bound_type CplexRelaxL2pL0::operator()(NodeInterface& node, vec xub)
{
	uvec Q1= conv_to <uvec>::from(node.getS1()),
	     Q0= conv_to <uvec>::from(node.getS0()),
	     Qv= conv_to <uvec>::from(node.getSBar());

	const ProblemData& pb = *(this->ctx->problem);
	mat A_Q1 = pb.H.cols(Q1);
	vec b = zeros(pb.H.n_cols);
	double lb;
	double ls;
	double l1;
	double lambda = pb.param.db;

	vec x=optim_withCplex_model_bigM_l2pl0_withoutNode(pb.H, pb.y, lambda, pb.bigM, Q1, Q0);

	b(Q1) = ones(Q1.n_elem);

	if (!x.empty()) {
		node.setFeasible(true);

		if (Qv.n_elem)
			b(Qv) = abs(x(Qv)) / pb.bigM;
		ls = pow(norm((pb.y) - (pb.H) * x, 2), 2);
		l1 = sum(b);
	} else {
		ls = pow(norm(pb.y - A_Q1 * x(Q1), 2), 2);
		l1 = (double) Q1.n_elem;
	}

	if (is_binary(b)) {
		node.setSolution(true);
	}

	lb = ls + lambda*l1;
	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(l1);
	node.setLBLS(ls);
	return make_pair(x, lb);
}

vec CplexRelaxL2pL0::optim_withCplex_model_bigM_l2pl0_withoutNode(mat A ,vec y, double lambda, double bigM, uvec Q1, uvec Q0) {
	mat AA = this->ctx->problem->HtH;
	vec  Ay;
	Ay = A.t() * y;
	//	bigM = 1*max(abs(Ay)) / pow(norm(A.col(1)), 2);
	int Q = A.n_cols;
	vec x_relache = zeros(Q);
	vec b_relache = zeros(Q);
	//clock_t t0, t1;
	ChronometerInterface* chrono = alloc_chrono();

	IloEnv env;
	try {

		//t0=clock();
		chrono->start();

		IloModel model(env);
		IloCplex cplex(model);

		//-----------variables------------

		IloNumVarArray x(env, Q, -bigM, bigM); //declaration de Q variables réelles de borne inf -bigM et borne supp bigM
		IloNumVarArray b(env, Q,0,1); //** declaration de Q variables Booléennes

		model.add(x);
		model.add(b);

		//-----------Objective------------

		IloExpr objExpr(env);

		// -2 Y' * A * X
		for (int i = 0; i < Q; i++)
			objExpr += -2 * Ay(i) * x[i];

		// X'*AA*X
		for (int i = 0; i < Q; i++)
			for (int j = 0; j < Q; j++)
				objExpr += x[i] * AA(i, j) * x[j];

		// lambda sum b
		for (int i = 0; i < Q; i++)
			objExpr += lambda * b[i];

		IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
		model.add(obj);

		//-----------Contraintes------------

		for (int i = 0; i < Q; i++) {
			stringstream name;
			name << "ctr_bigM_" << i + 1 << "_1";
			model.add(
					IloRange(env, -IloInfinity, -bigM * b[i] - x[i], 0,
							name.str().c_str()));

			name << "ctr_bigM_" << i + 1 << "_2";
			model.add(
					IloRange(env, 0, bigM * b[i] - x[i], IloInfinity,
							name.str().c_str()));
		}

		for (unsigned int i = 0; i < Q0.n_rows; i++) {
			model.add(IloRange(env, 0, b[Q0(i)], 0));
		}

		for (unsigned int i = 0; i < Q1.n_rows; i++) {
			model.add(IloRange(env, 1, b[Q1(i)], 1));
		}


		cplex.setOut(env.getNullStream());
		cplex.setParam(IloCplex::Threads, 1); // nombre de threads
		cplex.setParam(IloCplex::Param::RootAlgorithm, 1);


		//t1=clock();
		//this->ctx->T_model +=(float)(t1-t0)/CLOCKS_PER_SEC;
		this->ctx->T_model += chrono->check();

		cplex.solve();

		IloNumArray vals_x(env);
		IloNumArray vals_b(env);

		cplex.getValues(vals_x,x);
		cplex.getValues(vals_b,b);


		for (int i = 0; i < Q; i++)
			x_relache[i]=vals_x[i];

		for (int i = 0; i < Q; i++)
			b_relache[i]=vals_b[i];

		env.end();

	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	delete chrono;
	return x_relache;
}
#endif // USE_CPLEX

HomotopyStoppingCriteriaReturn HomotopyL2pL0StoppingCriteria::operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2)
{
	double lmd = pb_param;
	HomotopyStoppingCriteriaReturn ret;
	ret.stop = false;

	if (lambda <= lmd) {
		ret.stop = true;
		ret.x_Q1 = w;
		ret.x_Qv = x;
		if (lambda < lmd) {
			vec sn = sign(x.rows(Iint1) - gamma * d1.rows(Iint1) / 2);
			double gm = lmd-lambda;
			ret.x_Qv.rows(Iint1) -= d1.rows(Iint1) * gm;
			ret.x_Q1 -= d2 * gm;
		}
	}

	return ret;
}

bound_type GenericHomotopyL2pL0::operator()(NodeInterface& node, vec xub)
{
	//clock_t th, thf;
	uvec Q1, Q0, Qv;
	vec x_Qv, x, b, w,score;
	mat A_Q1, A_Qv;

	Q1 = conv_to<uvec>::from(node.getS1());
	Q0 = conv_to<uvec>::from(node.getS0());
	Qv = conv_to<uvec>::from(node.getSBar());
	const ProblemData& pb = *(this->ctx->problem);
	double lambda = pb.param.db;

	mat H = pb.H;

	x = zeros(H.n_cols);
	score = x;
	b = zeros(H.n_cols);

	b(Q1)=ones(Q1.n_elem);

	A_Q1 = H.cols(Q1);
	A_Qv = H.cols(Qv);

	//th = clock();
	ChronometerInterface* chrono = alloc_chrono();
	chrono->start();

	vec score_tmp;
	w = xub(Q1);

	x_Qv = mixed_BigM_homotopytemps(A_Q1, &w, &A_Qv, Q1, Qv, pb.y,
			pb.bigM, lambda/(2*pb.bigM) ,&score_tmp);


	//thf = clock();
	//this->ctx->T_homo += (float) (thf - th) / CLOCKS_PER_SEC;
	this->ctx->T_homo += chrono->check();
	delete chrono;

	score(Qv) = score_tmp;

	x(Q1) = w;
	if (!x_Qv.empty()) {
		x(Qv) = x_Qv;
		node.setFeasible(true);
	}

	b(Qv) = abs(x(Qv)) / pb.bigM;

	if (is_binary(b)) {
		node.setSolution(true);
	}

	node.setLPS(score);

	double ls = pow(norm( (pb.y) - (H) * x, 2), 2);
	double l1 = sum(b);
	double lb = ls + lambda * l1;
	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(l1);
	node.setLBLS(ls);
	return make_pair(x, lb);
}

bound_type ActiveSetL2pL0::operator()(NodeInterface& node, vec xub)
{
	//clock_t th, thf;
	uvec Q1, Q0, Qv;
	vec x_Qv, x, b, w,score;
	mat A_Q1, A_Qv;

	Q1 = conv_to<uvec>::from(node.getS1());
	Q0 = conv_to<uvec>::from(node.getS0());
	Qv = conv_to<uvec>::from(node.getSBar());
	const ProblemData& pb = *(this->ctx->problem);

	mat H = pb.H;
	double lambda = pb.param.db;

	x = zeros(H.n_cols);
	score = x;
	b = zeros(H.n_cols);

	b(Q1)=ones(Q1.n_elem);

	A_Q1 = H.cols(Q1);
	A_Qv = H.cols(Qv);

	ChronometerInterface* chrono = alloc_chrono();
	//th = clock();
	chrono->start();

	vec score_tmp;

	vec x_Qv0 ;
	x_Qv0.reset();

	if(this->ctx->warm_restart){
		x_Qv0 = node.getxLB(true)(Qv);
	}

	w = xub(Q1);
	x_Qv =	this->mixed_BigM_featuresign_l2pl0_withoutNode(A_Q1, &w, &A_Qv, x_Qv0 , Q1, Qv, pb.y, pb.bigM, lambda/(2*pb.bigM));
	score(Qv) =x_Qv;


	//thf = clock();
	//this->ctx->T_homo += (float) (thf - th) / CLOCKS_PER_SEC;
	this->ctx->T_homo += chrono->check();
	delete chrono;


	if (!x_Qv.empty()) {
		x(Qv) = x_Qv;
		node.setFeasible(true);
	}


	x(Q1) = w;

	b(Qv) = abs(x(Qv)) / pb.bigM;

	if (is_binary(b)) {
		node.setSolution(true);
	}

	double ls = pow(norm( (pb.y) - (H) * x, 2), 2);
	double l1 = sum(b);
	double lb = ls + lambda * l1;

	node.setxLB(x);
	node.setLB(lb);
	node.setLBL1(l1);
	node.setLBLS(ls);
	return make_pair(x, lb);
}

vec ActiveSetL2pL0::calculr(vec y,mat B,mat H,vec w,vec x,uvec ind_w_M,uvec ind_x_M,double M,int N) {
	vec a1,a2;
	if (! ind_x_M.is_empty() )
		a1=H.cols(ind_x_M)*(M*sign(x(ind_x_M)));
	else
		a1=zeros(N);

	if (! ind_w_M.is_empty())
		a2=B.cols(ind_w_M)*(M*sign(w(ind_w_M)));
	else
		a2=zeros(N,1);

	return y-a2-a1;
}


double ActiveSetL2pL0::fobj_featuresign(vec x,vec w,mat B,mat H,vec y,double lambda) {
	return 0.5 * pow(norm(y -H*x -B*w),2) + lambda * norm(x,1);
}

vec ActiveSetL2pL0::mixed_BigM_featuresign_l2pl0_withoutNode(mat B, vec *w, mat *H,vec x, uvec uQ1, uvec uQv, vec y, double M, double lambda) {

	double eps = exp10(-3);
	double eps_ = exp10(-10);

	double lambda0;

	vec Hy = abs(H->t() * (y - (B * *w)));

	uword I0 = Hy.index_max();
	lambda0 = Hy.at(I0);

	int N  = H->n_rows;
	int Q1 = H->n_cols;
	int Q2 = B.n_cols;

	if(x.is_empty())
		x = zeros(Q1);


	if(lambda >=lambda0)
		return x;


	uvec ind_x_M, ind_w_M;
	uvec ind_x_in, ind_w_in;
	uvec ind_x_inp, ind_w_inp;

	uvec ind_x_0;

	vec Etat_var_x = ones(Q1); //1 -> Xint ; 0-> (x == 0) ; 2 -> X_M

	ind_x_M = find(abs(abs(x) - M) <= eps);
	x.rows(ind_x_M) = (M * sign(x.rows(ind_x_M)));
	Etat_var_x.elem(ind_x_M) = 2 * ones(ind_x_M.n_rows);

	ind_x_0 = find(abs(x) <= eps);
	x(ind_x_0)= zeros(ind_x_0.n_elem);
	Etat_var_x.elem(ind_x_0) = zeros(ind_x_0.n_rows);

	ind_x_in = find(Etat_var_x == 1);


	vec Etat_var_w = ones(Q2); //1 -> Wint ; 2 -> w_M

	ind_w_M = find(abs(abs(*w) - M) <= eps);
	w->rows(ind_w_M) = (M * sign(w->rows(ind_w_M)));

	Etat_var_w.elem(ind_w_M) = 2 * ones(ind_w_M.n_rows);
	ind_w_in = find(Etat_var_w == 1);

	mat AA = this->ctx->problem->HtH;

	mat BB = AA.submat(uQ1, uQ1);
	mat HH = AA.submat(uQv, uQv);
	mat HB = AA.submat(uQv, uQ1);
	mat BH = AA.submat(uQ1, uQv);


	mat inv_BB, R, F, S, inv_S,inv_Sp;

	//clock_t t0, t1;



	//t0 =clock();
	ChronometerInterface* chrono = alloc_chrono();
	chrono->start();

	if(! ind_w_in.is_empty()){

		inv_BB = inv(BB.submat(ind_w_in, ind_w_in));
		R =  B.cols(ind_w_in) * inv_BB;
		F = this->ctx->problem->eyeN + R * -B.cols(ind_w_in).t() ;
		S = H->cols(ind_x_in).t() * F * H->cols(ind_x_in);
		inv_S = inv(S);
	}else{
		S = HH.submat(ind_x_in,ind_x_in) ;//H->cols(ind_x_in).t() * H->cols(ind_x_in);
		inv_S=inv( S );
	}

	//t1=clock();
	//this->ctx->T_test +=(float)(t1-t0)/CLOCKS_PER_SEC;
	this->ctx->T_test += chrono->check();
	delete chrono;

	vec r, c1, c2,d1,d2, e, grad,thetax;

	uword i1, i2, i3 ;
	int ie1, ie2, ie3 ;

	double sigma,sigma1,sigma2,sigma3;

	sigma = -1;
	sigma1 = -1;
	sigma2 = -1;
	sigma3 = -1;

	int maxit,it2,it=0;

	vec progress, progress_xMp, progress_xMn,	progress_wMp, progress_wMn;
	double min_progress_xMp, min_progress_xMn,	min_progress_wMp, min_progress_wMn, t;


	int ii1, ii2, ii3, ii4;
	uword id_min_progress_xMp, id_min_progress_xMn,	id_min_progress_wMp, id_min_progress_wMn;//, progress_id;
	uvec id_sort_progress;


	vec x_new, w_new, sx_temp, sw_temp;
	double progress_born=1;
	uword id_progress_born, id_lsearch, pos;

	double fobj_lsearch, fobj_temp, lsearch ;

	bool born_x, born_change;

	thetax = sign(x);

	int niter = 1000;

	it=0;

	r= calculr(y,B, *H, *w,x, ind_w_M, ind_x_M, M, N);


	fobj_lsearch = fobj_featuresign(x, *w, B, *H, y, lambda);

	while(it<niter){

		if(it>=1){
			// Activation de variables
			if(! ind_w_in.is_empty() ){
				c1 = H->t()*r - HB.cols(ind_w_in)* w->rows(ind_w_in)- HH.cols(ind_x_in)*x(ind_x_in);
				c2 = B.t()*r - BH.cols(ind_x_in)*x(ind_x_in)- BB.cols(ind_w_in)* w->rows(ind_w_in);
			}else if(! ind_x_in.is_empty()){
				c1 = H->t()*r - HH.cols(ind_x_in)*x(ind_x_in);
				c2 = B.t()*r - BH.cols(ind_x_in)*x(ind_x_in);
			}else{
				c1 = H->t()*r;
				c2 = B.t()*r;
			}

			e= abs(c1(ind_x_0))- lambda ;
			i1 = arg_max_p(ind_x_0, e, &sigma1, &ie1, eps);

			e= (-c1(ind_x_M) % sign(x(ind_x_M)) )+ lambda ;
			i2 = arg_max_p(ind_x_M, e, &sigma2, &ie2, eps);

			e= -c2(ind_w_M) % sign(w->rows(ind_w_M));
			i3 = arg_max_p(ind_w_M, e, &sigma3, &ie3, eps);

			if ( (sigma1<0) && (sigma2<0) && (sigma3<0) ){

				grad = -c1(ind_x_in) % sign(x(ind_x_in))+lambda;

				if(! grad.is_empty()){
					if(max(abs( grad )) < eps){
						this->ctx->nbr_iter += it;
						return x;

						/*node->ind_Qv_in = ind_x_in;
						node->ind_Qv_0 = ind_x_0;
						node->ind_Qv_M = ind_x_M;

						node->invAAQv_in = inv_S;*/
					}
					sigma=-1;

				}else{

					this->ctx->nbr_iter += it;
					return x;
				}
			}

			//if(sigma1>0)
			//sigma = sigma1;
			//else
			//sigma = max(sigma2,sigma3);

			sigma = max(sigma1, max(sigma2,sigma3));

			if( (sigma == sigma1) && (ie1!= -1) ){

				thetax.row(i1) = sign(c1.row(i1));

				ind_x_inp= ind_x_in;
				inv_Sp=inv_S;
				pos =  deplacer(i1, &ind_x_0, &ind_x_in);
				if( pos != -1){

					if(! ind_w_in.is_empty()){

						inv_S = inversion_rec_add_P(inv_S,	H->cols(ind_x_inp).t() * F * H->col(i1),	H->col(i1).t() * F * H->col(i1));

					}else{

						inv_S = inversion_rec_add_P(inv_S, 	H->cols(ind_x_inp).t()  * H->col(i1),	HH.submat(i1,i1,i1,i1) );

					}
				}else{
					cout<<"deplacer1 fail"<<endl;
				}

			}else if( (sigma == sigma2) && (ie2!= -1) ){

				ind_x_inp= ind_x_in;
				inv_Sp=inv_S;
				if( deplacer(i2, &ind_x_M, &ind_x_in) != -1){
					if(! ind_w_in.is_empty()){

						inv_S = inversion_rec_add_P(inv_S,	H->cols(ind_x_inp).t() * F * H->col(i2),	H->col(i2).t() * F * H->col(i2));

					}else{

						inv_S = inversion_rec_add_P(inv_S,	H->cols(ind_x_inp).t()  * H->col(i2),	H->col(i2).t()  * H->col(i2));

					}

					r+= H->col(i2) * x.row(i2);

				}else	cout<<"deplacer2 fail"<<endl;

			}else if( (sigma == sigma3) && (ie3!= -1) ){

				inv_Sp=inv_S;
				if( deplacer(i3, &ind_w_M, &ind_w_in) != -1){
					if(! ind_w_in.is_empty()){

						inv_BB = inv(BB.submat(ind_w_in, ind_w_in));
						R =  B.cols(ind_w_in) * inv_BB;
						F = this->ctx->problem->eyeN + R * -B.cols(ind_w_in).t() ;
						S = H->cols(ind_x_in).t() * F * H->cols(ind_x_in);
						inv_S = inv(S);
					}else{
						S =  H->cols(ind_x_in).t() * H->cols(ind_x_in);
						inv_S=inv( S );
					}

				}else	cout<<"deplacer3 fail"<<endl;

				r+= B.col(i3) * w->row(i3);
			}
		}
		maxit=1000;

		it2=0;
		while(it<niter){

			it2++;
			it++;

			x_new = x;
			w_new = *w;

			if(! ind_w_in.is_empty()){
				x_new(ind_x_in) = inv_S* (H->cols(ind_x_in).t() * F * r - lambda * thetax(ind_x_in));
				w_new(ind_w_in) = R.t() *(r- H->cols(ind_x_in) * x_new(ind_x_in));
			}else
				x_new(ind_x_in) = inv_S* (H->cols(ind_x_in).t()*r - lambda * thetax(ind_x_in));


			if( all(sign(x(ind_x_in)) == sign(x_new(ind_x_in))) || ind_x_in.is_empty() )
				if (all(abs(x_new) <=M) || x_new.is_empty() )
					//    	if(all(sign(w->rows(ind_w_in)) == sign(w_new(ind_w_in))) || ind_w_in.is_empty())
					if (all(abs(w_new) <=M) || w_new.is_empty()){

						x=x_new;
						*w=w_new;
						break;
					}

			d1=(x_new(ind_x_in) - x(ind_x_in));
			d2=(w_new(ind_w_in) - w->rows(ind_w_in));

			progress_xMp = (+M  - x(ind_x_in)) /d1;
			progress_xMn = (-M  - x(ind_x_in)) /d1;
			progress_wMp = (+M  - w->rows(ind_w_in)) /d2;
			progress_wMn = (-M  - w->rows(ind_w_in)) /d2;

			progress_born = 1;

			id_min_progress_xMp = arg_min_p(ind_x_in, progress_xMp, &min_progress_xMp, &ii1, eps_);

			if(progress_born > min_progress_xMp){
				progress_born 	 = min_progress_xMp;
				id_progress_born = id_min_progress_xMp;
				born_x= 1;
			}

			id_min_progress_xMn = arg_min_p(ind_x_in, progress_xMn, &min_progress_xMn, &ii2, eps_);

			if(progress_born > min_progress_xMn){
				progress_born 	 = min_progress_xMn;
				id_progress_born = id_min_progress_xMn;
				born_x= 1;
			}

			id_min_progress_wMp = arg_min_p(ind_w_in, progress_wMp, &min_progress_wMp, &ii3, eps_);

			if(progress_born > min_progress_wMp){
				progress_born 	 = min_progress_wMp;
				id_progress_born = id_min_progress_wMp;
				born_x= 0;
			}

			id_min_progress_wMn = arg_min_p(ind_w_in, progress_wMn, &min_progress_wMn, &ii4, eps_);

			if(progress_born > min_progress_wMn){
				progress_born 	 = min_progress_wMn;
				id_progress_born = id_min_progress_wMn;
				born_x= 0;
			}

			progress = -x(ind_x_in) /d1;
			id_sort_progress = sort_index(progress);

			lsearch = 0;
			//bool pasplus1=0;

			for(unsigned int i=0; i< progress.n_elem ; i++){

				t = progress(id_sort_progress(i));

				if (t <= 0)
					continue;

				if (t >= progress_born)
					break;


				//if(i1 == ind_x_in(id_sort_progress(i)) )

				//	continue;

				sx_temp= x;
				sw_temp= *w;

				sx_temp(ind_x_in)= x(ind_x_in)+ d1*t;
				sw_temp(ind_w_in) = w->rows(ind_w_in)+ d2*t;


				fobj_temp = fobj_featuresign(sx_temp, sw_temp, B, *H, y, lambda);


				if ( fobj_temp < fobj_lsearch ) {
					fobj_lsearch = fobj_temp;
					lsearch = t;
					id_lsearch = ind_x_in(id_sort_progress(i));
				}else{
					//if(pasplus1)
					if (ind_x_in(id_sort_progress(i))== i1)
						continue;
					break;
					//pasplus1 = 1;
				}
			}

			// test obj de progress_born
			t = progress_born;

			sx_temp= x;
			sw_temp= *w;

			sx_temp(ind_x_in)= x(ind_x_in)+ d1*t;
			sw_temp(ind_w_in) = w->rows(ind_w_in)+ d2*t;


			fobj_temp = fobj_featuresign(sx_temp, sw_temp, B, *H, y, lambda);


			if ( fobj_temp <= fobj_lsearch ) {
				fobj_lsearch = fobj_temp;
				lsearch = t;
				id_lsearch = id_progress_born;
			}

			if ( (lsearch >0) && (lsearch < progress_born)){

				x(ind_x_in) = x(ind_x_in)+ ( (x_new(ind_x_in)- x(ind_x_in)) % (lsearch * ones(ind_x_in.n_elem)) ) ;
				w->rows(ind_w_in) = w->rows(ind_w_in)+ ( (w_new(ind_w_in)- w->rows(ind_w_in)) % (lsearch* ones(ind_w_in.n_elem) ) );
				thetax(ind_x_in) = sign(x(ind_x_in));

				born_change=0;
				pos = deplacer(id_lsearch, &ind_x_in, &ind_x_0);
				if( pos  != -1){

					thetax(id_lsearch) = 0;
					inv_S= inversion_rec_del_P(inv_S, pos);

				}else	cout<<"deplacer4 fail"<<endl;

			}else if((lsearch >0) && lsearch == progress_born ){

				x(ind_x_in) = x(ind_x_in)+ ( (x_new(ind_x_in)- x(ind_x_in)) % (lsearch * ones(ind_x_in.n_elem)) ) ;
				w->rows(ind_w_in) = w->rows(ind_w_in)+ ( (w_new(ind_w_in)- w->rows(ind_w_in)) % (lsearch* ones(ind_w_in.n_elem) ) );
				thetax(ind_x_in) = sign(x(ind_x_in));

				born_change=1;


				if (lsearch !=1){
					if(born_x==1){
						pos =  deplacer(id_progress_born, &ind_x_in, &ind_x_M);
						if( pos != -1){

							inv_S= inversion_rec_del_P(inv_S, pos);
							r-= H->col(id_progress_born) * x.row(id_progress_born);

						}else
							cout<<"deplacer 5 fail"<<endl;

					}else if(born_x==0)
						if( deplacer(id_progress_born, &ind_w_in, &ind_w_M) != -1){
							if(! ind_w_in.is_empty()){

								inv_BB = inv(BB.submat(ind_w_in, ind_w_in));
								R =  B.cols(ind_w_in) * inv_BB;
								F = this->ctx->problem->eyeN + R * -B.cols(ind_w_in).t() ;
								S = H->cols(ind_x_in).t() * F * H->cols(ind_x_in);
								inv_S = inv(S);
							}else{
								S =  H->cols(ind_x_in).t() * H->cols(ind_x_in);
								inv_S=inv( S );
							}

							r-= B.col(id_progress_born) * w->row(id_progress_born);

						}else
							cout<<"deplacer 6 fail id:"<< id_progress_born << endl;

					w->rows(ind_w_M)= M* sign(w->rows(ind_w_M));
					x(ind_x_M)= M* sign(x(ind_x_M));

				}//else{
				//	if(it2==1)
				//	break;
				//}


			}else {

				if(it2==1){
					if( (sigma == sigma1) && (ie1!= -1) ){

						thetax.row(i1) = 0;

						pos =  deplacer(i1, &ind_x_in, &ind_x_0);
						if( pos != -1){
							inv_S= inversion_rec_del_P(inv_S, pos);
							//inv_S = inv_Sp;
						}else{
							cout<<"deplacer_1 fail"<<endl;
						}


					}else if( (sigma == sigma2) && (ie2!= -1)  ){
						if( deplacer(i2, &ind_x_in, &ind_x_M) != -1)
							inv_S=inv_Sp;
						else
							cout<<"deplacer_2 fail"<<endl;

					}else if( (sigma == sigma3)  && (ie3!= -1) ){

						if( deplacer(i3,&ind_w_in, &ind_w_M) != -1)
							inv_S=inv_Sp;
						else
							cout<<"deplacer_3 fail"<<endl;

					}

				}else{
					cout<<"lsearch ==0 it2=="<<it2<<endl;
					w->rows(ind_w_M)= M* sign(w->rows(ind_w_M));
					x(ind_x_M)= M* sign(x(ind_x_M));

				}
			}
		}
	}
	cout<<"it : "<< it << endl;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

// **************** P_2/0 + ASC + ANC problem ****************

#ifdef USE_CPLEX
bound_type CplexRelaxL2L0_ASC_ANC::operator()(NodeInterface& node, vec xub) {
	//cout<<">> CplexRelaxL2L0_ASC_ANC::operator() - node " << node.getIdx() << endl;
	uvec  Q1= conv_to <uvec>::from(node.getS1()),
			 	Qb= conv_to <uvec>::from(node.getSBar());
	const ProblemData& pb = *(this->ctx->problem);
	unsigned int K(pb.param.ui);
	double lb(0);
	//vec x = zeros(pb.H.n_cols);
	vec x = this->optim_withCplex_model_l2l0_ASC_ANC(pb.H, pb.y, sort(Qb), sort(Q1), node.getIdx());
	if (!x.empty()) {
			lb = pow(norm((pb.y) - (pb.H) * x, 2), 2);
			// Les noeuds de bornes pour lesquels on calcule une borne inférieure ne sont jamais solutions
			// Mais peuvent être réalisables
			node.setFeasible(true);
	} else {
			lb = exp10(10);
			node.setFeasible(false);
	}
	//
	node.setSolution(false);
	node.setxLB(x);
	node.setLB(lb);
	node.setLBLS(lb);
	return make_pair(x, lb);
}

vec CplexRelaxL2L0_ASC_ANC::optim_withCplex_model_l2l0_ASC_ANC(mat H ,vec y,  uvec Qb, uvec Q1, unsigned long long int n) {
	bool ASC_asEq(ctx->problem->get_ASC_formulation());
	mat HtH(this->ctx->problem->HtH);
	vec Hy(H.t()*y);
	vec x_relache = zeros(H.n_cols);
	ChronometerInterface * chrono = alloc_chrono();


	//cout << "ASC_asEq = " << ASC_asEq << endl;
	IloNum lb_ASC(1.);
	if (!ASC_asEq){ lb_ASC = 0.;}
	//cout << "lb_ASC = " << lb_ASC << endl;
	//pause_cin_txt("LB = CplexRelaxL2L0_ASC_ANC");


	IloEnv env;
	try {
				uvec Qv = sort(join_cols(Qb,Q1));		// Qv = Qvalide
				chrono->start();
				if (H.n_rows >= H.n_cols) {
						mat sub_HtH(HtH(Qv,Qv));
						//
						IloModel model(env);
						IloCplex cplex(model);
						//
						IloNumVarArray x(env,Qv.n_elem,0,1);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						//
						IloExpr objExpr(env);
						for (int i(0); i<Qv.n_elem;++i){
								objExpr += -2*Hy(Qv(i))*x[i];
								for (int j(0); j < Qv.n_elem; ++j) {
										objExpr += x[i]*sub_HtH(i,j)*x[j];
								}
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
						model.add(obj);
						//
						//IloRange ctr_sum(env, 0, IloSum(x), 1, "ctr_sum");
						IloRange ctr_sum(env, lb_ASC, IloSum(x), 1, "ctr_sum");
						model.add(ctr_sum);
						//
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_bnd;
								name_bnd << "ctr_BND_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_bnd.str().c_str()));
						}
						//
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1);
						this->ctx->T_model += chrono->check();
						//
						cplex.solve();
						IloNumArray vals(env);
						cplex.getValues(vals,x);
						//
						for (int i = 0; i < Qv.n_elem; ++i){
								x_relache[Qv(i)]=vals[i];
						}
						//x_relache.t().print("x_relache");
						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_OC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
				} else {
						mat sub_H = H.cols(Qv);
						//
						IloModel model(env);
						IloCplex cplex(model);
						//
						IloNumVarArray x(env,Qv.n_elem,0,1);
						IloNumVarArray z(env,sub_H.n_rows,-IloInfinity,+IloInfinity);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						for (int i; i <H.n_rows; ++i ) {	std::stringstream name;	name << "z" << i + 1;	z[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						model.add(z);
						//
						IloExpr objExpr(env);
						for (int i(0);i<sub_H.n_rows;++i){
								objExpr+= z[i]*z[i];
								objExpr+= -2*z[i]*y(i);		// Note : z variable, y valeur (vecteur)
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize,"OBJ");
						model.add(obj);
						//

						//IloRange ctr_sum(env,0,IloSum(x),1,"ctr_ASC");
						IloRange ctr_sum(env, lb_ASC, IloSum(x), 1, "ctr_sum");
						model.add(ctr_sum);
						//
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_anc;
								name_anc << "ctr_ANC_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_anc.str().c_str()));
						}
						//
						for(int i(0);i<sub_H.n_rows;++i) {
								std::stringstream name;
								name << "ctr_eq_lin_" << i;
								IloExpr Hx_i(env);
								for (int j(0); j<Qv.n_elem; ++j){
										Hx_i+= H(i,Qv(j))*x[j];
								}
								model.add(IloRange(env,0,Hx_i-z[i],0,name.str().c_str()));
						}
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1); // nombre de threads
						this->ctx->T_model += chrono->check();
						//
						cplex.solve();
						//
						IloNumArray vals(env);
						cplex.getValues(vals,x);
						for (int i(0); i < Qv.n_elem; ++i) {
								x_relache[Qv(i)] = vals[i];
						}
						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_UC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
						//pause_cin_txt("LB solver - next ?");
				}
	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	delete chrono;
	return x_relache;
}
#endif // USE_CPLEX
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

bound_type FCLS_L2L0_ASC_ANC::operator()(NodeInterface& node, vec xub) {
		bool verbose(false);
		uvec Q1,Qb,Qv;
		mat H_Qv;
		vec x,x_Qv;
		double lb(0);
		//
		Q1 = conv_to<uvec>::from(node.getS1());
		Qb = conv_to<uvec>::from(node.getSBar());
		Qv = sort(join_cols(Qb,Q1));
		//
		const ProblemData& pb = *(this->ctx->problem);
		x = zeros(pb.H.n_cols);

		// Valide seulement pour les instances de type Q <= N
		if (pb.H.n_cols > pb.H.n_rows) {
				cout << "Erreur : FCLS valide pour N >= Q seulement;" << endl;
				// this->ctx->force_stop = true;
				// return make_pair(x, exp10(10));
				abort();
		}
		//
		H_Qv = pb.H.cols(Qv);
		ChronometerInterface * chrono = alloc_chrono();
		chrono->start();
		x_Qv = this->FCLS_L2L0(H_Qv,Qv,pb.y,node.getIdx());

		this->ctx->T_FCLS += chrono->check();

		delete chrono;
		//
		if (!x_Qv.empty()) {
				x(Qv) = x_Qv;
				lb = pow(norm(pb.y- pb.H* x ,2), 2);
				node.setFeasible(true);
		} else {
				lb = exp10(10);
				node.setFeasible(false);
		}
		//
		//node.setSolution(false);
		// Par défaut, on sait que FCLS produit du sparse ... on peut donc supposer qu'il y aura des cas de 0-branching et des
		// solution sparse

		node.setxLB(x);
		node.setLB(lb);
		node.setLBLS(lb);
		//
		node.updateSvNz(x);
		if (node.getNZ().size() <= pb.param.ui ) {		node.setSolution(true);	}
		else {	node.setSolution(false);	}

		/*
		// -----------------------ABSURDITE-----------------------------------------
		bool absurde(false);
		bool negX(false);
		// Valeur dégueu trouvée par matlab correspondant à des pouièmes numériques : 1.00000000001566635710
		for (int i(0); i < x.size(); ++i) {	if (x[i] < 0 || abs(x[i]) > 1.00000000001568) {	absurde = true;	}	}
		cout << "SELECTION_FCLS (LB)\tSUM = " << sum(x) << endl;
		for (int i(0); i < x.size(); ++i) {
				if (x[i] != 0) {	string tok = x[i] < 0 ? "-":"+";	cout << "\t"<<tok<<"\tx["<<i<<"] = " << x[i] << endl;	}
				if( x[i] < 0 ) {negX = true;}
		}	cout << endl;
		//TODO : Créer le répertoire NEG_INST_FCLS
		if(negX) {
			string tok("LB");
			vec m_y = conv_to<vec>::from(pb.y);
			mat m_H = pb.H;
			mat m_x = conv_to<mat>::from(x);
			mat m_Q = conv_to<mat>::from(Qv);

			std::stringstream file_y;	file_y << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_y.txt";
			std::stringstream file_H;	file_H << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_H.txt";
			std::stringstream file_x;	file_x << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_x.txt";
			std::stringstream file_Q;	file_Q << "NEG_INST_FCLS/"<<tok<<"/" << this->ctx->inst_id << "_" << node.getIdx() << "_Q.txt";
			m_y.save(file_y.str(),raw_ascii);
			m_Q.save(file_Q.str(),raw_ascii);
			m_H.save(file_H.str(),raw_ascii);
			m_x.save(file_x.str(),raw_ascii);
			this->ctx->force_stop = true;
			//pause_cin_txt("Neg x");
			cout << this->ctx->inst_id << endl;
			//abort();
		}
		*/

		return make_pair(x, lb);
}

vec FCLS_L2L0_ASC_ANC::FCLS_L2L0(mat H, uvec uQv, vec y, unsigned long long int n) {
		bool verbose(false);

		if(verbose) {cout << "FCLS(LB) node n°"<< n << endl;}
		//
		double tol(exp10(-9));
		int N(H.n_rows),Q(H.n_cols),iter(0),iter_ow(0);		// ,iter_o_w(0)
		// Initialisations
		mat dH,E,EtE,iEtE;
		vec One(ones(Q,1)), iEtEOne(zeros(Q,1)),W(zeros(Q,1)),dy(zeros(N,1)),f(zeros(N+1,1)),Etf(zeros(Q,1)),ls(zeros(Q,1)),x(zeros(Q,1)),x_Old(zeros(Q,1));
		double delta(1./10./max(y)),sumIEtEOne,lambdiv;
		//
		dH = delta*H;
		E = join_cols(One.t(),dH);
		EtE = E.t()*E;
		iEtE = inv(EtE);
		iEtEOne = iEtE*One;
		sumIEtEOne = sum(iEtEOne);
		W = iEtE.diag();
		dy = delta*y;
		f = join_cols(ones(1),dy);
		Etf = E.t()*f;
		//
		ls = iEtE*Etf;
		// The result of the product is an expression template called arma::Glue which can be converted to a 1x1 matrix. To do this inline and assign it to a double evaluate it explicitly using .eval() and take the only element which is (0,0).
		lambdiv = (-(1.-(ls.t()*One)) /sumIEtEOne).eval()(0,0);
		x = ls - lambdiv*iEtEOne;
		x_Old = x;
		// --------------------- //

		mat L;//,M_1,M_2;														// Création de la matrice L pour le calcul des lagrangiens
		mat sL1,sL2,cL;															// Sauvegarde de la matrice L hors (sL1) et dans (sL2) la boucle while d'exclusion des max_neg; cL la matrice sur laquelle on effectue le test d'arrêt
		vec x_e,lag,max_neg;
		uword idx_max_neg;												// index du "most positive" lambda (while 2)
		uvec R, P(regspace<uvec>(0,Q-1));

		if(verbose){cout << "R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
		if(verbose){cout << "P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
		if(verbose){cout <<"------" << endl;}

		// --------------------- //
		if (!find(x < -tol).is_empty()) {
			iter = 0;
			R.clear();
			while (!find(x < -tol  && abs(x)>1e-5 ).is_empty()) {
					// On ne peut pas déplacer de P dans R un indice qui n'est pas dans P ...
					// => Provient de merde numériques à 10e-9 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					deplacer(find(x < -tol && abs(x)>1e-5), &P, &R,"P->R");
					// deplacer(find(x < -tol), &P, &R,"P->R");
					R = sort(R);
					if(verbose){cout << "R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
					if(verbose){cout << "P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
					// Reset x
					x = x_Old;
					L = getFCLS_Matrix(R,iEtE,iEtEOne,One);
					sL1.clear();	sL1 = L;
					x_e = x(R); x_e.insert_rows(x_e.n_rows,1);
					lag = inv(L)*x_e;
					// if (verbose) {lag.t().print("lag");}
					//if (verbose) {pause_cin_txt("Next step ? ");}
//					while (!find(lag.rows(0,R.size()-1)>0).is_empty()) {
					while (!find(lag.rows(0,R.size()-1)>1e-6).is_empty()) {		// PATCH SEB
							//max_neg = W(R)%lag.rows(0,R.size()-1);		// %	: element-wise multiplication of two objects

							max_neg = lag.rows(0,R.size()-1);		// %	: element-wise multiplication of two objects

							idx_max_neg = max_neg.index_max();
							if(verbose){cout << "idx_max_neg => " << idx_max_neg << "\t R(idx_max_neg) = " << R(idx_max_neg) << "\t max_neg = "<< max_neg(idx_max_neg) <<endl;}
							deplacer(R(idx_max_neg),&R,&P,"R->P");
							//cout << "\n" << endl;
							if(verbose){cout << "R : ";for (auto elt : R) {cout << elt << " ";} cout << endl;}
							if(verbose){cout << "P : ";for (auto elt : P) {cout << elt << " ";} cout << endl;}
							//
							L = getFCLS_Matrix(R,iEtE,iEtEOne,One);
							sL2.clear();	sL2 = L;
							//
							x_e = x(R); x_e.insert_rows(x_e.n_rows,1);
							lag = inv(L)*x_e;
							// if (verbose) {lag.t().print("lag");}
							//if(verbose){pause_cin_txt("While_2 - NEXT ! ");}
					}
					if (!R.is_empty()) {
							x = x - iEtE.cols(R)*lag.rows(0,R.size()-1) - lag(R.size())*iEtEOne;
							// if (verbose) {x.print("x");}
					}
					iter++;

					if (verbose && iter % 10000 == 0) {
							cout << "LB \t ITER = " << iter <<"\t node "<< n <<endl;
					}

					// Le cas de "base" est L = sL2;
					cL = sL2;
					if (L.n_rows == sL1.n_rows) {	cL = sL1;}
					//
					if (all(all(iter > cL))) {
							//pause_cin_txt("all(all(iter > cL)) - next ? ");
							break;
					}
					//if(verbose){pause_cin_txt("While_1 - NEXT ! ");}
			}
			iter_ow++;
			//cout << "ITER_OW = " << iter_ow << endl;
			if (all(all(iter_ow > 10*cL))) {
					// Cette condition est liée à la boucle pour tout pixel dans l'image étudiée (implémentation à la said)
					//pause_cin_txt("all(all(iter_ow > 10*cL)) - next ? ");
					//break;		// Pas dans une boucle ... quel intérêt ??
			}

		}	//while (!find(x < -tol).is_empty()) {
		//cout << "PASSAGE PAR ICI" << endl;		// Tester le break;
		uvec neglig = find(abs(x)<1e-7);
		x.elem(neglig) = zeros(neglig.size());
		//pause_cin_txt("NEXT ! ");
		return x;
}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

bound_type OSQP_RelaxL2L0_ASC_ANC::operator()(NodeInterface& node, vec xub) {
		//cout << ">> bound_type OS_RelaxL2L0_ASC_ANC::operator() - node " << node.getIdx() << endl;
		uvec Q1,Qb;
		double lb(0);
		const ProblemData& pb = *(this->ctx->problem);
		Q1 = conv_to<uvec>::from(node.getS1());
		Qb = conv_to<uvec>::from(node.getSBar());
		//vec x = zeros(pb.H.n_cols);
		vec x= this->optim_withOSQP_model_l2l0_ASC_ANC(pb.H, pb.y, sort(Qb), sort(Q1),node.getIdx());
		if (!x.empty()) {
				lb = pow(norm( (pb.y) - pb.H * x, 2), 2);
				node.setFeasible(true);
		} else {
			lb = exp10(10);
			node.setFeasible(false);
		}

		//
		node.setxLB(x);
		node.setLB(lb);
		node.setLBLS(lb);
		//
		node.updateSvNz(x);
		if (node.getNZ().size() <= pb.param.ui ) {		node.setSolution(true);	}
		else {	node.setSolution(false);	}
		//
		return make_pair(x,lb);
}

vec OSQP_RelaxL2L0_ASC_ANC::optim_withOSQP_model_l2l0_ASC_ANC(mat H ,vec y, uvec Qb, uvec Q1, unsigned long long int n){
		bool ASC_asEq(ctx->problem->get_ASC_formulation());
		//cout << "ASC_asEq = " << ASC_asEq << endl;

		ChronometerInterface * chrono = alloc_chrono();
		chrono->start();
		uvec Qv =  sort(join_cols(Qb,Q1));
		//
		double eps = 1e-10;
		unsigned long maxiter = 30000;
		mat HtH(this->ctx->problem->HtH), sub_HtH(HtH(Qv,Qv));
		int nV(sub_HtH.n_cols),nC(1);
		//
		vec Hty(-1*H.cols(Qv).t() * y),rows_sub_HTH(reshape(sub_HtH,nV*nV,1));
		vec lower_arma(zeros(sub_HtH.n_cols, 1)),upper_arma(ones(sub_HtH.n_cols, 1));
		vec A_arma = arma::ones(sub_HtH.n_rows,1);				// La matrice des contraintes
		vec x_relache = zeros(H.n_cols);
		//
		real_t Q[nV*nV];								// qpOASES::real_t Q[nV*nV];
		real_t A[nV];										// qpOASES::real_t A[nV];
		real_t g[nV];										// qpOASES::real_t g[nV];
		real_t lb[nV];									// qpOASES::real_t lb[nV];
		real_t ub[nV];									// qpOASES::real_t ub[nV];
		real_t ubA[1] = { 1. };					// qpOASES::real_t ubA[1] = { 1. };

		real_t lbA[1] = { 1. };					// qpOASES::real_t lbA[1] = { 0. };
		// ATTENTION => lbA[1] définit le vecteur de taille 1
		// Pour y acceder, c'est avec par indicage au départ de 0.
		if (!ASC_asEq){ lbA[0] = 0.;}
		//cout << "lbA = " << lbA[0] << endl;
		//pause_cin_txt("LB = OSQP_RelaxL2L0_ASC_ANC");


		int_t nWSR = 10;								// qpOASES::int_t nWSR = 10;
		nWSR = 5 *(nV + nC);						// Manual - default value;
		//
		for (int k(0); k<rows_sub_HTH.n_rows; ++k) {
				Q[k] = rows_sub_HTH[k];
		}
		for (int k(0); k<Hty.n_rows; ++k) {
				g[k] = Hty[k];
		}
		for (int k(0);k<lower_arma.n_rows;++k) {
				lb[k] = lower_arma[k]; 	ub[k] = upper_arma[k];
		}
		//A_arma.print("A_arma");
		for (int k(0); k<A_arma.n_rows; ++k) {
				A[k] = A_arma[k];
		}
		//
		// qpOASES::QProblem P(nV,nC,qpOASES::HST_UNKNOWN,qpOASES::BT_TRUE);
		QProblem P(nV,nC,HST_UNKNOWN,BT_TRUE);
		Options myOptions;
		myOptions.printLevel = PL_LOW;
		P.setOptions(myOptions);
		//
		// Temporair A SUPPRIMER
		//double save_chrono(chrono->check());
		//vec tempo_T(1); tempo_T.at(0) = save_chrono;
		//this->ctx->T_model += save_chrono;
		// Original
		this->ctx->T_model += chrono->check();


		real_t xOpt[nV];
		P.init( Q,g,A,lb,ub,lbA,ubA, nWSR );
		P.getPrimalSolution( xOpt );
		//
		for (int i = 0; i < Qv.n_elem; ++i){
				x_relache[Qv(i)]=xOpt[i];
				//cout << Qv(i) << "\t"<< x_relache[Qv(i)]<<endl;
		}
		delete chrono;

		// if (abs(sum(x_relache)-1) > 1e-10) {
		// 		cout << "\\/ LB \t" << this->ctx->inst_id << " \t "<< n <<" \t " << sum(x_relache) << endl;
		// 		//pause_cin_txt("LB NOT 1");
		// }



		return x_relache;
}
/*
vec OSQP_RelaxL2L0_ASC_ANC::optim_withCplex_model_l2l0_ASC_ANC(mat H ,vec y, uvec Qb, uvec Q1, unsigned long long int n) {
	cout << ">>>> Cplex_mainOS_LB_operator()" << endl;
	mat HtH(this->ctx->problem->HtH);
	vec Hy(H.t()*y);
	vec x_relache = zeros(H.n_cols);
	ChronometerInterface * chrono = alloc_chrono();

	IloEnv env;
	try {
				uvec Qv = sort(join_cols(Qb,Q1));		// Qv = Qvalide
				chrono->start();
				if (H.n_rows >= H.n_cols) {
						mat sub_HtH(HtH(Qv,Qv));
						IloModel model(env);
						IloCplex cplex(model);
						IloNumVarArray x(env,Qv.n_elem,0,1);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						IloExpr objExpr(env);
						for (int i(0); i<Qv.n_elem;++i){
								objExpr += -2*Hy(Qv(i))*x[i];
								for (int j(0); j < Qv.n_elem; ++j) {
										objExpr += x[i]*sub_HtH(i,j)*x[j];
								}
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
						model.add(obj);
						IloRange ctr_sum(env, 0, IloSum(x), 1, "ctr_sum");
						model.add(ctr_sum);
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_bnd;
								name_bnd << "ctr_BND_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_bnd.str().c_str()));
						}
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1); // nombre de threads
						this->ctx->T_model += chrono->check();
						cplex.solve();
						IloNumArray vals(env);
						cplex.getValues(vals,x);
						for (int i = 0; i < Qv.n_elem; ++i){
								x_relache[Qv(i)]=vals[i];
						}
						//x_relache.t().print("x_relache");
						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_OC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
				} else {
						mat sub_H = H.cols(Qv);
						IloModel model(env);
						IloCplex cplex(model);
						IloNumVarArray x(env,Qv.n_elem,0,1);
						IloNumVarArray z(env,sub_H.n_rows,-IloInfinity,+IloInfinity);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						for (int i; i <H.n_rows; ++i ) {	std::stringstream name;	name << "z" << i + 1;	z[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						model.add(z);
						IloExpr objExpr(env);
						for (int i(0);i<sub_H.n_rows;++i){
								objExpr+= z[i]*z[i];
								objExpr+= -2*z[i]*y(i);		// Note : z variable, y valeur (vecteur)
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize,"OBJ");
						model.add(obj);
						IloRange ctr_ASC(env,0,IloSum(x),1,"ctr_ASC");
						model.add(ctr_ASC);
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_anc;
								name_anc << "ctr_ANC_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_anc.str().c_str()));
						}
						for(int i(0);i<sub_H.n_rows;++i) {
								std::stringstream name;
								name << "ctr_eq_lin_" << i;
								IloExpr Hx_i(env);
								for (int j(0); j<Qv.n_elem; ++j){
										Hx_i+= H(i,Qv(j))*x[j];
								}
								model.add(IloRange(env,0,Hx_i-z[i],0,name.str().c_str()));
						}
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1); // nombre de threads
						this->ctx->T_model += chrono->check();
						cplex.solve();
						IloNumArray vals(env);
						cplex.getValues(vals,x);
						double sum_x;
						for (int i(0); i < Qv.n_elem; ++i) {
								sum_x += vals[i];
								x_relache[Qv(i)] = vals[i];
						}
						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_UC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
						//pause_cin_txt("LB solver - next ?");
				}
	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}
	delete chrono;
	return x_relache;
}
*/


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


bound_type GenericHomotopyL2L0_ASC_ANC::operator()(NodeInterface& node, vec xub) {
		uvec Q1, Q0,Qb, Qv;
		vec x_Qv, x, w;
		mat A_Q1, A_Qv;
		//
		Q1 = conv_to<uvec>::from(node.getS1());
		Q0 = conv_to<uvec>::from(node.getS0());
		Qb = conv_to<uvec>::from(node.getSBar());

		Qv = sort(join_cols(Qb,Q1));
		//
		const ProblemData& pb = *(this->ctx->problem);
		double pb_param = 1;				// Tau
		double lb;
		double eps_neglig = exp10(-7);
		double eps_delta_sto = exp10(-10);

		bool verbose(false);

		//
		unsigned int K = pb.param.ui;
		x = zeros(pb.H.n_cols);
		//
		//A_Q1 = pb.H.cols(Q1);
		A_Qv = pb.H.cols(Qv);
		//
//		if (Q1.size() < K) {
				ChronometerInterface * chrono = alloc_chrono();
				chrono->start();
				x_Qv = this->homotopy_ASC_ANC(&A_Qv, Qv, pb.y,pb_param, node.getIdx());
				this->ctx->T_homo += chrono->check();
				delete chrono;
				//
				uvec neglig = find(abs(x_Qv)<eps_neglig);
				x_Qv.elem(neglig) = zeros(neglig.size());

//		}

		if (!x_Qv.empty()) {
				x(Qv) = x_Qv;
				node.setFeasible(true);
		}
		lb = pow(norm(pb.y- pb.H* x ,2), 2);
		//

		if (verbose) {cout << "HP "<< node.getIdx()	<<"\t SUM = "<< norm(x,1) << "\t LB = "<< lb << "\t delta_neglig =" << abs(norm(x,1)-1) << " |delta_neglig| < eps_neglig" << (abs(norm(x,1)-1) < eps_delta_sto) << endl;}
		int neg_x = -1;
		int nz_x(0);
		for (int i(0); i < x.size(); ++i) {
				string tok ="";
				if (x[i] != 0) {
						if (x[i]<0 && x[i]<-1e-10) {neg_x = i; tok = "!!!";}
						if (verbose) {cout << "\tx["<<i<<"] = " << x[i] <<" "<<tok <<  endl;}
						++nz_x;
				}
		}

		if (verbose) {cout << "NZ_x = " << nz_x << "\t N = " << pb.H.n_rows << endl;}
		bool break_negx(false);
		if (neg_x != -1 || sum(x)==0) {
				cout << "!!! x["<<neg_x <<"] = "<< x[neg_x] << endl;
				break_negx = true;
				cout << this->ctx->inst_id << " node : " << node.getIdx() << endl;
				// Pour la résolution matlab

				vec m_y = conv_to<vec>::from(pb.y);
				mat m_H = pb.H;
				mat m_x = conv_to<mat>::from(x);
				mat m_Q = conv_to<mat>::from(Qv);

				std::stringstream file_y;	file_y << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_y.txt";
				std::stringstream file_H;	file_H << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_H.txt";
				std::stringstream file_x;	file_x << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_x.txt";
				std::stringstream file_Q;	file_Q << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_Q.txt";
				m_y.save(file_y.str(),raw_ascii);
				m_Q.save(file_Q.str(),raw_ascii);
				m_H.save(file_H.str(),raw_ascii);
				m_x.save(file_x.str(),raw_ascii);

				//abort();
				//pause_cin_txt("ICI");
				this->ctx->force_stop = true;
				cout << this->ctx->inst_id << "\t" << node.getIdx() << endl;
		}



		/*
		vec m_y = conv_to<vec>::from(pb.y);
		mat m_H = pb.H;
		mat m_x = conv_to<mat>::from(x);
		mat m_Q = conv_to<mat>::from(Qv);

		std::stringstream file_y;	file_y << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_y.txt";
		std::stringstream file_H;	file_H << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_H.txt";
		std::stringstream file_x;	file_x << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_x.txt";
		std::stringstream file_Q;	file_Q << "NEG_INST_HOMOTOPY/" << this->ctx->inst_id << "_" << node.getIdx() << "_Q.txt";
		m_y.save(file_y.str(),raw_ascii);
		m_Q.save(file_Q.str(),raw_ascii);
		m_H.save(file_H.str(),raw_ascii);
		m_x.save(file_x.str(),raw_ascii);
		*/


		node.setxLB(x);
		node.setLB(lb);
		node.setLBLS(lb);
		/////////////////////////////////////////////////////
		node.updateSvNz(x);
		if (node.getNZ().size() <= pb.param.ui && abs(norm(x,1)-1) < eps_delta_sto && !this->ctx->force_stop) {		node.setSolution(true);	}
		else {	node.setSolution(false);	}



		return make_pair(x, lb);
}

vec GenericHomotopy_ASC_ANC::homotopy_ASC_ANC(mat *H, uvec uQv, vec y, double pb_param,unsigned long long int no_node) {
		bool verbose(false);//(no_node==0);//(no_node==11811160062);//(no_node == 382);//(false);//(no_node == 2 || no_node == 0);
		bool patch_neg_lambda(true);
		bool smart_inv(false);
		//if (no_node == 139262) {verbose = true;}
		double eps_o_i = exp10(-8);
		double eps_i_o = exp10(-8);
		double eps_lmbd_neg = exp10(-10);

		//double eps_i_o = 0;//exp10(-7);

		int niter = 1000;
		//
		int N(H->n_rows), Q1(H->n_cols);
		//
		double lambda, gamma;
		double gamma_xint_plus_P, // in SBar, 0 -> positive non saturée
					 gamma_x0_plus;     // in SBar, non-saturated -> 0

		uword i1, i3;			 //uword i1, i2, i3, i4, i5, i6, i7, i8;
		int i1_, i3_;

		vec Hy = H->t() * y;
		uword I0 = Hy.index_max();
		lambda = Hy.at(I0);




		vec x(zeros(Q1)), r(y), a1(zeros(N)), d1(zeros(Q1)), v1, e, c;
		uvec Iint1p, I = regspace<uvec> (0, 0);
		uvec sub_I = regspace<uvec> (0, 0);		// cas spécifique i3;
		//
		I(0) = I0;
		vec res = this->ctx->problem->y;

		mat AA = this->ctx->problem->HtH;
		mat HH = AA.submat(uQv, uQv);
		// DEBUG
		mat A = this->ctx->problem->H.cols(uQv);

		//
		sp_mat eyeN1;
		eyeN1 = this->ctx->problem->eyeN(span(0, N - 1), span(0, N - 1));
		vec Hr, Br, HAd, a, b;
		mat inv_BB,R,F,P, S , inv_S;
		vec Etat_var_x = zeros(Q1); //1 -> Xint ; 0-> (x == 0) ;




		Hr = H->t() * r;

		F = eyeN1; // + P;		// Voir page 64 - projection avec un - P au leu de P
		S = HH.submat(I,I);
		inv_S = inv(S);

		c = Hr ;// - HH.cols(I) * x.rows(I);		// - HB.cols(Iint2) * w->rows(Iint2);//
		d1.elem(I) = inv_S * sign(c.rows(I));

		uvec J;
		Etat_var_x.at(I0) = 1;

		I = find(Etat_var_x == 1);
		J = find(Etat_var_x == 0); // à optimiser
		////////////////////////////////////////////////////////////////////////////
		HomotopyMovement last_move(HomotopyMovement::Init);
		double def_index(max(uQv)+666);
		uword tabu_index(def_index);	// Un peu de magie noire
		////////////////////////////////////////////////////////////////////////////

		ChronometerInterface* chrono = alloc_chrono();
		for (int it = 0; it < niter; it++) {
			if (it > this->ctx->itmax) {
					this->ctx->itmax = it;
			}
			//
			string tok_lmv("Init");
			if (last_move==HomotopyMovement::ItO) {tok_lmv = "ItO";}
			if (last_move==HomotopyMovement::OtI) {tok_lmv = "OtI";}

			if (verbose) {cout << "----------------------------------" << endl;}
			if (verbose) {cout << "IT = " << it << "\t lambda = " << lambda << "\t |x| = " << norm(x, 1) <<"\t z = " << pow(norm(y- A* x ,2), 2) << "\t node " << no_node << endl;}
			if (last_move != HomotopyMovement::Init) {
					if (verbose) {cout << "\t LMV = " << tok_lmv << "\t tabu_index = " << uQv(tabu_index) << endl;}
			}
			if (verbose) {cout << "\tI("<<I.size()<<") : \t ";for (auto elt : I) {	cout << uQv(elt) << " ";	} cout << endl;}
			//cout << "\tJ("<<J.size()<<") : \t ";for (auto elt : J) {	cout << uQv(elt) << " ";	} cout << endl;
			//
			v1 = HH.submat(J, I) * d1.rows(I);
			//v2 = zeros(J.n_elem);
			//e = (lambda - c.rows(J)) / (1 + v2 - v1);
			//////////////////////////////////////////////////////////////////////////
			// OUT -> IN
			e = (lambda - c.rows(J)) / (1 - v1);
			if (last_move==HomotopyMovement::ItO) {
						uvec i_tabu = find(J==tabu_index);
						e(i_tabu.at(0)) = 0;
			}

			i1 = arg_min_st_p(J, e, &gamma_xint_plus_P, &i1_);
			//i1 = arg_min_p(J, e, &gamma_xint_plus_P, &i1_, eps_o_i);

			//////////////////////////////////////////////////////////////////////////
			// OUT -> IN
			sub_I = I(find(d1(I)<0));
			//sub_I = I;

			// e = -x.rows(I) / d1.rows(I);
			// i3 = arg_min_p(I, e, &gamma_x0_plus, &i3_, eps);

			e = -x.rows(sub_I) / d1.rows(sub_I);

			if (verbose) {
					cout << "IN->OUT \t I = " << I.size() << "/ sub(I) = "<<sub_I.size()<<endl;
					for (int k(0);k<sub_I.size();++k) {
							cout << uQv(sub_I(k))<<"\t X="<<x(sub_I(k))<<"\t| D ="<< d1(sub_I(k))<<"\t| G ="<<-x(sub_I(k))/d1(sub_I(k))<<endl;
					}
			}

			if (last_move==HomotopyMovement::OtI) {
					uvec i_tabu = find(sub_I==tabu_index);
					if (!i_tabu.is_empty()) {
							e(i_tabu.at(0)) = 0;
					}
			}


			i3 = arg_min_st_p(sub_I, e, &gamma_x0_plus, &i3_);	// (it==62 && verbose)
			//i3 = arg_min_p(sub_I, e, &gamma_x0_plus, &i3_, eps_i_o);	// (it==62 && verbose)

			//
			if (i3 != -1) {
					uvec i3_vec = find(I==i3);
					i3_ = i3_vec.at(0);
			}
			//
			//gamma = min(gamma_xint_plus_P, gamma_x0_plus);
			// une alternative pour prévenir des erreurs dans les tests d'égalités
			if (verbose) {
					cout << "GAMMA :" << endl;
					if (i1 != -1) {cout << "\tgamma_1("<<uQv(i1)<<") = "<<gamma_xint_plus_P<< "\t| d("<<uQv(i1)<<")= "<<d1(i1)<<endl;}
					if (i3 != -1) {cout << "\tgamma_3("<<uQv(i3)<<") = "<<gamma_x0_plus<< "\t| d("<<uQv(i3)<<")= "<<d1(i3)<<endl;}
			}

			bool choix_ajout(false);
			if (gamma_xint_plus_P < gamma_x0_plus) {
					gamma = gamma_xint_plus_P;
					choix_ajout = true;
					if (verbose) {cout <<"\t => "<< uQv(i1) << "  out -> in " << endl;}
			} else if(gamma_xint_plus_P > gamma_x0_plus) {
					gamma = gamma_x0_plus;
					if (verbose) {cout <<"\t => "<< uQv(i3) << "  in -> out " << endl;}
			} else {
					if (verbose) {cout << "EGALITE GAMMA = " << gamma_x0_plus << endl;}
					gamma = exp10(10);
			}
			//  any condition is in force
			if (gamma == exp10(10)) {
					delete chrono;
					return x;
			}
			// if (i1 != -1) {cout << "\tAV(i1="<<uQv(i1)<<") \t  x = "<<x[i1]<<endl;}
			// if (i3 != -1) {cout << "\tAV(i3="<<uQv(i3)<<") \t  x = "<<x[i3]<<endl;}
			if (lambda-gamma <0 && patch_neg_lambda) {
					// cout << "gamma = "<< gamma<< endl;
					// cout << "abs(lambda-gamma) = " << abs(lambda-gamma) << endl;
					// cout << "gamma' = " << (gamma - abs(lambda-gamma)) << endl;
					gamma = gamma - abs(lambda-gamma);
					if (verbose) {pause_cin_txt("lmbd- gamma neg");}
			}
			//  new solution
			x.elem(I) = x.rows(I) + gamma * d1.rows(I);

			double Tau = pb_param;
			double nx = norm(x.rows(I), 1);
			if (nx >= Tau) {
				if (verbose) {cout << "|x| = " << nx << endl;}
				if (nx > Tau) {
						vec sn = sign(x.rows(I) - gamma * d1.rows(I) / 2);
						vec gm = (nx - Tau) / (d1.rows(I).t() * sn);
						x.rows(I) -= d1.rows(I) * gm;
				}
				if (verbose) {cout << "|x|_rs = " << norm(x,1) << endl;}
				delete chrono;
				return x;

			}

			lambda = lambda - gamma;
			res = y- *H * x;
			//

			if (abs(lambda) <= eps_lmbd_neg && patch_neg_lambda) {
					// cout << "LAMBDA NULL -> "<<lambda<< endl;
					delete chrono;
					return x;
			}

			//////////////////////////////////////////////////////////////////////////
			tabu_index = def_index;
			chrono->start();
			//  mettre à jour selon le cas (gamma)
			if (!choix_ajout) {
				// RETRAIT D'UNE VARIABLE
				last_move = HomotopyMovement::ItO;
				tabu_index = i3;
				Etat_var_x.at(i3) = 0;
				deplacer(i3, &I, &J);
				x.at(i3) = 0;
				c = Hr - HH.cols(I) * x.rows(I);
				//  % update direction Iint
				d1 = zeros(Q1);
				/*
				if (smart_inv) {
						inv_S = inversion_rec_del_P(inv_S, i3_);
				} else {

						S = HH.submat(I,I);
						inv_S = inv(S);

				}
				d1.elem(I) = inv_S * sign(c.rows(I));
				*/
				d1.elem(I) = solve(HH.submat(I,I),sign(c.rows(I)));

			} else { //if (gamma == gamma_xint_plus_P ) // { //|| gamma == gamma_xint_plus_N) {
				last_move = HomotopyMovement::OtI;
				tabu_index = i1;
				//
				Etat_var_x.at(i1) = 1;
				Iint1p = I;
				deplacer(i1, &J, &I);
				c = Hr - HH.cols(I) * x.rows(I);
				//  % update direction Iint
				d1 = zeros(Q1);

				// ATTENTION F =  eyeN1 - voir plus haut;
				// H->cols(Iint1p).t() * F * H->col(i1) - ici F = Id(N) eequiv a un slice
				// HH.submat(Iint1p,i1)
				// idem pour H->col(i1).t() * F * H->col(i1)
				// HH.submat(i1,i1)

				//inv_S = inversion_rec_add_P(inv_S,	H->cols(Iint1p).t() * F * H->col(i1),	H->col(i1).t() * F * H->col(i1));
				//inv_S = inversion_rec_add_P(inv_S,	HH.submat(Iint1p,i1),	HH.submat(i1,i1));

				/*
				if (smart_inv) {
						inv_S = inversion_rec_add_P(inv_S,H->cols(Iint1p).t() * eyeN1 * H->col(i1),	H->col(i1).t() * eyeN1 * H->col(i1));
				} else {
						S = HH.submat(I,I);
						inv_S = inv(S);
				}
				d1.elem(I) = inv_S * sign(c.rows(I));
				*/
				d1.elem(I) = solve(HH.submat(I,I),sign(c.rows(I)));

			}

			//d1.elem(I).t().print("d1.elem(I)");

			/*
			if (verbose) {
					cout << "COMPO_AP_"<< it << "\tSUM = " << sum(x) << "\tabs(SUM) = " << sum(abs(x)) << endl;
					for (int i(0);i<x.size();++i) {
							//if (x[i] != 0) {
									//cout << "x["<<uQv(i)<<"]= "<<x[i]<<" \t d["<<uQv(i)<<"]="<<d1[i]<<endl;
									if (x[i] > 0) {cout << "\t+ x["<<uQv(i)<<"] = " << x[i] << "\t| d("<<uQv(i)<<")= "<< d1(i) << "" << endl;}
									if (x[i] < 0) {cout << "\t- x["<<uQv(i)<<"] = " << x[i] << "\t| d("<<uQv(i)<<")= "<< d1(i) << "\t!!!" << endl;}
							//}
					}
			}
			*/
			if (verbose) {
					for (int k(0);k<I.size();++k) {
							cout << uQv(I(k))<<"\t X="<<x(I(k))<<"\t| D ="<< d1(I(k))<<endl;
					}
			}


			/*

			/////
			// Protocole de nettoyage
			// Détecter les valeur de pente négatives
			uvec neg_pente(find(d1 < 0));
			vec e_neg(-x.rows(neg_pente) / d1.rows(neg_pente));
			//neg_pente.t().print("neg_pente");
			//e_neg.t().print("e_neg");

			uvec gamma3_neg = find(e_neg < eps_i_o);



			if (!gamma3_neg.is_empty()){
					if (verbose) {cout << "|gamma3_neg| = " << gamma3_neg.size() << endl;}
					for (size_t g(0); g < gamma3_neg.size(); ++g) {
							//cout << "=> " << gamma3_neg.at(0) << " / " << uQv(neg_pente(gamma3_neg.at(0))) << endl;
							if (verbose) {cout << "SHIFT => d1(" << gamma3_neg.at(g) << ") => " << uQv(neg_pente(gamma3_neg.at(g)))<<  endl;}
							// uvec i3_neg = find(I==uQv(neg_pente(gamma3_neg.at(0))));
							uvec i3_neg = find(I==uQv(neg_pente(gamma3_neg.at(g))));
							if (!i3_neg.is_empty()) {
									if (verbose) {cout << "\tI("<<I.size()<<") : \t ";for (auto elt : I) {	cout << uQv(elt) << " ";	} cout << endl;}
									if (verbose) {cout << "\tindex in inv(S)["<<inv_S.n_rows <<"x"<<inv_S.n_cols<<"] => " << i3_neg.at(0) << endl;}
									//Etat_var_x.at(uQv(neg_pente(gamma3_neg.at(0)))) = 0;
									Etat_var_x.at(uQv(neg_pente(gamma3_neg.at(g)))) = 0;
									//x.at(uQv(neg_pente(gamma3_neg.at(0)))) = 0;
									x.at(uQv(neg_pente(gamma3_neg.at(g)))) = 0;
									// d1.at(uQv(neg_pente(gamma3_neg.at(0)))) = 0;
									d1.at(uQv(neg_pente(gamma3_neg.at(g)))) = 0;
									// inv_S = inversion_rec_del_P(inv_S, i3_neg.at(0));
									inv_S = inversion_rec_del_P(inv_S, i3_neg.at(0));
									// deplacer(uQv(neg_pente(gamma3_neg.at(0))), &I, &J);
									deplacer(uQv(neg_pente(gamma3_neg.at(g))), &I, &J);

									//
									c = Hr - HH.cols(I) * x.rows(I);
									d1 = zeros(Q1);
									d1.elem(I) = inv_S * sign(c.rows(I));

							}
							if (verbose) {
									pause_cin_txt("Next_shift");
							}
					}
			}

			*/









			this->ctx->T_test += chrono->check();
			if (verbose) {
					pause_cin_txt("Next it ");
			}


			//if(verbose && no_node == 17) {pause_cin_txt("Next it ");}
		}//for
		delete chrono;
		return x;
}






/*
vec GenericHomotopy_ASC_ANC::Cplex_model_l2l0_ASC_ANC(mat H ,vec y, uvec Qb, uvec Q1, unsigned long long int n) {
	mat HtH(this->ctx->problem->HtH);
	vec Hy(H.t()*y);
	vec x_relache = zeros(H.n_cols);
	ChronometerInterface * chrono = alloc_chrono();

	IloEnv env;
	try {
		//if (n%2 == 0) { // || n % 2 == 0
				//cout << "INT-node" << endl;
				uvec Qv = sort(join_cols(Qb,Q1));		// Qv = Qvalide
				//
				chrono->start();
				// //
				if (H.n_rows >= H.n_cols) {
						//cout << "\tUC Case "<< endl;
						//cout << "\tdim(HtH) = " << HtH.n_rows << " x " << HtH.n_cols << endl;
						mat sub_HtH(HtH(Qv,Qv));
						//cout << "\tdim(sub_HtH) = " << sub_HtH.n_rows << " x " << sub_HtH.n_cols << endl;
						//
						IloModel model(env);
						IloCplex cplex(model);
						//
						IloNumVarArray x(env,Qv.n_elem,0,1);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						//
						IloExpr objExpr(env);
						for (int i(0); i<Qv.n_elem;++i){
								objExpr += -2*Hy(Qv(i))*x[i];
								for (int j(0); j < Qv.n_elem; ++j) {
										objExpr += x[i]*sub_HtH(i,j)*x[j];
								}
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize, "OBJ");
						model.add(obj);
						//
						IloRange ctr_sum(env, 0, IloSum(x), 1, "ctr_sum");
						model.add(ctr_sum);
						//
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_bnd;
								name_bnd << "ctr_BND_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_bnd.str().c_str()));
						}
						//
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1); // nombre de threads
						this->ctx->T_model += chrono->check();
						//
						cplex.solve();
						//cout << "OBJ VAL" << cplex.getObjValue() << endl;
						IloNumArray vals(env);
						cplex.getValues(vals,x);
						// //
						for (int i = 0; i < Qv.n_elem; ++i){
								x_relache[Qv(i)]=vals[i];
						}
						//x_relache.t().print("x_relache");
						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_OC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
				} else {

						//mat sub_H = H(span(0,H.n_rows - 1),Qv);				//  span(start,end) can be replaced by span::all to indicate the entire range
						mat sub_H = H.cols(Qv);
						//cout << "\tUC Case "<< endl;
						//cout << "size(Qv) = " << Qv.n_elem << endl;
						//cout << "size(H) = " << H.n_rows << " x " << H.n_cols << " \t size(sub_H) = " << sub_H.n_rows << " x " << sub_H.n_cols << endl;
						//
						IloModel model(env);
						IloCplex cplex(model);
						//
						IloNumVarArray x(env,Qv.n_elem,0,1);
						IloNumVarArray z(env,sub_H.n_rows,-IloInfinity,+IloInfinity);
						for (int i; i <Qv.n_elem; ++i ) {	std::stringstream name;	name << "x" << Qv(i);	x[i] = IloNumVar(env, name.str().c_str());	}
						for (int i; i <H.n_rows; ++i ) {	std::stringstream name;	name << "z" << i + 1;	z[i] = IloNumVar(env, name.str().c_str());	}
						model.add(x);
						model.add(z);
						//
						IloExpr objExpr(env);
						for (int i(0);i<sub_H.n_rows;++i){
								objExpr+= z[i]*z[i];
								objExpr+= -2*z[i]*y(i);		// Note : z variable, y valeur (vecteur)
						}
						IloObjective obj(env, objExpr, IloObjective::Minimize,"OBJ");
						model.add(obj);
						//
						IloRange ctr_ASC(env,0,IloSum(x),1,"ctr_ASC");
						model.add(ctr_ASC);
						//
						for (int i(0); i < Qv.n_elem; ++i){
								std::stringstream name_anc;
								name_anc << "ctr_ANC_" << Qv(i);
								model.add(IloRange(env, 0,x[i],1,name_anc.str().c_str()));
						}
						//
						for(int i(0);i<sub_H.n_rows;++i) {
								std::stringstream name;
								name << "ctr_eq_lin_" << i;
								IloExpr Hx_i(env);
								for (int j(0); j<Qv.n_elem; ++j){
										Hx_i+= H(i,Qv(j))*x[j];
								}
								model.add(IloRange(env,0,Hx_i-z[i],0,name.str().c_str()));
						}
						cplex.setOut(env.getNullStream());
						cplex.setParam(IloCplex::Threads, 1); // nombre de threads
						this->ctx->T_model += chrono->check();
						//
						cplex.solve();
						//cout << "OBJ VAL" << cplex.getObjValue() << endl;
						IloNumArray vals(env);
						cplex.getValues(vals,x);

						//cout << "size(vals) " << vals.getSize() << endl;
						double sum_x;
						for (int i(0); i < Qv.n_elem; ++i) {
								//cout << "x("<<Qv(i)<<") = " << vals[i] << endl;
								sum_x += vals[i];
								x_relache[Qv(i)] = vals[i];
						}
						//cout << "sum_x = " << sum_x << endl;


						// std::stringstream file_name;
						// file_name << "l2l0_ASC_ANC_lb_node_" << n <<"_UC.lp";
						// cplex.exportModel(file_name.str().c_str());
						env.end();
						//pause_cin_txt("LB solver - next ?");
				}
	} catch (IloException & e) {
		cerr << " ERREUR : exception = " << e << endl;
	}

	delete chrono;
	return x_relache;
}
*/
