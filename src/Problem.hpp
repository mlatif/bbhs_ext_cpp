#ifndef BBPROBLEM
#define BBPROBLEM 1

#include "interfaces.hpp"

class ProblemData {
public:
	mat H; /**< The dictionary matrix. */
	mat HtH; /**< Precomputation to ease the inversion on a partial support. */
	colvec H_columnwise_norm_as_column; /**< Precomputation to ease l1 screening. */
	sp_mat eyeN; /**< Identity matrix of correct size. */
	double bigM; /**< BigM constraint. */
	vec y; /**< Data measurements. */
	vec sqrt_yty; /**< Precomputation to ease l1 screening. */
	ParamType param; /**< lambda/epsilon/k problem parameter. */
	ProblemType type; /**< Problem type (P2+0 or P2/0 or P0/2 or P2/0+ASC+ANC). */
	ASCFormulation asc_form;	/** ASC formulation (default value : equality) */
	/**
	 * Construct a problem instance, with some precomputations done.
	 *
	 * @param Hmat The dictionary matrix.
	 * @param yvec The data measurements.
	 * @param BigM The BigM constraint.
	 * @param param The lambda/epsilon/k problem parameter.
	 * @param pt The problem type (P2+0 or P2/0 or P0/2).
	 */
	ProblemData(mat Hmat, vec yvec, double BigM, ParamType param, ProblemType pt);
	/**
	 * Construct a P2/0+ASC+ANC problem instance, with some precomputations done.
	 *
	 * @param Hmat The dictionary matrix.
	 * @param yvec The data measurements.
	 * @param param ie k the sparsity coefficient.
	 * @param pt The problem type ie L2L0_ASC_ANC.
	 * @param asc_f The ASC formulation (eq/ineq)
	 */
	ProblemData(mat Hmat, vec yvec, ParamType param, ProblemType pt);
	void set_ASC_formulation(ASCFormulation asc_f);
	ASCFormulation get_ASC_formulation();



	virtual ~ProblemData();
};

#endif
