#ifndef BBLB
#define BBLB 1

#include "Context.hpp"
#include "interfaces.hpp"
#include "MimosaConfig.hpp"

// Lower bounds calculation
// Each of the following class is context-aware

/** \file LB.cpp - Compute Lower Bounds
 * This is the place for the various lower bounds using convex relaxations:
 * - Cplex based
 * - Homotopy based
 * - Active set based
 * The focus is on efficiency here: we have a lot of lower bounds computations
 * happening, se we must have something fast.
 *
 * Some remarks:
 * - xub stands for the x leading to the upper bound of the analyzed node,
 *   that is the solution of the problem restricted to the support S1.
 * - The function call operator handles the node feasibility and solution flags,
 *   and is just a convenient wrapper around the protected method(s) which is (are)
 *   carrying the actual computations.
 */
/**
 * Generic code for handling the context attribute.
 */
class ContextAwareLB : public LBInterface {
protected:
	ContextData* ctx; /**< Context reference to, most notably, update time stats. */
public:
	void setContext(ContextData* context) { this->ctx = context; }
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	ContextAwareLB(ContextData* context) : ctx(context) {}
	ContextAwareLB() : ctx(nullptr) {}
};

#ifdef USE_CPLEX
/**
 * Cplex relaxation for P0/2 problem.
 */
class CplexRelaxL0L2 : public ContextAwareLB {
protected:
	/**
	 * Actual cplex relaxation is done here.
	 *
	 * @param A The dictionary matrix.
	 * @param y The data measurements.
	 * @param epsilon The data-fitting constraint.
	 * @param bigM The BigM constraint.
	 * @param Q1 The vector version of S1.
	 * @param Q0 The vector version of S0.
	 */
	virtual vec optim_withCplex_model_bigM_l0l2(mat A, vec y, double epsilon, double bigM, uvec Q1, uvec Q0);
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	CplexRelaxL0L2(ContextData* context): ContextAwareLB(context) {}
	CplexRelaxL0L2(): ContextAwareLB() {}
};

/**
 * Cplex relaxation for P2/0 problem.
 */
class CplexRelaxL2L0 : public ContextAwareLB {
protected:
	/**
	 * Actual cplex relaxation is done here.
	 *
	 * @param A The dictionary matrix.
	 * @param y The data measurements.
	 * @param k The sparsity constraint.
	 * @param bigM The BigM constraint.
	 * @param Q1 The vector version of S1.
	 * @param Q0 The vector version of S0.
	 */
	virtual vec optim_withCplex_model_bigM_l2l0(mat A ,vec y, int k, double bigM, uvec Q1, uvec Q0);
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	CplexRelaxL2L0(ContextData* context): ContextAwareLB(context) {}
	CplexRelaxL2L0(): ContextAwareLB() {}
};

/**
 * Cplex relaxation for P2+0 problem.
 */
class CplexRelaxL2pL0 : public ContextAwareLB {
protected:
	/**
	 * Actual cplex relaxation is done here.
	 *
	 * @param A The dictionary matrix.
	 * @param y The data measurements.
	 * @param lambda The data-fitting vs sparsity trade-off.
	 * @param bigM The BigM constraint.
	 * @param Q1 The vector version of S1.
	 * @param Q0 The vector version of S0.
	 */
	virtual vec optim_withCplex_model_bigM_l2pl0_withoutNode(mat A ,vec y, double lambda, double bigM, uvec Q1, uvec Q0);
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	CplexRelaxL2pL0(ContextData* context): ContextAwareLB(context) {}
	CplexRelaxL2pL0(): ContextAwareLB() {}
};
#endif // USE_CPLEX

/**
 * A stopping criteria indicate whether the homotopy relaxation should stop, and in this case what are the returned values.
 */
struct HomotopyStoppingCriteriaReturn {
	bool stop; /**< True if the relaxation should return immediately without further iterations. */
	vec x_Q1; /**< If stop is set to True, the value of the relaxed antecedent on the S1 support. Otherwise garbage. */
	vec x_Qv; /**< If stop is set to True, the value of the relaxed antecedent on the SBar support. Otherwise garbage. */
};

/**
 * A stopping criteria indicate whether the homotopy relaxation should stop, and in this case what are the returned values.
 */
class HomotopyStoppingCriteriaInterface {
public:
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2) = 0;
};

/**
 * P0/2 stopping criteria: the error constraint is satisfied, or the l1 norm of the relaxed antecedent is already larger than the current upper bound.
 */
class HomotopyL0L2StoppingCriteria : public HomotopyStoppingCriteriaInterface {
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2);
};

/**
 * P2/0 stopping criteria: the l1 norm of the antecedent is already larger than the constraint (if not we try to improve the cost function, so don't return).
 */
class HomotopyL2L0StoppingCriteria : public HomotopyStoppingCriteriaInterface {
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2);

};

/**
 * P2+0 stopping criteria: we've got the correct lambda.
 */
class HomotopyL2pL0StoppingCriteria : public HomotopyStoppingCriteriaInterface {
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2);
};

/**
 * P0/2 early stopping criteria: putting all components of SBar to 0 gives a feasible solution
 * (aka: the S1 support is sufficient to satisfy the error constraint).
 */
class HomotopyL0L2EarlyStopping : public HomotopyStoppingCriteriaInterface {
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2);
};

/**
 * No-op early stopping criteria for P2/0 and P2+0.
 */
class HomotopyDummyEarlyStopping : public HomotopyStoppingCriteriaInterface {
	virtual HomotopyStoppingCriteriaReturn operator()(ContextData* ctx, vec &x, vec &w, vec &d1, vec &d2, double pb_param, uvec &Iint1, double gamma, double lambda, mat &H, mat &B, vec &res, int Q2);
};


/**
 * Generic homotopy code
 */
class GenericHomotopy : public ContextAwareLB {
protected:
	HomotopyStoppingCriteriaInterface &stopping_criteria; /**< The stopping criteria used in the homotopy loop. */
	HomotopyStoppingCriteriaInterface &early_stopping; /**< The criteria used to stop before starting the homotopy iterations. */
	/**
	 * Actual homotopy relaxation is done here.
	 *
	 * @param B The dictionary matrix whose columns are reduced to those of S1.
	 * @param w The upper bound antecedent.
	 * @param H The dictionary matrix whose columns are reduced to those of SBar.
	 * @param uQ1 The vector version of S1.
	 * @param uQv The vector version of SBar.
	 * @param y The data measurements.
	 * @param M The BigM constraint.
	 * @param pb_param The problem parameter.
	 * @param score Filled to be the node score for future branching (LPS score).
	 */
	virtual vec mixed_BigM_homotopytemps(mat B, vec *w, mat *H, uvec uQ1, uvec uQv, vec y,
			double M, double pb_param,vec *score);

public:
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	GenericHomotopy(ContextData* context, HomotopyStoppingCriteriaInterface& stp_crit, HomotopyStoppingCriteriaInterface& early_stp): ContextAwareLB(context), stopping_criteria(stp_crit), early_stopping(early_stp) {}
	GenericHomotopy(HomotopyStoppingCriteriaInterface& stp_crit, HomotopyStoppingCriteriaInterface& early_stp): ContextAwareLB(), stopping_criteria(stp_crit), early_stopping(early_stp) {}
};

/**
 * Homotopy relaxation for P0/2 problem, using unified homotopy code.
 */
class GenericHomotopyL0L2 : public GenericHomotopy {
	HomotopyL0L2StoppingCriteria stopping_criteria;
	HomotopyL0L2EarlyStopping early_stopping;
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	GenericHomotopyL0L2(ContextData* context) : GenericHomotopy(context, this->stopping_criteria, this->early_stopping) {}
};

/**
 * Homotopy relaxation for P0/2 problem, using unified homotopy code.
 */
class GenericHomotopyL2L0 : public GenericHomotopy {
	HomotopyL2L0StoppingCriteria stopping_criteria;
	HomotopyDummyEarlyStopping early_stopping;
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	GenericHomotopyL2L0(ContextData* context) : GenericHomotopy(context, this->stopping_criteria, this->early_stopping) {}
};

/**
 * Homotopy relaxation for P0+2 problem, using unified homotopy code.
 */
class GenericHomotopyL2pL0 : public GenericHomotopy {
	HomotopyL2pL0StoppingCriteria stopping_criteria;
	HomotopyDummyEarlyStopping early_stopping;
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	GenericHomotopyL2pL0(ContextData* context) : GenericHomotopy(context, this->stopping_criteria, this->early_stopping) {}
};


/**
 * Active Set relaxation for P2+0 problem.
 */
class ActiveSetL2pL0 : public ContextAwareLB {
protected:
	virtual vec calculr(vec y,mat B,mat H,vec w,vec x,uvec ind_w_M,uvec ind_x_M,double M,int N);

	virtual double fobj_featuresign(vec x,vec w,mat B,mat H,vec y,double lambda);

	/**
	 * Actual active set relaxation is done here.
	 *
	 * @param B The dictionary matrix whose columns are reduced to those of S1.
	 * @param w The upper bound antecedent.
	 * @param H The dictionary matrix whose columns are reduced to those of SBar.
	 * @param x The initial starting point (parent node xLB in the case of warm start).
	 * @param uQ1 The vector version of S1.
	 * @param uQv The vector version of SBar.
	 * @param y The data measurements.
	 * @param M The BigM constraint.
	 * @param lambda The trade-off between sparsity and data-fitting.
	 */
	virtual vec mixed_BigM_featuresign_l2pl0_withoutNode(mat B, vec *w, mat *H,vec x, uvec uQ1, uvec uQv, vec y, double M, double lambda);
public:
	virtual bound_type operator()(NodeInterface& node, vec xub);
	/**
	 * This constructor is preferred to avoid stupid nullptr Context errors.
	 */
	ActiveSetL2pL0(ContextData* context): ContextAwareLB(context) {}
	ActiveSetL2pL0(): ContextAwareLB() {}
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
/**
 	* @brief Compute the lower bounds for l2l0_ASC_ANC problem with Cplex solver;
 	* @param A The dictionary matrix.
 	* @param y The data measurements.
 	* @param k The sparsity coefficient.
	* @param Qb The vector version of Sbar
 	* @param Q1 The vector version of S1.
 	* @param n The index of the node
 */
#ifdef USE_CPLEX
class CplexRelaxL2L0_ASC_ANC : public ContextAwareLB {
		protected:
				virtual vec optim_withCplex_model_l2l0_ASC_ANC(mat A ,vec y, uvec Qb, uvec Q1, unsigned long long int n);
				/* COMPARAISON DES RESULTATS (main = Cplex VS other = qpOASES)
						Ajouter la ligne dans bound_type CplexRelaxL2L0_ASC_ANC::operator()(NodeInterface& node, vec xub)
						vec x_other = this->optim_withOSQP_model_l2l0_ASC_ANC(pb.H, pb.y, sort(Qb), sort(Q1), node.getIdx());
						et décommenter la fonction
						vec CplexRelaxL2L0_ASC_ANC::optim_withOSQP_model_l2l0_ASC_ANC(...)
				*/
				// virtual vec optim_withOSQP_model_l2l0_ASC_ANC(mat A ,vec y, uvec Qb, uvec Q1, unsigned long long int n);
		public:
				virtual bound_type operator()(NodeInterface& node, vec xub);
				CplexRelaxL2L0_ASC_ANC(ContextData* context): ContextAwareLB(context) {}
				CplexRelaxL2L0_ASC_ANC(): ContextAwareLB() {}
};
#endif // USE_CPLEX
/**
	* @brief Compute the lower bounds for l2l0_ASC_ANC problem with FCLS algorithm;
	* @param H The dictionary matrix.
	* @param Qv The vector version of the valid indices in the current problem (ie the union of S1 and Sbar)
	* @param y The data measurements.
	* @param n The index of the node
*/
class FCLS_L2L0_ASC_ANC : public ContextAwareLB {
		protected:
				virtual vec FCLS_L2L0(mat H, uvec Qv, vec y,unsigned long long int n);
		public :
				virtual bound_type operator()(NodeInterface& node, vec xub);
				FCLS_L2L0_ASC_ANC(ContextData* context): ContextAwareLB(context) {}
				FCLS_L2L0_ASC_ANC(): ContextAwareLB() {}
};

/**
 	* @brief Compute the lower bounds for l2l0_ASC_ANC problem with qpOASES solver;
 	* @param A The dictionary matrix.
 	* @param y The data measurements.
 	* @param k The sparsity coefficient.
	* @param Qb The vector version of Sbar
 	* @param Q1 The vector version of S1.
 	* @param n The index of the node
 */

class OSQP_RelaxL2L0_ASC_ANC : public ContextAwareLB {
		protected:
				virtual vec optim_withOSQP_model_l2l0_ASC_ANC(mat A ,vec y, uvec Qb, uvec Q1, unsigned long long int n);
				/* COMPARAISON DES RESULTATS (main = qpOASES VS other = Cplex)
						Ajouter la ligne dans bound_type OSQP_RelaxL2L0_ASC_ANC::operator()(NodeInterface& node, vec xub)
						vec x_other =  this->optim_withCplex_model_l2l0_ASC_ANC(pb.H, pb.y, sort(Qb), sort(Q1),node.getIdx());
						et décommenter la fonction
						vec OSQP_RelaxL2L0_ASC_ANC::optim_withCplex_model_l2l0_ASC_ANC(...){
				*/
				// virtual vec optim_withCplex_model_l2l0_ASC_ANC(mat A ,vec y, uvec Qb, uvec Q1, unsigned long long int n);
		public:
				virtual bound_type operator()(NodeInterface& node, vec xub);
				OSQP_RelaxL2L0_ASC_ANC(ContextData* context): ContextAwareLB(context) {}
				OSQP_RelaxL2L0_ASC_ANC(): ContextAwareLB() {}
};



/////////////////////////////////////////////////////////////////////////////////////////
// HomotopyStoppingCriteriaReturn ; A stopping criteria indicate whether the homotopy relaxation should stop, and in this case what are the returned values.
//	bool stop, vec x_Q1, vec x_Qv;
// VOIR PLUS HAUT
/////////////////////////////////////////////////////////////////////////////////////////
// Intégrer les :
//		* HomotopyStoppingCriteriaInterface
//		* HomotopyL2L0StoppingCriteria : public HomotopyStoppingCriteriaInterface
//		* HomotopyDummyEarlyStopping : public HomotopyStoppingCriteriaInterface
//

class GenericHomotopy_ASC_ANC : public ContextAwareLB {
		protected:
			HomotopyStoppingCriteriaInterface &stopping_criteria;
			HomotopyStoppingCriteriaInterface &early_stopping;
			virtual vec homotopy_ASC_ANC(mat *H, uvec uQv, vec y, double pb_param,unsigned long long int no_node);
		public:

				GenericHomotopy_ASC_ANC(ContextData* context, HomotopyStoppingCriteriaInterface& stp_crit,HomotopyStoppingCriteriaInterface& early_stp): ContextAwareLB(context), stopping_criteria(stp_crit), early_stopping(early_stp) {} //, {}
				GenericHomotopy_ASC_ANC(HomotopyStoppingCriteriaInterface& stp_crit,HomotopyStoppingCriteriaInterface& early_stp): ContextAwareLB(),stopping_criteria(stp_crit),early_stopping(early_stp) {}//  {}
};


class GenericHomotopyL2L0_ASC_ANC : public GenericHomotopy_ASC_ANC {
	HomotopyL2L0StoppingCriteria stopping_criteria;
	HomotopyDummyEarlyStopping early_stopping;
	public:
			virtual bound_type operator()(NodeInterface& node, vec xub);
			GenericHomotopyL2L0_ASC_ANC(ContextData* context) : GenericHomotopy_ASC_ANC(context, this->stopping_criteria,this->early_stopping) {} // GenericHomotopy(context, this->stopping_criteria, this->early_stopping) {}
};


#endif
