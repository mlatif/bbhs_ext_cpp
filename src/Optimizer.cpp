#include <iostream>
#include "armadillo"

#include "Optimizer.hpp"
#include "NodeImplementation.hpp"
#include "node_container.hpp"
#include "Util.h"

#include "UB.hpp"
#include "LB.hpp"

#include "MimosaConfig.hpp"

//
#include<stack>

//
#include <fstream> // Stream class to both read and write from/to files.

using namespace std;
using namespace arma;


Optimizer::Optimizer(ContextData* ctx, SplitInterface* branch, ContractInterface* ctrt) :
		context(ctx), computeUB(nullptr), computeLB(nullptr),
		split(branch), contract(ctrt){}

vec Optimizer::optimize(ProblemData& problem, RelaxationType rt, NodeContainerInterface& node_set)
{
	if (split == nullptr || contract == nullptr)
		throw missing_bb_operators();

	this->context->problem = &problem;
	ContextData& ctx = *this->context;
	DlibQPSolver UBsolver;
	OptimizerNode* root_node = new OptimizerNode(*this);
	//MinimierLBCompare cmp;
	//LIFONodeContainer node_set(ctx);
	//MinimierNodeContainer node_set(ctx, cmp);
	//LIFOThenMinimierThresholdNodeContainer node_set(ctx, cmp, 2000);

	if (this->computeUB != nullptr) {
		delete this->computeUB;
		this->computeUB = nullptr;
	}
	if (this->computeLB != nullptr) {
		delete this->computeLB;
		this->computeUB = nullptr;
	}
	switch(problem.type) {
	case L2L0:
		this->computeUB = new UBL2L0(&ctx, UBsolver);
		switch(rt) {
		case Cplex:
#ifdef USE_CPLEX
			this->computeLB = new CplexRelaxL2L0(&ctx);
			break;
#else
			std::cerr << "ERROR: could not use Cplex relaxation: this binary has no Cplex support !" << std::endl;
			exit(1);
#endif
		case Homotopy:
			this->computeLB = new GenericHomotopyL2L0(&ctx);
			break;
		default:
			std::cerr << "ERROR: could not use Active Sets relaxation for L2L0 problem !" << std::endl;
			exit(1);
		}
		break;
	case L0L2:
		this->computeUB = new UBL0L2(&ctx, UBsolver);
		switch(rt) {
		case Cplex:
#ifdef USE_CPLEX
			this->computeLB = new CplexRelaxL0L2(&ctx);
			break;
#else
			std::cerr << "ERROR: could not use Cplex relaxation: this binary has no Cplex support !" << std::endl;
			exit(1);
#endif
		case Homotopy:
			this->computeLB = new GenericHomotopyL0L2(&ctx);
			break;
		default:
			std::cerr << "ERROR: could not use Active Sets relaxation for L0L2 problem !" << std::endl;
			exit(1);
		}
		break;
	case L2pL0:
		this->computeUB = new UBL2pL0(&ctx, UBsolver);
		switch(rt) {
		case Cplex:
#ifdef USE_CPLEX
			this->computeLB = new CplexRelaxL2pL0(&ctx);
			break;
#else
			std::cerr << "ERROR: could not use Cplex relaxation: this binary has no Cplex support !" << std::endl;
			exit(1);
#endif
		case Homotopy:
			this->computeLB = new GenericHomotopyL2pL0(&ctx);
			break;
		case ActiveSet:
			this->computeLB = new ActiveSetL2pL0(&ctx);
			break;
		}
		break;
	}


	// Initializing the support
	support sbar;
	sbar.clear();
	for(unsigned int i=0; i< problem.H.n_cols;i++)
		sbar.push_back(i);
	root_node->setSBar(sbar);
	// Computing the bounds
	std::cout << "=== Root UB ===" << std::endl;
	bound_type original_UB_pair = (*this->computeUB)(*root_node);
	vec original_xUB = original_UB_pair.first;
	double original_UB = original_UB_pair.second;
	std::cout << "=== Root LB ===" << std::endl;
	(*this->contract)(*root_node);
	(*this->computeLB)(*root_node, original_xUB);
	ctx.updateUB(original_UB, original_xUB);
	// Pushing to our stack
	node_set.push(root_node);
	// Main loop
	clock_t t0;
	t0=clock();
	double current_time=0;
	ctx.it_num = 0;
	ctx.BBNodeNum = 1;
	ctx.BestNodeNum = 1;
	vec x_opt = zeros<vec> (problem.H.n_cols);
	vec x_relache;
	while (!node_set.empty()) {
		++ctx.it_num;

		current_time = (float)(clock()-t0)/CLOCKS_PER_SEC ;
		if (current_time> ctx.TimeBBMax){
			++ctx.BBNodeNum;
			return x_opt;
		}
		if(ctx.it_num %1000==0){
			cout<<"N iteration : " << ctx.it_num << " times: "<< current_time <<endl;
		}

		NodeInterface* node = node_set.pop();
		node_list nl = node->split();
		bool UBupdated = false;
		for (node_list::reverse_iterator child = nl.rbegin(); child != nl.rend(); child++) {
			//NodeInterface* contracted_child = (*child)->contract();
			(*this->contract)(**child);
			bound_type upper_bound = (*this->computeUB)(**child);
			vec xub = upper_bound.first;
			double ub = upper_bound.second;
			++ctx.BBNodeNum;
			if (!(*child)->getSBar().empty()) {
				// Safe-guard against a screening which fixed
				// every remaining variable to 0.
				bound_type lower_bound = (*this->computeLB)(**child, xub);
				vec xlb = lower_bound.first;
				double lb = lower_bound.second;
				if ((*child)->isSolution() && ctx.updateUB(lb, xlb)) {
					UBupdated = true;
					x_opt = xlb;
					cout<<" is solution_ bornsupp: "<< lb << endl;
					ctx.BestNodeNum = ctx.BBNodeNum;
					ctx.nbr_update_Bsupp++;
					delete *child;
				} else if (!(*child)->isFeasible() || !node_set.push(*child)) {
					// The second condition means "The child LB is greater than the current UB"
					// This, with the infeasible child case, means we do not need it anymore
					delete *child;
				}
			}
			if (ctx.updateUB(ub, xub)) {
				UBupdated = true;
				x_opt = xub;
				cout<<" is solution_ bornsupp: "<< ub <<endl;
				ctx.nbr_update_Bsupp++;
				ctx.BestNodeNum = ctx.BBNodeNum;
			}
		}
		// The parent node has been split out, we do not need it afterwards.
		delete node;
		if (UBupdated)
			node_set.prune(ctx.UB);
	}
	return x_opt;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
vec Optimizer::optimize_ASC_ANC(ProblemData& problem, RelaxationType rt, NodeContainerInterface& node_set) {
			bool verbose(false);
			//
			if (split == nullptr || contract == nullptr) {
				throw missing_bb_operators();
			}
			//
			this->context->problem = &problem;
			ContextData& ctx = *this->context;
			OptimizerNode* root_node = new OptimizerNode(*this);

#ifdef USE_CPLEX
			CplexQPSolver_ASC_ANC UB_CplexQP_solver(&ctx);
#endif
			FCLSSolver_ASC_ANC UB_FCLS_solver(&ctx);
			OSQPSolver_ASC_ANC UB_OSQP_solver(&ctx);

			////////////////////////////////////////////////////////////////////////////
			if (this->computeUB != nullptr) {
				delete this->computeUB;
				this->computeUB = nullptr;
			}
			if (this->computeLB != nullptr) {
				delete this->computeLB;
				this->computeUB = nullptr;
			}
			////////////////////////////////////////////////////////////////////////////
			switch (rt) {
				case Cplex:
#ifdef USE_CPLEX
						this->computeUB = new UBL2L0_ASC_ANC_QPCplex(&ctx,UB_CplexQP_solver);
						this->computeLB = new CplexRelaxL2L0_ASC_ANC(&ctx);
						break;
#else
						std::cerr << "ERROR: could not use Cplex relaxation: this binary has no Cplex support !" << std::endl;
						exit(1);
#endif
				// case Homotopy:
				// 		//this->computeUB = new UBL2L0_ASC_ANC_QPCplex(&ctx,UB_CplexQP_solver);
				// 		this->computeUB = new UBL2L0_ASC_ANC_OSQP(&ctx,UB_OSQP_solver);
				// 		//this->computeUB = new UBL2L0_ASC_ANC_FCLS(&ctx,UB_FCLS_solver);
				// 		this->computeLB = new GenericHomotopyL2L0_ASC_ANC(&ctx);
				// 		break;
				case FCLS :
						this->computeUB = new UBL2L0_ASC_ANC_FCLS(&ctx,UB_FCLS_solver);
						this->computeLB = new FCLS_L2L0_ASC_ANC(&ctx);
						break;
				case OSQP :
						// OPERATEUR UNIQUE O.pen S.ource Q.uadratic P.rogramming
						this->computeUB = new UBL2L0_ASC_ANC_OSQP(&ctx,UB_OSQP_solver);
						// OPERATEUR HYBRIDE (Pour comparer les résultats Cplex qpOASES)
						//this->computeUB = new UBL2L0_ASC_ANC_OSQP_QPCplex(&ctx,UB_OSQP_solver,UB_CplexQP_solver);
						this->computeLB = new OSQP_RelaxL2L0_ASC_ANC(&ctx);
						break;
				case HYB_HOM_OSQP:
						this->computeUB = new UBL2L0_ASC_ANC_OSQP(&ctx,UB_OSQP_solver);
						this->computeLB = new GenericHomotopyL2L0_ASC_ANC(&ctx);
						break;
				case HYB_HOM_FCLS:
						this->computeUB = new UBL2L0_ASC_ANC_FCLS(&ctx,UB_FCLS_solver);
						this->computeLB = new GenericHomotopyL2L0_ASC_ANC(&ctx);
						break;
				default:
					cout << "The Show Must Go On ;-)" << endl;
			}
			////////////////////////////////////////////////////////////////////////////
			unsigned int K = ctx.problem->param.ui;
			vec x_opt = zeros<vec> (problem.H.n_cols);


			//	Initializing the support of root node;
			support sbar;
			sbar.clear();
			for(unsigned int i=0; i< problem.H.n_cols;i++) {
					sbar.push_back(i);
			}
			root_node->setSBar(sbar);
			//
			root_node->setSv(sbar);
			////////////////////////////////////////////////////////////////////////////
			clock_t t0;		t0=clock();
			double current_time=0;
			ctx.it_num = 0;		ctx.BBNodeNum = 1;	ctx.BestNodeNum = 1;
			////////////////////////////////////////////////////////////////////////////
			bound_type original_UB_pair = (*this->computeUB)(*root_node);
			vec original_xUB = original_UB_pair.first;
			double original_UB = original_UB_pair.second;
			(*this->computeLB)(*root_node, original_xUB);
			ctx.updateUB(original_UB, original_xUB);
			////////////////////////////////////////////////////////////////////////////
			// Cas particulier => le solveur retourne une solution au max K-sparse sur le noeud racine ie
			// sur l'ensemble de l'espace de recherche => root_node is_solution()
			if (root_node->isSolution() && ctx.updateUB(root_node->getLB(),root_node->getxLB())) {
					// Pas besoin de mettre de booleen UBupdated => pas de noeuds à pruner
					if (ctx.verbose) {cout << "> ROOT - UBupdated \t "<< ctx.UB << endl;}
					x_opt = root_node->getxLB();
					ctx.BestNodeNum = 0;//root_node->getIdx();
					ctx.nbr_update_Bsupp++;
					delete root_node;
			} else {	// else de if (root_node->isSolution()) {
					////////////////////////////////////////////////////////////////////////////
					node_set.push(root_node);
					vec xub = zeros(ctx.problem->H.n_cols);
					double ub = 0;
					////////////////////////////////////////////////////////////////////////////
					while(!node_set.empty()){
							if (ctx.force_stop){
									cout << "FORCE STOP = " << ctx.force_stop << endl;
									//pause_cin_txt("MAINFS");
									return zeros<vec> (problem.H.n_cols);
							}
							++ctx.it_num;
							current_time = (float)(clock()-t0)/CLOCKS_PER_SEC ;
							if (current_time> ctx.TimeBBMax){
									++ctx.BBNodeNum;
									return x_opt;
							}
							///////////////////////////////////////////////////////////////////////
							if(ctx.it_num %10==0 && !ctx.verbose){
									cout << "-----------------------------------------------------------------------"<< endl;
									cout << "#(it) = "<< ctx.it_num << "\t #(node) = "<< ctx.BBNodeNum << "\t T = "<< current_time <<"\t card(L) = " << node_set.size() <<endl;
									cout << "xUB* = "; 	for (size_t i(0); i < ctx.xUB.n_rows; ++i){ if (ctx.xUB[i]>0) { char tok = ctx.x_truth[i]>0 ?'\'':'\0';  cout << i<< tok << "\t ";} }  cout << endl;
									cout << "UB*  = " << ctx.UB << "\tsum(xUB*) = " << sum(ctx.xUB) << endl;
							} else if(ctx.verbose) {
									cout << "-----------------------------------------------------------------------"<< endl;
									cout << "#(it) = "<< ctx.it_num << "\t #(node) = "<< ctx.BBNodeNum << "\t T = "<< current_time<<"\t card(L) = " << node_set.size()  << endl;
									int cpt_sp(0);
									cout << "xUB* = "; 	for (size_t i(0); i < ctx.xUB.n_rows; ++i){ if (ctx.xUB[i]>0) { ++cpt_sp; char tok = ctx.x_truth[i]>0 ?'\'':'\0';  cout << i<< tok << "\t ";} }  cout << endl;
									cout << "kUB* = " << cpt_sp<<"\tUB*  = " << ctx.UB << endl;
							}
							///////////////////////////////////////////////////////////////////////
							NodeInterface* node = node_set.pop();
							node_list nl = node->split();
							bool UBupdated = false;
							///////////////////////////////////////////////////////////////////////
							for (node_list::reverse_iterator child = nl.rbegin(); child != nl.rend(); child++) {
									(*this->contract)(**child);
									++ctx.BBNodeNum;
									//TODO FActoriser tout ça
									//if ((*child)->getIdx() % 2 != 0 &&  (*child)->getS1().size() < ctx.problem->param.ui) {
									if ((*child)->getIdx() % 2 != 0 &&  (*child)->getS1().size() < ctx.problem->param.ui && !(*child)->getSv().empty()) {
											if (ctx.verbose) {
													cout << "\t@BR : " << (*child)->getIdx() << endl;
													cout << "\tS1("<<(*child)->getS1().size()<<") : ";	for (auto i : (*child)->getS1()) {	cout << i << " ";} cout << "\t||\tS0("<<(*child)->getS0().size()<<") : "; for (auto i : (*child)->getS0()) {	cout << i << " ";} cout<< endl;
													// cout << "\tSV("<<(*child)->getSv().size()<<") : ";	for (auto i : (*child)->getSv()) {	cout << i << " ";} cout << endl;
											}
											node_set.push(*child);
									}
									//
									if ((*child)->getIdx() % 2 != 0 && (*child)->getS1().size() == ctx.problem->param.ui) {
											if (ctx.verbose) {
													cout << "\t@EXT/LEAF/K : " << (*child)->getIdx() <<endl;
													cout << "\tS1("<<(*child)->getS1().size()<<") : ";	for (auto i : (*child)->getS1()) {	cout << i << " ";}  cout << "\t||\tS0("<<(*child)->getS0().size()<<") : ";	for (auto i : (*child)->getS0()) {	cout << i << " ";} cout<< endl;
											}
											bound_type upper_bound = (*this->computeUB)(**child);
											xub = upper_bound.first; //vec xub = upper_bound.first;
											ub = upper_bound.second; //double ub = upper_bound.second;
											if(ctx.verbose){cout << "\t\tub = " << ub << "\t ctx.UB = "<< ctx.UB << endl;}
											//Inutile de tester l'ensemble des branchements valides ici ?????
											//if (verbose){cout << "\t\tSV("<<(*child)->getSv().size()<<") : ";	for (auto i : (*child)->getSv()) {	cout << i << " ";} cout << endl;}
											if (ctx.updateUB(ub, xub)) {		// (*child)->isSolution() && ctx.updateUB(ub, xub) ??  isSolution() retourne faux, sûrement à cause du non recalcul du noeud
														if (ctx.verbose) {cout << "> EXT - UBupdated \t "<< ctx.UB << endl;}
														UBupdated = true;
														x_opt = xub;
														ctx.nbr_update_Bsupp++;
														ctx.BestNodeNum = (*child)->getIdx(); //ctx.BBNodeNum;
											}
									}
									// --------------------------------------------------------------//
									// OUI C'est redondant, on verra apr_s
									if ((*child)->getIdx() % 2 != 0 && (*child)->getS1().size() < ctx.problem->param.ui && (*child)->getSv().empty()) {
											if (ctx.verbose) {
													cout << "\t@EXT/LEAF/F : " << (*child)->getIdx() <<endl;
													cout << "\tS1("<<(*child)->getS1().size()<<") : ";	for (auto i : (*child)->getS1()) {	cout << i << " ";}  cout << "\t||\tS0("<<(*child)->getS0().size()<<") : ";	for (auto i : (*child)->getS0()) {	cout << i << " ";} cout<< endl;
											}
											bound_type upper_bound = (*this->computeUB)(**child);
											xub = upper_bound.first; //vec xub = upper_bound.first;
											ub = upper_bound.second; //double ub = upper_bound.second;
											if(ctx.verbose){cout << "\t\tub = " << ub << "\t ctx.UB = "<< ctx.UB << endl;}
											//Inutile de tester l'ensemble des branchements valides ici ?????
											//if (verbose){cout << "\t\tSV("<<(*child)->getSv().size()<<") : ";	for (auto i : (*child)->getSv()) {	cout << i << " ";} cout << endl;}
											if (ctx.updateUB(ub, xub)) {		// (*child)->isSolution() && ctx.updateUB(ub, xub) ??  isSolution() retourne faux, sûrement à cause du non recalcul du noeud
														if (ctx.verbose) {cout << ">EXT - UBupdated \t "<< ctx.UB << endl;}
														UBupdated = true;
														x_opt = xub;
														ctx.nbr_update_Bsupp++;
														ctx.BestNodeNum = (*child)->getIdx(); //ctx.BBNodeNum;
											}
									}
									// --------------------------------------------------------------/
									if ((*child)->getIdx() % 2 == 0 && !(*child)->getSBar().empty()) {		//if (!(*child)->getSBar().empty()) {
											if (ctx.verbose) {
													cout << "\t@INT/EXPL : " << (*child)->getIdx() << endl;
													cout << "\tS1("<<(*child)->getS1().size()<<") : ";	for (auto i : (*child)->getS1()) {	cout << i << " ";}  cout << "\t||\tS0("<<(*child)->getS0().size()<<") : "; for (auto i : (*child)->getS0()) {	cout << i << " ";} cout<< endl;
											}
											vec xlb = (*child)->getxLB();
											double lb = (*child)->getLB();
											if (ctx.verbose){cout << "\t\tlb = " << lb << "\t ctx.UB = "<< ctx.UB <<"\t #(NZ) = "<< (*child)->getNZ().size() << endl;}
											//if (verbose){cout << "\t\tSV("<<(*child)->getSv().size()<<") : ";	for (auto i : (*child)->getSv()) {	cout << i << " ";} cout << endl;}
											////////////////////////////////////////////////////////////
											if ((*child)->getSv().size() == 0) {
													if (ctx.verbose) {cout <<"@SV ! " <<(*child)->getIdx()<<"\t|SV|="<< (*child)->getSv().size() <<"\tS("<<(*child)->isSolution()<<")\tU("<<(lb < ctx.UB-ctx.tolF)<<")\tP("<<(lb < ctx.UB)<<")"<< endl;}
											};
											////////////////////////////////////////////////////////////
											if ((*child)->isSolution() && ctx.updateUB(lb, xlb)) {
													if (ctx.verbose) {cout << "> INT - UBupdated \t " << ctx.UB << endl;}
													UBupdated = true;
													x_opt = xlb;
													ctx.BestNodeNum = (*child)->getIdx();//ctx.BBNodeNum;
													ctx.nbr_update_Bsupp++;
													delete *child;
											} else if (!(*child)->isFeasible() || !node_set.push(*child)) {
													// The second condition means "The child LB is greater than the current UB"
													// This, with the infeasible child case, means we do not need it anymore
													delete *child;
											}
									}

							} // 	for (node_list::reverse_iterator child = nl.rbegin(); child != nl.rend(); child++)
							//pause_cin_txt("Next node ?");
							delete node;
							//
							if (UBupdated) {
									node_set.prune(ctx.UB);
							}
							//
							// if (ctx.force_stop) {
							// 		//pause_cin_txt("FORCE STOP IN Optimizer");
							// 		return zeros<vec> (problem.H.n_cols);
							// }
					}	// while(!node_set.empty())
			}	// if (root_node->isSolution()) {
			//
			return x_opt;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
Optimizer::~Optimizer() {
	if (this->computeUB != nullptr)
		delete this->computeUB;
	if (this->computeLB != nullptr)
		delete this->computeLB;
}
