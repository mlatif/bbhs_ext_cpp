#include "is_binary.h"

bool is_binary(vec b) {
	double eps_binary = exp10(-8);
	for (int i = 0; i < b.n_elem; i++) {
		if (b(i) > eps_binary && 1 - b(i) > eps_binary)
			return false;
	}
	return true;

}
