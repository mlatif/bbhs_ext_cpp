#include "Problem.hpp"
#include "LB.hpp"
#include "UB.hpp"

ProblemData::ProblemData(mat Hmat, vec yvec, double BigM, ParamType parameter, ProblemType pt)
: H(Hmat), HtH(Hmat.t()*Hmat), H_columnwise_norm_as_column(sqrt(sum(pow(Hmat, 2), 0)).t()), bigM(BigM), y(yvec), sqrt_yty(sqrt(yvec.t()*yvec)), param(parameter), type(pt)
{
	eyeN = eyeN.eye(Hmat.n_rows, Hmat.n_rows);
}
// ASC ANC VERSION
ProblemData::ProblemData(mat Hmat, vec yvec, ParamType parameter, ProblemType pt) : H(Hmat), HtH(Hmat.t()*Hmat), H_columnwise_norm_as_column(sqrt(sum(pow(Hmat, 2), 0)).t()), y(yvec), sqrt_yty(sqrt(yvec.t()*yvec)), param(parameter), type(pt){
	eyeN = eyeN.eye(Hmat.n_rows, Hmat.n_rows);
}
void ProblemData::set_ASC_formulation(ASCFormulation form) {	this->asc_form = form;	}
ASCFormulation ProblemData::get_ASC_formulation() {	return this->asc_form;	}




ProblemData::~ProblemData() {}
