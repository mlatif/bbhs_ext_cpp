#ifndef BBCONTEXT
#define BBCONTEXT 1

#include "MimosaConfig.hpp"

#ifdef USE_CPLEX
#include <ilcplex/ilocplex.h>
#include <ilcplex/ilocplexi.h>
ILOSTLBEGIN
#endif

#include "interfaces.hpp"
#include "Problem.hpp"
#include <string>
/**
 * This is filled by the Branch&Bound main loop. It can be used by other objects
 * to base some strategy on the global evolution of the optimization process.
 * For example, a node container could have a varying pop behavior depending on
 * the number of iterations done so far.
 */
class ContextData {
public:
	int it_num; /**< The number of Branch&Bound iterations done so far. */
	double UB; /**< The current best upper bound. */
	vec xUB; /**< The current best upper bound antecedent. */
	NodeContainerInterface* feasible_solution_set; /**< The set of solutions found so far (still subject to changes). */
	ProblemData* problem; /**< The problem to be solved (instance, dictionary matrix and such). */
	float T_test; /**< Time spent on tests for homotopy/activeset relaxation. */
	float T_model; /**< Time spent on model construction for cplex relaxation. */
	float T_relaxation; /**< Copy-pasted from old code, but seems to be never used. */
	float T_homo; /**< Overall time spent on homotopy/activeset computations. */
	float T_FCLS; /**< Overall time spent on FCLS computations. */
	long int nbr_iter; /**< Total number of homotopy/activeset iterations. */
	int itmax; /**< Maximum iteration number for a node homotopy relaxation. */
	int cut; /**< Controls, for P0/2, the threshold where we consider a node relaxation to be too close to the best upper bound to be useful. */
	bool warm_restart; /**< For activeset, should we start from scratch or from the parent node lower bound antecedent. */
	int BBNodeNum; /**< Number of nodes analyzed (aka bounded) so far by our Branch & Bound. */
	int BestNodeNum; /**< The index of the node leading to the best solution found so far by our Branch & Bound. */
	double TimeBBMax; /**< The maximum computing time given to solve the problem. */
	int nbr_update_Bsupp; /**< The number of best upper bound update. */
	double T_best_Bsupp; /**< The time taken to find the best solution of the problem. */
#ifdef USE_CPLEX
	IloInt CplexNodeNum; /**< The number of nodes explored by the Cplex solver (aka when we don't use our Branch & Bound). */
	IloAlgorithm::Status CplexStatus; /**< The status code of Cplex at the end of the optimization process. */
	int cplex_branching_rule; /**< The branching rule to be used for the Cplex solver. */
#endif
	double gap; /**< Numeric gap for the Cplex solver. */
	double tolC; /**< Numeric tolerance for constraint violation. */
	double tolH; /**< Numeric tolerance for heap comparator. */
	double tolX; /**< Numeric tolerance for x comparisons. */
	double tolF; /**< Numeric tolerance for objective function comparisons. */
	//
	vec x_truth; /**< For the <b>l2_l0_asc_anc</b> problem, allows the optimal solution to be set as a parameter of the context object. */
	std::string inst_id;
	std::string set_id;
	bool force_stop;
	bool verbose;
	//

	ContextData();
	/**
	 * This constructor is preferred, as in the classic case you tie one context to one problem
	 * (aka you do not generally use one context to solve multiple problems).
	 */
	ContextData(ProblemData* pb);

	virtual ~ContextData() {}

	// we let the compiler inline it at will
	/**
	 * Updates the current solution with the candidate in arguments.
	 * If the candidate is worse than the current solution, does nothing.
	 *
	 * @param ub The candidate upper bound value.
	 * @param xUB The candidate upper bound antecedent.
	 * @returns nothing.
	 */
	bool updateUB(double ub, vec xUB) {
		bool change = ub < this->UB - this->tolF;
		if (change) {
			this->UB = ub;
			this->xUB = xUB;
			//cout << "# UPDATE_UB\tctx.UB = " << this->UB << endl;
		}
		return change;
	}

	//


	/**
	 * Reset every field at it's default value.
	 */
	void reset(ProblemData* problem);
	/** <b>l2_l0_asc_anc version</b> - Reset every field at it's default value and define the real solution vector as a parameter
    @param problem : The current problem object
    @param x_truth : The real solution vector
    @return nothing.
	*/
	// void reset(ProblemData* problem, vec x_truth);
	void reset(ProblemData* problem, vec x_truth,std::string i_id,std::string s_id,bool verbose);
};

#endif
