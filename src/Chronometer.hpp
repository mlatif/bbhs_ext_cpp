#ifndef BBCHRONOMETER
#define BBCHRONOMETER

#include "interfaces.hpp"

// To be guarded in the proper ifdef

#include <time.h>

/**
 * GNU/Linux chronometer using standard library.
 */
class LinuxChronometer : public ChronometerInterface {
	clock_t start_base;
public:
	virtual void start();
	virtual double check();
};

// Let the compiler inline
inline ChronometerInterface* alloc_chrono()
{
	// To be guarded in the proper ifdef
	return new LinuxChronometer();
}

#endif
