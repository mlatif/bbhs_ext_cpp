#ifndef BBNODECONTAINER
#define BBNODECONTAINER 1

#include <algorithm>
#include "Context.hpp"
#include "interfaces.hpp"

/** \file node_container.hpp
 * \brief Nodes storage and retrieval.
 *
 * A node container stores (pointer of) nodes, and gives them back
 * according to some strategy, which can be arbitrarily complex.
 */

/**
 * For LIFONodeContainer, we give back nodes in a stack/LIFO manner,
 * which corresponds to an always-intensification strategy.
 */
class LIFONodeContainer : public NodeContainerInterface {
	ContextData& ctx; /**< Context reference, currently not used. */
	std::vector<NodeInterface*> storage; /**< Internal node storage. */
public:
	/**
	 * This container doesn't need the Context reference, but this is a clear
	 * way to see if everything fits together well, as a node container *could*
	 * need to modify it's behaviour depending on the context.
	 */
	LIFONodeContainer(ContextData& ctx);
	virtual ~LIFONodeContainer();
	virtual NodeInterface* top();
	virtual NodeInterface* pop();
	virtual bool push(NodeInterface* node);
	virtual double getLowestLB();
	virtual void prune(double ub);
	virtual bool empty() { return this->storage.empty(); }
	virtual void reset();
	virtual void remove(NodeInterface* node);
	virtual unsigned long long int size(){ return this->storage.size();	};

};


/**
 * This implements an ordering by the node's lower bound.
 */
class MinimierLBCompare {
public:
	/**
	 * Just returns the correct boolean check.
	 */
	virtual bool operator()(NodeInterface* n1, NodeInterface* n2) const {
		// !!! THIS IS **NOT** REVERSED. !!!
		// The stl heap implementation is using a max heap approach, so
		// that the greatest element in the sense of the compare operation is at the top
		// of the heap. As we want to have the lowest LB at the top with this comparator,
		// we must reverse the comparison.
		return n1->getLB() > n2->getLB();
	}
	MinimierLBCompare(ContextData& ctx) {}
};


/**
 * This implements an ordering by the node's l1 norm of xlb.
 */
class MinimierL1Compare {
	double tol;
public:
	/**
	 * Just returns the correct boolean check.
	 */
	bool operator()(NodeInterface* n1, NodeInterface* n2) const {
		// !!! THIS IS **NOT** REVERSED. !!!
		// The stl heap implementation is using a max heap approach, so
		// that the greatest element in the sense of the compare operation is at the top
		// of the heap. As we want to have the lowest LB at the top with this comparator,
		// we must reverse the comparison.
		return n1->getLBL1() > (n2->getLBL1() + this->tol);
	}
	MinimierL1Compare(ContextData& ctx) : tol(ctx.tolH) {}
};

/**
 * This implements an ordering by the node's least-square error of xlb.
 */
class MinimierLSCompare {
public:
	/**
	 * Just returns the correct boolean check.
	 */
	bool operator()(NodeInterface* n1, NodeInterface* n2) const {
		// !!! THIS IS **NOT** REVERSED. !!!
		// The stl heap implementation is using a max heap approach, so
		// that the greatest element in the sense of the compare operation is at the top
		// of the heap. As we want to have the lowest LB at the top with this comparator,
		// we must reverse the comparison.
		return n1->getLBLS() > n2->getLBLS();
	}
	MinimierLSCompare(ContextData& ctx) {}
};


/**
 * For MinimierNodeContainer, we give back nodes according to the ordering produced
 * by a heap using a MinimierLBCompare (sub)class.
 * If MinimierLBCompare class is used (instead of some subclass), it means we are ordering
 * by the lower bound of the nodes, the node with the lowest lower bound being the first node to pop out of
 * the heap. This corresponds to an always-diversification strategy.
 */
template <class Comp>
class MinimierNodeContainer : public NodeContainerInterface {
	ContextData& ctx; /**< Context reference, currently not used. */
	std::vector<NodeInterface*> storage; /**< Internal node storage. */
	Comp compare; /**< Compare operator for the heap. */
public:
	/**
	 * Constructs a heap-based node container. By supplying a MinimierLBCompare,
	 * this heap will be ordered such that the node with the lowest lower bound will
	 * get picked first.
	 *
	 * @param context The context reference (currently unused).
	 */
	MinimierNodeContainer(ContextData& context) : ctx(context), compare(context) {
		make_heap(this->storage.begin(), this->storage.end(), this->compare);
	}

	virtual ~MinimierNodeContainer() { this->reset(); }

	virtual NodeInterface* top() {
		return this->storage.front();
	}

	virtual NodeInterface* pop() {
		pop_heap(this->storage.begin(), this->storage.end(), this->compare);
		NodeInterface* node = this->storage.back();
		this->storage.pop_back();
		return node;
	}

	virtual bool push(NodeInterface* node) {
		if (node->getLB() < this->ctx.UB) {
			this->storage.push_back(node);
			push_heap(this->storage.begin(), this->storage.end(), this->compare);
			return true;
		}
		return false;
	}

	virtual double getLowestLB() {
		// Naive implementation for now
		double min_lb = this->storage.front()->getLB();
		for (std::vector<NodeInterface*>::iterator it = this->storage.begin(); it != this->storage.end(); it++) {
			double cur_lb = (*it)->getLB();
			if (cur_lb < min_lb)
				min_lb = cur_lb;
		}
		return min_lb;
	}

	virtual void prune(double ub) {
		std::vector<NodeInterface*>::iterator it = this->storage.begin();
		while(it != this->storage.end()) {
			if ((*it)->getLB() > ub) {
				delete *it;
				it = this->storage.erase(it);
			} else {
				it++;
			}
		}
		// As we've deleted some nodes, the heap structure may be destroyed,
		// so we remake it just in case (TODO: show that the heap structure can / can never be destroyed)
		make_heap(this->storage.begin(), this->storage.end(), this->compare);
	}

	virtual bool empty() { return this->storage.empty(); }

	virtual void reset() {
		if (!this->storage.empty()) {
			for (std::vector<NodeInterface*>::iterator it = this->storage.begin(); it != this->storage.end(); it++) {
				delete *it;
			}
		}
		this->storage.clear();
	}

	virtual void remove(NodeInterface* node) {
		this->storage.erase(std::remove(this->storage.begin(), this->storage.end(), node), this->storage.end());
	}

	virtual unsigned long long int size() {	return this->storage.size();	}
};


// Stack then heap container

/**
 * For LIFOThenMinimierThresholdNodeContainer, we give back nodes first like a stack,
 * and when the iteration number is greater than a threshold by a heap using a MinimierLBCompare (sub)class.
 */
template <class Comp>
class LIFOThenMinimierThresholdNodeContainer : public NodeContainerInterface {
	ContextData& ctx; /**< Context reference, used for threshold condition verification. */
	NodeContainerInterface* container; /**< The actual container used. */
	unsigned int threshold; /**< The threshold on the number of iteration to use. */
public:
	/**
	 * Constructs a variable-strategy node container. By supplying a MinimierLBCompare,
	 * the heap used after the threshold condition is verified will be ordered such that the node with the lowest lower bound will
	 * get picked first.
	 *
	 * @param context The context reference (used to get the number of iterations done so far).
	 * @param thresh The iteration threshold to use for container change.
	 */
	LIFOThenMinimierThresholdNodeContainer(ContextData& context, unsigned int thresh): ctx(context), threshold(thresh)
	{
		this->container = new LIFONodeContainer(context);
	}

	virtual ~LIFOThenMinimierThresholdNodeContainer() {
		delete this->container;
	}

	virtual NodeInterface* top() {
		return this->container->top();
	}

	virtual NodeInterface* pop() {
		// We do the container change at pop because there is exactly one per iteration happening.
		if (this->ctx.it_num == this->threshold) {
			NodeContainerInterface *heap = new MinimierNodeContainer<Comp>(this->ctx);
			while (!this->container->empty())
				heap->push(this->container->pop());
			delete this->container;
			this->container = heap;
		}
		return this->container->pop();
	}

	virtual bool push(NodeInterface* node) {
		return this->container->push(node);
	}

	virtual double getLowestLB() {
		return this->container->getLowestLB();
	}

	virtual void prune(double ub) {
		this->container->prune(ub);
	}

	virtual bool empty() { return this->container->empty(); }

	virtual void reset() {
		delete this->container;
		this->container = new LIFONodeContainer(this->ctx);
	}
	virtual void remove(NodeInterface* node) {
		this->container->remove(node);
	}

	virtual unsigned long long int size() {	return this->container->size();	}

};

#endif
