#include "NodeImplementation.hpp"
#include "Util.h"		// 	Pour utiliser : pause_cin_txt("updateSv");
#include "Chronometer.hpp"
// Common implementation

CommonNodeImplementation::CommonNodeImplementation() : LB(0), LBL1(0), LBLS(0), UBLS(0),  feasible(false), solution(false),idx(0)
{
	// DO NOT CONSTRUCT NODES BY HAND!!
	// Except for the root node, you should use the duplicate method,
	// which is not tied to a particular node class implementation.
	this->S1.clear();
	this->S0.clear();
	this->SBar.clear();
	this->Sv.clear();
	this->NZ.clear();
}

int CommonNodeImplementation::getDepth()
{
	return this->S1.size() + this->S0.size();
}

support CommonNodeImplementation::getS0()
{
	return this->S0;
}

support CommonNodeImplementation::getS1()
{
	return this->S1;
}

support CommonNodeImplementation::getSBar()
{
	return this->SBar;
}

int CommonNodeImplementation::getK()
{
	return this->S1.size();
}

double CommonNodeImplementation::getUBLS()
{
	return this->UBLS;
}

double CommonNodeImplementation::getLBLS()
{
	return this->LBLS;
}

double CommonNodeImplementation::getLBL1()
{
	return this->LBL1;
}

vec CommonNodeImplementation::getLPS()
{
	return this->lps_score;
}

// For now, setUB and setxUB are never called, but they could.
void CommonNodeImplementation::setUB(double ub) {
	this->UB = ub;
}

void CommonNodeImplementation::setLB(double lb)
{
	this->LB = lb;
}

void CommonNodeImplementation::setxUB(vec xub) {
	this->xub = xub;
}

void CommonNodeImplementation::setxLB(vec xlb)
{
	this->xlb = xlb;
}

void CommonNodeImplementation::setUBLS(double ls)
{
	this->UBLS = ls;
}

void CommonNodeImplementation::setLBLS(double ls)
{
	this->LBLS = ls;
}

void CommonNodeImplementation::setLBL1(double l1)
{
	this->LBL1 = l1;
}

void CommonNodeImplementation::setS0(support s0)
{
	this->S0 = s0;
}

void CommonNodeImplementation::setS1(support s1)
{
	this->S1 = s1;
}

void CommonNodeImplementation::setSBar(support sbar)
{
	this->SBar = sbar;
}

void CommonNodeImplementation::setLPS(vec lps)
{
	this->lps_score = lps;
}


void CommonNodeImplementation::setFeasible(bool feasible)
{
	this->feasible = feasible;
}

bool CommonNodeImplementation::isFeasible()
{
	return this->feasible;
}

void CommonNodeImplementation::setSolution(bool solution)
{
	this->solution = solution;
}

bool CommonNodeImplementation::isSolution()
{
	return this->solution;
}
// -----------------------------------------------------------------------------
unsigned long long int CommonNodeImplementation::getIdx() {
		return this-> idx;
}
void CommonNodeImplementation::setIdx(unsigned long long int n) {
		this->idx = n;
}
support CommonNodeImplementation::getSv(){
		return this->Sv;
}
void CommonNodeImplementation::setSv(support sv){
		this->Sv = sv;
}
void CommonNodeImplementation::resetSv() {
		this->Sv.clear();
}

uvec CommonNodeImplementation::getNZ() {
		return this->NZ;
}
void CommonNodeImplementation::setNZ(uvec nz) {
		this->NZ = nz;
}
void CommonNodeImplementation::resetNZ() {
		this->NZ.clear();
}

void CommonNodeImplementation::updateSvNz(vec xlb) {
		// Pourquoi je ne peux tout simplement pas initialiser Sv aux éléments non nuls de x_lb;
		// => La problématique est que dans le cas présent, je suis au noeud racine donc nécessairement on tape dans l'intersection des deux sous ensembles;
		// => Mais cela peut potientiellement varier dans les sous noeuds. Il faut s'assurer que le sous ensemble est maintenu avec des éléments qui sont toujours dans Sbar ...
		// Problèmes/faits
		//		=> Une intersection rapide ne se fait qu'avec des uvec
		//		=> support est un vector<int>
		//		=> le type d'un uvec est un vector de uword qui par def est un entier non signé
		//		=> Une méthode bourrin vient donc à nettoyer puis ajouter en complexité linéaire (en nb elements) les indices (uwords <=> int non signé) de l'intersection
		// -------------------------------------------------------------------------
		// xlb.t().print("xlb");
		// Etape 1 : Détermination des ensembles courants d'indices et de l'intersection
		uvec S_NZ(find(xlb > 0)), 	S_B(conv_to<uvec>::from(this->getSBar())),	S_I(intersect(S_B,S_NZ));
		// cout << "S_NZ("<<S_NZ.size()<<") :\t "; for (auto e : S_NZ) {cout << e << ", ";};
		// cout << endl << "S_B("<<S_B.size()<<") :\t "; for (auto e : S_B) {cout << e << ", ";};
		// cout << endl << "S_I("<<S_I.size()<<") :\t"; for (auto e : S_I) {cout << e << ", ";}; cout << endl;
		// pause_cin_txt("SNZ");
		// Etape 2 : Nettoyage de printemps et mise à jour
		this->resetSv();
		if (!S_I.is_empty()) {	for (auto e : S_I) {	this->Sv.push_back(e);	};	}
		this->resetNZ();
		this->setNZ(S_NZ);
		//pause_cin_txt("updateSv");		// Décommenter  #include "Util.h"
}




// ==== Optimizer version ====

OptimizerNode::OptimizerNode(Optimizer& opti) : CommonNodeImplementation(), settedLB(false), settedxLB(false), optimizer(opti), settedUB(false),settedxUB(false)
{
	// DO NOT CONSTRUCT NODES BY HAND!!
	// Except for the root node, you should use the duplicate method,
	// which is not tied to a particular node class implementation.
}

mat OptimizerNode::getHtHinv()
{
	uvec Q1= conv_to <uvec>::from(this->S1);
	if (!Q1.is_empty())
		return inv(this->optimizer.context->problem->HtH.submat(Q1,Q1));
	return mat();
}

NodeInterface* OptimizerNode::duplicate(bool preComputedLB,bool preComputedUB)
{
	OptimizerNode* new_node = new OptimizerNode(this->optimizer);
	new_node->LB = this->LB;
	new_node->xlb = this->xlb;
	new_node->UBLS = this->UBLS;
	new_node->LBLS = this->LBLS;
	new_node->LBL1 = this->LBL1;
	new_node->S1 = this->S1;
	new_node->S0 = this->S0;
	new_node->SBar = this->SBar;
	new_node->idx = this->idx;
	//
	new_node->Sv = this->Sv;
	//
	if (preComputedLB) {
			new_node->setLB(this->LB);
			new_node->setxLB(this->xlb);
	} else {
			new_node->LB = this->LB;
			new_node->xlb = this->xlb;
	}
	//
	if (preComputedUB) {
			new_node->setUB(this->UB);
			new_node->setxUB(this->xub);
	} else {
			new_node->UB = this->UB;
			new_node->xub = this->xub;
	}
	//
	return new_node;
}


void OptimizerNode::setLB(double lb)
{
	this->settedLB = true;
	this->LB = lb;
}
void OptimizerNode::setxLB(vec xlb)
{
	this->settedxLB = true;
	this->xlb = xlb;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void OptimizerNode::setUB(double ub)
{
	this->settedUB = true;
	this->UB = ub;
}
void OptimizerNode::setxUB(vec xub)
{
	this->settedxUB = true;
	this->xub = xub;
}

// These are calling the correct object function
bound_type OptimizerNode::computeUB()
{
	return (*this->optimizer.computeUB)(*this);
}
///////////////////////////////////////////////////////////////////////////////
/////Modifiée pour pouvoir limiter le nombre de calculs de bornes sup//////////
///////////////////////////////////////////////////////////////////////////////
double OptimizerNode::getUB()
{
	if (this->settedUB){
			return this->UB;
	} else {
			bound_type tmp = this->computeUB();
			return tmp.second;
	}
}

vec OptimizerNode::getxUB()
{
	if (this->settedxUB) {
			return this->xub;
	} else {
			bound_type tmp = this->computeUB();
			return tmp.first;
	}
}
;

bound_type OptimizerNode::computeLB(vec xub)
{
	bound_type tmp = (*this->optimizer.computeLB)(*this, xub);
	// Setting inner cache
	//this->xlb = tmp.first;
	//this->LB = tmp.second;
	return tmp;
}

double OptimizerNode::getLB(bool old_is_good)
{
	if (this->settedLB)
		return this->LB;
	else {
		bound_type tmp = this->computeLB(this->getxUB());
		return tmp.second;
	}
}

vec OptimizerNode::getxLB(bool old_is_good)
{
	if (this->settedxLB || old_is_good) {
		if (this->xlb.is_empty())
			// this edge case happens when we wish to get an old xlb from the root node,
			// before any bounds computations.
			return zeros(this->optimizer.context->problem->H.n_cols);
		return this->xlb;
	} else {
		bound_type tmp = this->computeLB(this->getxUB());
		return tmp.first;
	}
}

node_list OptimizerNode::split()
{
	return (*this->optimizer.split)(*this);
}


NodeInterface& OptimizerNode::contract()
{
	return (*this->optimizer.contract)(*this);
}
