#ifndef BBNODE
#define BBNODE 1

#include "Context.hpp"
#include "interfaces.hpp"
#include "Optimizer.hpp"

/**
 * \file NodeImplementation.hpp
 * \brief Concrete node implementation
 *
 * The code here is separated in two.
 * First, everything which is related to storage inside the node
 * is handled in CommonNodeImplementation which is *abstract*.
 * Second, everything which is related to operators on a node
 * (lb, ub, contract, split) depends on where these operators are
 * stored, and this is different between the code which is using
 * runBB (v1 of the modularized code) and the code which is using Optimizer (v2).
 */

/**
 * This class handles everything related to storage inside a node.
 */
class CommonNodeImplementation : public NodeInterface {
protected:
	double LB, /**< Lower bound value. */
				 UB,
	       LBL1, /**< Lower bound l1 component. */
	       LBLS, /**< Lower bound least-square component. */
	       UBLS; /**< Least-square component of the upper bound. */
	support S1, /**< Not-null support. */
	        S0, /**< 0-valued support. */
					SBar, /**< To be determined support. */
					Sv;		/**< Subset of Sbar - non zeros components to be determined support. */
	uvec NZ;
	vec xlb, /**< Lower bound antecedent. */
			xub,
	    lps_score; /**< L1 path selection score. */
	bool feasible, /**< Feasibility flag (aka constraints are not violated). */
	     solution; /**< SOlution flag (aka all the bi are binary in the relaxation computation). */
	unsigned long long int idx;	/**< The index of the node (used in l2l0_asc_anc version). */
public:
	// These should never call external node operations such as UB, LB, Split, ...
	virtual ~CommonNodeImplementation(){}
	CommonNodeImplementation();
	virtual int getDepth();
	virtual support getS0();
	virtual support getS1();
	virtual support getSBar();
	virtual int getK();
	virtual double getUBLS();
	virtual double getLBLS();
	virtual double getLBL1();
	virtual vec getLPS();
	virtual void setUB(double ub);
	virtual void setLB(double lb);
	virtual void setxUB(vec xub);
	virtual void setxLB(vec xlb);
	virtual void setUBLS(double ls);
	virtual void setLBLS(double ls);
	virtual void setLBL1(double l1);
	virtual void setS0(support s0);
	virtual void setS1(support s1);
	virtual void setSBar(support sbar);
	virtual void setLPS(vec lps);
	virtual void setFeasible(bool feasible);
	virtual bool isFeasible();
	virtual void setSolution(bool solution);
	virtual bool isSolution();
	//
	virtual unsigned long long int getIdx();
	virtual void setIdx(unsigned long long int);
	//
	virtual support getSv();
	virtual void setSv(support);
	virtual void updateSvNz(vec);
	virtual void resetSv();
	//
	virtual uvec getNZ();
	virtual void setNZ(uvec);
	//virtual void updateNZ(vec);
	virtual void resetNZ();

};

/**
 * This is the node implementation for the Optimizer code (v2).
 * LB, UB, Split, Contract are all located in Optimizer.
 */
class OptimizerNode : public CommonNodeImplementation {
	bool settedLB, /**< Flag signaling if the stored LB comes from actual computation or from parent node inheritance. */
	     settedxLB; /**< Flag signaling if the stored xLB comes from actual computation or from parent node inheritance. */
	bool settedUB,
			 settedxUB;
	Optimizer& optimizer; /**< Optimizer reference for all the operators. */
public:
	// These should never call external node operations such as UB, LB, Split, ...
	virtual ~OptimizerNode(){}
	OptimizerNode(Optimizer& optimizer);
	virtual mat getHtHinv();
	virtual NodeInterface* duplicate(bool preComputedLB=false,bool preComputedUB=false);
	virtual void setLB(double lb);
	virtual void setxLB(vec xlb);
	// These are calling the correct object function
	virtual double getUB();
	virtual vec getxUB();
	virtual bound_type computeUB();

	virtual double getLB(bool old_is_good = false);
	virtual vec getxLB(bool old_is_good = false);
	virtual bound_type computeLB(vec xub);

	virtual node_list split();
	virtual NodeInterface& contract();
	//
	virtual void setUB(double ub);
	virtual void setxUB(vec xub);

};

#endif
