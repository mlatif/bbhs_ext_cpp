#ifndef BBCPLEXSOLVER
#define BBCPLEXSOLVER 1

#include "interfaces.hpp"
#include "Problem.hpp"
#include "Context.hpp"


/**
 * Solves a problem using exclusively the Cplex solver.
 */
class CplexSolver {
protected:
	/**
	 * Generate and export to a file the P0/2 Cplex model.
	 */
	void generate_model_l0l2_with_bigM(mat H, vec y, double epsilon, double bigM);
	/**
	 * Solve a P0/2 problem.
	 */
	vec Cplexsolver_l0l2(mat *A,vec *y,double epsilon ,double BigM);

	/**
	 * Generate and export to a file the P2/0 Cplex model of an underdetermined P2/0 (more dictionary rows than columns).
	 */
	void generate_model_with_bigM_l2l0(mat H, vec y, int k, double bigM);
	/**
	 * Generate and export to a file the P2/0 Cplex model of an overdetermined P2/0 (more dictionary columns than rows).
	 */
	void generate_model_with_bigM_l2l0_NC(mat H, vec y, int k, double bigM);
	/**
	 * Solve a P2/0 problem.
	 */
	vec Cplexsolver_l2l0(mat *A,vec *y,int k ,double BigM);

	/**
	 * Generate and export to a file the P2+0 Cplex model.
	 */
	void generate_model_with_bigM_l2pl0(mat H, vec y,  double lambda, double bigM);
	/**
	 * Solve a P2+0 problem.
	 */
	vec Cplexsolver_l2pl0(mat *A,vec *y, double lambda,double BigM);
	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////
	/**
	* Generate and export to a file the P2/0+ASC+ANC Cplex model - overdetermined case ie N > Q with N the number of observations, Q the number of endmembers.
	* @param H the dictionary of dimensions N rows x Q columns;
	* @param y the observations vector of dimensions N rows x 1 columns
	* @param k the sparsity coefficient (cardinality of S1)
	*
	* @return nothing but write the model as a .lp file
	*/
	void generate_model_l2l0_ASC_ANC_OC(mat H, vec y, int k,bool ASC_asEq, std::string set_id);
	/**
	* Generate and export to a file the P2/0+ASC+ANC Cplex model - underdetermined case ie N <= Q with N the number of observations, Q the number of endmembers.
	* @param H the dictionary of dimensions N rows x Q columns;
	* @param y the observations vector of dimensions N rows x 1 columns
	* @param k the sparsity coefficient (cardinality of S1)
	*
	* @return nothing but write the model as a .lp file
	*/
	void generate_model_l2l0_ASC_ANC_UC(mat H, vec y, int k,bool ASC_asEq, std::string set_id);
	/**
	 * Solve a P2/0+ASC+ANC problem.
	 */
	vec Cplexsolver_l2l0_ASC_ANC(mat *A,vec *y,int k);

public:
	ContextData* context; /**< Context that will be filled during optimization. */

	virtual ~CplexSolver() {}
	/**
	 * It is advised to give directly the ContextData during construction.
	 */
	CplexSolver(ContextData* ctx = nullptr) : context(ctx) {}
	/**
	 * Start the optimization process for a given problem.
	 *
	 * @param problem The problem instance to solve.
	 * @returns the solution antecedent.
	 */
	vec optimize(ProblemData& problem);
};

#endif
