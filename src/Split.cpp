#include "Split.hpp"

node_list AbstractSplitInTwo::operator()(NodeInterface& node)
{
	node_list ret;
	support sbar = node.getSBar();
	support sv(node.getSv());
	// --------------------- //
	if (!sbar.empty() && !sv.empty()) {
		int qj = this->getBranchingIndex(node);
		// --------------------- //
		//std::string typeBranch = node.getxLB()(qj) != 0 ? "BRANCH":"0-BRANCH";
		//cout <<typeBranch<<"("<<node.getIdx()<<")"<<"\t qj = " << qj << " \t x("<<qj<<") = "<< node.getxLB()(qj) <<endl;
		// --------------------- //
		// Creating the children
		sbar.erase(remove(sbar.begin(), sbar.end(), qj), sbar.end());
		sv.erase(remove(sv.begin(), sv.end(), qj), sv.end());
		NodeInterface* LeftChild = node.duplicate(this->ctx.problem->type == ProblemType::L2L0_ASC_ANC);
		support upS1 = node.getS1();
		upS1.push_back(qj);
		LeftChild->setS1(upS1);
		LeftChild->setSBar(sbar);
		LeftChild->setSv(sv);
		LeftChild->setIdx(2*node.getIdx()+1);
		ret.push_back(LeftChild);
		//
		NodeInterface* RightChild = node.duplicate(false,this->ctx.problem->type == ProblemType::L2L0_ASC_ANC);
		support downS0 = node.getS0();
		downS0.push_back(qj);
		RightChild->setS0(downS0);
		RightChild->setSBar(sbar);
		RightChild->setSv(sv);
		RightChild->setIdx(2*node.getIdx()+2);
		ret.push_back(RightChild);
	}
	return ret;
}

int MaxXiBranchingRule::getBranchingIndex(NodeInterface& node)
{
	vec x_relache = node.getxLB();
	// Taking the maximum xi absolute value inside the not-yet-determined support
	support sbar = node.getSBar();
	uvec Qv = conv_to<uvec>::from(sbar);
	return Qv(index_max(abs(x_relache(Qv))));
}

int MaxNZXiBranchingRule::getBranchingIndex(NodeInterface& node) {
		//	Les branchements ne peuvent se faire que sur un ensemble valide ie dans Sbar et dans Sv;
		vec x_relache(node.getxLB());
		support S_NZ(node.getSv());	//,	S_B(node.getSBar());
		//cout << "S_NZ("<<S_NZ.size()<<") :\t "; for (auto e : S_NZ) {cout << e << ", ";}; cout << endl << "S_B("<<S_B.size()<<") :\t "; for (auto e : S_B) {cout << e << ", ";};	cout << endl;
		if (!S_NZ.empty()) {
				uvec Qv = conv_to<uvec>::from(S_NZ);
				return Qv(index_max(abs(x_relache(Qv))));
		}
		return -1;
}



int MostUndeterminedXiBranchingRule::getBranchingIndex(NodeInterface& node)
{
	vec x_relache = node.getxLB();
	// Taking the maximum xi absolute value inside the not-yet-determined support
	support sbar = node.getSBar();
	uvec Qv = conv_to<uvec>::from(sbar);
	vec score = abs(0.5*ones(Qv.n_elem) - abs(x_relache(Qv)/this->ctx.problem->bigM));
	return Qv(index_min(score));

}

int MaxLPSBranchingRule::getBranchingIndex(NodeInterface& node)
{
	vec score = node.getLPS();
	uvec Qv = conv_to<uvec>::from(node.getSBar());
	return Qv(index_max(score(Qv)));
}
